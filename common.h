﻿/*
 * Copyright (C) 2020 Krzysztof Stachowiak
 *
 * This file is part of the Language Programming Language compiler.
 *
 * Language Programming Language is free software: you can redistribute it
 * and/or modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Language Programming Language compiler is distributed in the hope that it
 * will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef COMMON_H
#define COMMON_H

#include "test.h"

#include <stdbool.h>
#include <stdint.h>
#include <stdlib.h>
#include <stdio.h>
#include <string.h>

// Immediate termination of the program upon an error

#define ABORT(FORMAT, ...) \
    do { \
        fprintf(stderr, "[ABORT@%s:%d] " FORMAT "\n", __FILE__, __LINE__, ##__VA_ARGS__); \
        exit(2); \
    } while (0)

// Overview
// ========
//
// Common algorithms and data structures.

// Memory management
// -----------------

void* my_malloc(size_t size);
void *my_realloc(void *buffer, size_t size);
void my_free(void *x);

// Hashing
// -------

uint64_t hash_fast(char *buffer, size_t size);

// Dynamic array
// -------------

#define array_free(MACRO_ARRAY) \
    do { \
        my_free((MACRO_ARRAY).data); \
        (MACRO_ARRAY).data = NULL; \
        (MACRO_ARRAY).size = 0; \
        (MACRO_ARRAY).cap = 0; \
    } while(0)

#define array_copy(dst, src) \
    do { \
        int size = src.cap * sizeof(*dst.data); \
        dst.data = my_malloc(size); \
        memcpy(dst.data, src.data, size); \
        dst.size = src.size; \
        dst.cap = src.cap; \
    } while (0)

#define array_append(MACRO_ARRAY, MACRO_ELEMENT) \
    do { \
        if ((MACRO_ARRAY).cap == 0) { \
            (MACRO_ARRAY).data = my_malloc(sizeof(*((MACRO_ARRAY).data))); \
            (MACRO_ARRAY).cap = 1; \
        } else if ((MACRO_ARRAY).cap == (MACRO_ARRAY).size) { \
            (MACRO_ARRAY).cap *= 2; \
            (MACRO_ARRAY).data = my_realloc(\
                    (MACRO_ARRAY).data,\
                    (MACRO_ARRAY).cap * sizeof(*((MACRO_ARRAY).data))); \
        } \
        (MACRO_ARRAY).data[(MACRO_ARRAY).size++] = (MACRO_ELEMENT); \
    } while (0)

#define array_remove(MACRO_ARRAY, MACRO_INDEX) \
    do { \
        (MACRO_ARRAY).data[(MACRO_INDEX)] = (MACRO_ARRAY).data[(MACRO_ARRAY).size - 1]; \
        (MACRO_ARRAY).size -= 1; \
    } while(0)

#define array_pop(MACRO_ARRAY) \
    do { \
        array_remove((MACRO_ARRAY), (MACRO_ARRAY).size - 1); \
    } while(0)


// String operations
// -----------------

char *str_copy(char *x);
char *str_copy_range(char *first, char *last);
char *str_escape(char *x);
char *str_copy_line(char *string, int line);
int str_count_range(char *string, size_t offset, char character);

#define STR_TEMP_BUFFER_SIZE 1024 * 10

#define str_append(TXT, FORMAT, ...)\
    do {\
        char *_buffer_; \
        int _old_len_, _new_len_;\
        char *_new_text_;\
        _buffer_ = my_malloc(STR_TEMP_BUFFER_SIZE); \
        _new_len_ = sprintf(_buffer_, (FORMAT), ##__VA_ARGS__); \
        if (_new_len_ >= (STR_TEMP_BUFFER_SIZE) - 1) {\
            fprintf(stderr, "Buffer of %d not enough for %d bytes.", \
                STR_TEMP_BUFFER_SIZE, \
                _new_len_); \
            exit(2);\
        }\
        if (!(TXT)) {\
            _new_text_ = (char *)my_malloc(_new_len_ + 1);\
            memcpy(_new_text_, _buffer_, _new_len_ + 1);\
        } else {\
            _old_len_ = strlen((TXT));\
            _new_text_ = (char *)my_malloc(_old_len_ + _new_len_ + 1); \
            memcpy(_new_text_, (TXT), _old_len_);\
            memcpy(_new_text_ + _old_len_, _buffer_, _new_len_ + 1);\
            my_free((TXT));\
        } \
        (TXT) = _new_text_; \
        my_free(_buffer_); \
    } while(0)

#define str_append_range(TXT, FIRST, LAST)\
    do {\
        int _old_len_, _new_len_;\
        char *_new_text_;\
        _new_len_ = ((LAST) - (FIRST));\
        if (!(TXT)) {\
            _new_text_ = (char *) my_malloc(_new_len_ + 1);\
            memcpy(_new_text_, (FIRST), _new_len_);\
            _new_text_[_new_len_] = '\0';\
        } else {\
            _old_len_ = strlen((TXT));\
            _new_text_ = (char *)my_malloc(_old_len_ + _new_len_ + 1);\
            memcpy(_new_text_, (TXT), _old_len_);\
            memcpy(_new_text_ + _old_len_, (FIRST), _new_len_);\
            _new_text_[_old_len_ + _new_len_] = '\0';\
            my_free((TXT));\
        }\
        (TXT) = _new_text_;\
    } while(0)

// Pointer stack
// -------------
//
// Dynamically expanding stack of void pointers

struct PtrStack
{
    int size, cap;
    void **data;
};

void ptr_stack_init(struct PtrStack *ptr_stack);
void ptr_stack_deinit(struct PtrStack *ptr_stack, void(*destructor)(void*));
void ptr_stack_push(struct PtrStack *ptr_stack, void *x);
void *ptr_stack_top(struct PtrStack *ptr_stack);
void ptr_stack_pop(struct PtrStack *ptr_stack);

// Trie
// ----

struct TrieNode {
    char key;
    struct { struct TrieNode *data; int cap, size; } children;
    bool is_set;
    void *data;
};

struct Trie {
    struct TrieNode root;
};

void trie_init(struct Trie *trie);
void trie_deinit(struct Trie *trie, void (*destructor)(void*));
void trie_insert(struct Trie *trie, char *key, void *data);
struct TrieNode *trie_find(struct Trie *trie, char* key);
void trie_for_each(
        struct Trie *trie,
        void (*callback)(char*, struct TrieNode*, void*),
        void *data);

// Hash table
// ----------

struct HashTableNode {
    uint64_t hash;
    void *key;
    void *value;
};

struct HashTable {
    struct { struct HashTableNode *data; int cap, size; } nodes;
    uint64_t (*function)(void*);
};

void ht_init(struct HashTable *ht, uint64_t (*function)(void*));
void ht_deinit(struct HashTable *ht, void (*destructor)(void*));
bool ht_put(struct HashTable *ht, void *key, void *value);
void *ht_get(struct HashTable *ht, void *key);
void ht_for_each(
        struct HashTable *ht,
        void (*callback)(struct HashTableNode*, void *),
        void *data);

// String range based algorithms
// -----------------------------

bool all_of(char *first, char *last, int (*predicate)(int));
char *find(char *first, char *last, int value);
char *find_if_not(char *first, char *last, int (*predicate)(int));

// File handling
// -------------

char *my_getline(FILE *file, bool *eof);
char *my_getfile(char *filename);

// Test routines for this module
// -----------------------------

void common_test(struct TestContext *tc);

#endif
