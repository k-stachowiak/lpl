/*
 * Copyright (C) 2020 Krzysztof Stachowiak
 *
 * This file is part of the Language Programming Language compiler.
 *
 * Language Programming Language is free software: you can redistribute it
 * and/or modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Language Programming Language compiler is distributed in the hope that it
 * will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "error.h"
#include "common.h"

#include <stdlib.h>

void error_init(struct ErrorContext *ec)
{
    ec->message = NULL;
}

void error_deinit(struct ErrorContext *ec)
{
    if (ec->message) {
        my_free(ec->message);
        ec->message = NULL;
    }
}

void error_throw(struct ErrorContext *ec, char *message)
{
    char *message_copy = NULL;
    str_append(message_copy, "%s", message);
    ec->message = message_copy;
    longjmp(ec->jump_buffer, 1);
}
