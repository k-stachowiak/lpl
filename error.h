/*
 * Copyright (C) 2020 Krzysztof Stachowiak
 *
 * This file is part of the Language Programming Language compiler.
 *
 * Language Programming Language is free software: you can redistribute it
 * and/or modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Language Programming Language compiler is distributed in the hope that it
 * will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef ERROR_H
#define ERROR_H

#include <setjmp.h>

#include "common.h"

struct ErrorContext {
    jmp_buf jump_buffer;
    char *message;
};

void error_init(struct ErrorContext *ec);
void error_deinit(struct ErrorContext *ec);
void error_throw(struct ErrorContext *ec, char *message);

#define error_throwf(ec, format, ...)                        \
    do {                                                     \
        char *message_copy = NULL;                           \
        str_append(message_copy, format, ##__VA_ARGS__);     \
        (ec)->message = message_copy;                        \
        longjmp((ec)->jump_buffer, 1);                       \
    } while(0)

#define error_catch(ec) setjmp((ec)->jump_buffer)

#endif
