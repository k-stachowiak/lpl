/*
 * Copyright (C) 2020 Krzysztof Stachowiak
 *
 * This file is part of the Language Programming Language compiler.
 *
 * Language Programming Language is free software: you can redistribute it
 * and/or modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Language Programming Language compiler is distributed in the hope that it
 * will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef PARSER_H
#define PARSER_H

#include "ast.h"
#include "test.h"
#include "common.h"
#include "illiterate-tokenizer/char_stream.h"

// SourceDebugInfo
// ===============
// Gathers information about the locations in the source code

struct SrcRange {
    size_t first;
    size_t last;
};

struct SourceDebugInfo {
    struct HashTable map;
};

struct SrcRange *sdi_get(struct SourceDebugInfo *sdi, struct AstNode *ast);
void sdi_debug_print(struct SourceDebugInfo *sdi, char *source, FILE *out);

// Parser
// ======
// This module is responsible for the semantic analysis of the source code.

struct ParseResult {
    char *error_message;
    struct AstNode *result;
    struct SourceDebugInfo *sdi;
};

void parse_result_deinit(struct ParseResult *pr);

struct ParseResult parse(struct TokCharStream *cs, bool gather_debug_info);

// Test procedure
// ==============

void parser_test(struct TestContext *tc);

#endif
