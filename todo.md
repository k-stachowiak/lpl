TODO
====

General
-------
* [ ] Implement a proper hash table
* [ ] Revise storage of the debug info (source -> AST -> binary offsets)
* [ ] Consider reimplementation of the shunting yard algorithm based on external hooks

Compiler
--------
* [ ] Consider adding non-optimizing generator and finish it first.
* [ ] Rework the resource acquisition scheme in the memory manager. Note, how many objects can
      hold the same register, and a single object can hold many registers. Is that handled
      properly?
* [ ] RBP indirect locations aren't really acquiring RBP
* [ ] Add pre-scanning phase to detect operator definitions, inject them into the tokenizer.
* [ ] Add procedure call
  * [ ] Let's talk about the stack frame... alignment? size?
* [ ] Generate ELF objects

Language
--------
* [ ] Consider writing tests specificly for the amd64 memory manager and code genereator.
* [ ] Multi-assignment => implement swap in terms of that
* [ ] Arrays
  * [ ] Pointer arithmetic
* [ ] Function calls
  * [ ] Recursive calls first? Seem self sufficient and easily testable in the JIT context
  * [ ] Inner procedures - as above - may be a good vehicle to implement and test function calls
        before doing any serious linking
* [ ] Implement type constraints, e.g. for the procedure arguments
* [ ] Add product and union types
* [ ] Consider adding other loop types:
  * [ ] loop - pool: repeats given command sequence N times
  * [ ] for - rof: repeats given command sequence for each element in a collection
