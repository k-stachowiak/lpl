/*
 * Copyright (C) 2020 Krzysztof Stachowiak
 *
 * This file is part of the Language Programming Language compiler.
 *
 * Language Programming Language is free software: you can redistribute it
 * and/or modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Language Programming Language compiler is distributed in the hope that it
 * will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "test.h"
#include "common.h"
#include "illiterate-tokenizer/char_stream.h"
#include "illiterate-tokenizer/buffered_char_stream.h"
#include "illiterate-tokenizer/token_stream.h"
#include "parser.h"
#include "ast.h"
#include "scanner.h"
#include "amd64_generator.h"
#include "illiterate-amd64-encoder/amd64_encoder.h"
#include "jit_amd64_linux.h"

#include <stdio.h>
#include <string.h>

static int cmd_test(int argc, char **argv)
{
    int i;
    bool verbose = false;
    struct TestContext *root;

    // 1. Evaluate CLI looking for options:
    for (i = 1; i < argc; ++i) {
        if (strcmp(argv[i], "-v") == 0) {
            verbose = true;
        }
    }

    // 2. Perform tests:
    root = tc_create_root(NULL);
    common_test(root);
    parser_test(root);
    ast_test(root);
    scanner_test(root);
    amd64_generator_test(root);
    jit_amd64_linux_test(root);

    if (amd64_enc_test() != 0) {
        tc_failure(root, "AMD64 encoder internal tests failed");
    }

    if (tok_cs_test() + tok_bcs_test() + tok_ts_test() != 0) {
        tc_failure(root, "Tokenizer internal tests failed");
    }

    // 3. Print report and release resources:
    tc_report(root, stdout, verbose);
    tc_destroy_root(root);

    return 0;
}

static int cmd_rex(int argc, char **argv)
{
    int rex;

    // 1. Evaluate CLI:
    if (argc != 2) {
        fprintf(stderr, "Missing argument\n");
        return 1;
    }

    // 2. Load the source file:
    if (sscanf(argv[1], "%x", &rex) != 1) {
        fprintf(stderr, "Failed parsing argument\n");
        return 1;
    }

    // 3. Analyze the value:
    printf("W: %d\n", (rex >> 3) & 1);
    printf("R: %d\n", (rex >> 2) & 1);
    printf("X: %d\n", (rex >> 1) & 1);
    printf("B: %d\n", (rex >> 0) & 1);

    return 0;
}

static int cmd_modrm(int argc, char **argv)
{
    int modrm;

    // 1. Evaluate CLI:
    if (argc != 2) {
        fprintf(stderr, "Missing argument\n");
        return 1;
    }

    // 2. Load the source file:
    if (sscanf(argv[1], "%x", &modrm) != 1) {
        fprintf(stderr, "Failed parsing argument\n");
        return 1;
    }

    // 3. Analyze the value:
    printf("mod: %d\n", (modrm >> 6) & 3);
    printf("reg: %d\n", (modrm >> 3) & 7);
    printf("r/m: %d\n", (modrm >> 0) & 7);

    return 0;
}

static int cmd_compile(int argc, char **argv)
{
    FILE *input;
    struct TokCharStream cs;
    struct ParseResult parse_result;

    // 1. Evaluate CLI:
    if (argc < 2) {
        fprintf(stderr, "Missing file to compile\n");
        return 1;
    }

    // 2. Load the source file:
    input = fopen(argv[1], "r");
    if (!input) {
        fprintf(stderr, "Failed loading file \"%s\"\n", argv[1]);
        return 1;
    }
    tok_cs_init_file(&cs, input);

    // 3. Parse the loaded file:
    parse_result = parse(&cs, true);
    if (!parse_result.result) {
        fprintf(
            stderr,
            "Failed parsing source: %s\n",
            parse_result.error_message);
        tok_cs_deinit(&cs);
        fclose(input);
        parse_result_deinit(&parse_result);
        return 1;
    }

    // 4. Release resources
    parse_result_deinit(&parse_result);
    tok_cs_deinit(&cs);
    fclose(input);

    return 0;
}

enum DumpMode {
    DM_AST = 0x1,
    DM_SCAN = 0x2
};

static int cmd_dump_impl(int argc, char **argv, enum DumpMode mode)
{
    int result = 1;
    FILE *input;
    struct TokCharStream cs;
    struct ParseResult pr;
    struct ProcedureScanResult psr;

    // 1. Evaluate CLI:
    if (argc < 2) {
        fprintf(stderr, "Missing file to dump\n");
        return 1;
    }

    // 2. Load the source file:
    input = fopen(argv[1], "r");
    if (!input) {
        fprintf(stderr, "Failed loading file \"%s\"\n", argv[1]);
        return 1;
    }
    tok_cs_init_file(&cs, input);

    // From now on, we have presistently acquired resources - source, but
    // there's more comming. Therefore, we only terminate the procedure by
    // a `goto end` instruction beyond this point.

    // 3. Parse the AST:
    pr = parse(&cs, true);
    if (!pr.result) {
        fprintf(
            stderr,
            "Failed parsing source: %s\n",
            pr.error_message);
        goto end;
    }

    if (mode & DM_AST) {
        // 4? Dump the AST into stdout:
        char *dump = ast_to_string_dot(pr.result);
        if (!dump) {
            fprintf(stderr, "Failed preparing dump\n");
            goto end;
        }
        printf("%s\n", dump);
        my_free(dump);
    }

    if (mode & DM_SCAN) {
        // 5? Dump the scan result into stdout:
        char *dump;
        psr = scan_procedures(pr.result);
        if (psr.error_message) {
            fprintf(stderr, "Failed scanning source: %s\n", psr.error_message);
            psr_deinit(&psr);
            goto end;
        }
        dump = psr_to_string(&psr);
        if (!dump) {
            fprintf(stderr, "Failed preparing dump\n");
            psr_deinit(&psr);
            goto end;
        }
        printf("%s\n", dump);
        my_free(dump);
        psr_deinit(&psr);
    }

    result = 0;

    // 5. Release resources:
end:
    parse_result_deinit(&pr);
    tok_cs_deinit(&cs);
    fclose(input);

    return result;
}

static int cmd_dump(int argc, char **argv)
{
    if (argc < 2) {
        fprintf(stderr, "Dump command requires argument\n");
        return 1;
    }

    if (strcmp(argv[1], "ast") == 0) {
        return cmd_dump_impl(--argc, ++argv, DM_AST);
    } else if (strcmp(argv[1], "scan") == 0) {
        return cmd_dump_impl(--argc, ++argv, DM_SCAN);
    } else {
        fprintf(stderr, "Unknown dump type: \"%s\"\n", argv[1]);
        return 1;
    }
}

int main(int argc, char **argv)
{
    if (argc < 2) {
        fprintf(stderr, "Incorrect CLI\n");
        return 1;
    }

    if (strcmp(argv[1], "test") == 0) {
        return cmd_test(--argc, ++argv);
    } else if (strcmp(argv[1], "rex") == 0) {
        return cmd_rex(--argc, ++argv);
    } else if (strcmp(argv[1], "modrm") == 0) {
        return cmd_modrm(--argc, ++argv);
    } else if (strcmp(argv[1], "dump") == 0) {
        return cmd_dump(--argc, ++argv);
    } else if (strcmp(argv[1], "compile") == 0) {
        return cmd_compile(--argc, ++argv);
    } else {
        fprintf(stderr, "Unknown command: \"%s\"\n", argv[1]);
        return 1;
    }
}
