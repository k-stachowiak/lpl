/*
 * Copyright (C) 2020 Krzysztof Stachowiak
 *
 * This file is part of the Language Programming Language compiler.
 *
 * Language Programming Language is free software: you can redistribute it
 * and/or modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Language Programming Language compiler is distributed in the hope that it
 * will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "amd64_generator.h"
#include "amd64_memory.h"
#include "illiterate-amd64-encoder/amd64_mnemonics.h"
#include "error.h"

#include <stdint.h>
#include <inttypes.h>
#include <stdbool.h>
#include <assert.h>
#include <inttypes.h>

// Generation result
// =================

static struct PGRConstant *const_destroy(struct PGRConstant *constant)
{
    struct PGRConstant *result = constant->next;
    while (constant->rip_offsets) {
        struct PGRRipOffsetList *rip_offset = constant->rip_offsets;
        constant->rip_offsets = constant->rip_offsets->next;
        my_free(rip_offset);
    }
    bb_destroy(constant->bb);
    my_free(constant);
    return result;
}

static struct PGRRipOffsetList *const_new_offset(struct PGRConstant *constant)
{
    // 1. Allocate:
    struct PGRRipOffsetList *result = my_malloc(sizeof(*result));

    // 2. Initialize:
    result->record.base = -1;
    result->record.offset = -1;
    result->record.size = -1;
    result->next = constant->rip_offsets;

    // 3. Register:
    constant->rip_offsets = result;

    // 4. Return:
    return result;
}

// The generation algorithm generates binary code that is a concrete realization
// of a given procedure, for the given argument types. Generation also
// determines the types of the returned values, which may or may not depend on
// the instantiating argument types.

static void pgr_init_success(
        struct ProcedureGenerationResult *pgr,
        struct DataTypeList *result_types,
        struct BinaryBuffer *bb,
        struct PGRConstant *constants)
{
    pgr->error_message = NULL;
    pgr->result_types = *result_types;
    pgr->bb = bb;
    pgr->constants = constants;
}

static void pgr_init_failure(
        struct ProcedureGenerationResult *pgr,
        char *error_message)
{
    pgr->error_message = error_message;
    pgr->result_types.data = NULL;
    pgr->result_types.size = pgr->result_types.cap = 0;
    pgr->bb = NULL;
    pgr->constants = NULL;
}

void pgr_deinit(struct ProcedureGenerationResult *pgr)
{
    while (pgr->constants) {
        pgr->constants = const_destroy(pgr->constants);
    }
    bb_destroy(pgr->bb);
    dtl_deinit(&pgr->result_types);
    my_free(pgr->error_message);
}

// Architecture specific operations
// ================================

char *amd64_mangle_name(char *base_name, struct DataTypeList *argument_list)
{
    int i;
    char *result = NULL;
    str_append(result, "%s", base_name);
    for (i = 0; i != argument_list->size; ++i) {
        str_append(result, "#%s", dt_name(argument_list->data[i]));
    }
    return result;
}

// Code generation
// ===============

// Assembly encoder to binary buffer binding
// -----------------------------------------

static void bb_put(void *bbv, uint8_t byte)
{
    struct BinaryBuffer *bb = (struct BinaryBuffer*)bbv;
    bb_append_1(bb, byte);
}

static int bb_offset(void *bbv)
{
    struct BinaryBuffer *bb = (struct BinaryBuffer*)bbv;
    return bb->size;
}

// Context management
// ------------------

struct IffiGenerationContext;
struct DoodGenerationContext;
struct CallGenerationContext;

struct GenerationContext {
    bool gather_debug_info;
    struct SourceDebugInfo *sdi;
    struct ErrorContext error_context;
    struct AMD64Memory mem;
    struct AMD64Object *rbp;
    struct AMD64Object *rsp;
    size_t preservation_code_offset;
    uint8_t *reversion_buffer;
    int reversion_buffer_size;
    uint64_t reg_write_map;
    struct BinaryBuffer *bb;
    struct AMD64EncContext aec;
    struct DataTypeList result_types;
    struct IffiGenerationContext *igc_garbage;
    struct DoodGenerationContext *dgc_garbage;
    struct CallGenerationContext *cgc_garbage;
    struct PGRConstant *constants;
};

static struct PGRConstant *gc_const_find(struct GenerationContext *gc,
                                         uint8_t *data,
                                         int size) {
    struct PGRConstant *result = gc->constants;
    while (result) {
        if (size == result->bb->size &&
            memcmp(result->bb->data, data, size) == 0) {
            break;
        }
        result = result->next;
    }
    return result;
}

static struct PGRConstant *gc_const_push(struct GenerationContext *gc) {

    // Allocate:
    struct PGRConstant *result = my_malloc(sizeof(*result));

    // Initialize:
    result->bb = bb_create();
    result->rip_offsets = NULL;

    // Register:
    result->next = gc->constants;
    gc->constants = result;

    // Report:
    return result;
}

static void gc_pop_igc(struct GenerationContext *gc,
                       struct IffiGenerationContext *igc);
static void gc_pop_dgc(struct GenerationContext *gc,
                       struct DoodGenerationContext *dgc);
static void gc_pop_cgc(struct GenerationContext *gc,
                       struct CallGenerationContext *cgc);

static void gc_init(struct GenerationContext *gc, struct SourceDebugInfo *sdi)
{
    gc->sdi = sdi;
    error_init(&gc->error_context);
    amd64_mem_init(&gc->mem);
    gc->rbp = amd64_mem_create_reg_word(&gc->mem, AMD64_REG_BP);
    gc->rsp = amd64_mem_create_reg_word(&gc->mem, AMD64_REG_SP);
    gc->reg_write_map = 0;
    gc->reversion_buffer = NULL;
    gc->reversion_buffer_size = 0;
    gc->bb = bb_create();
    amd64_ec_init_generic(&gc->aec, gc->bb, bb_offset, bb_put);
    dtl_init(&gc->result_types);
    gc->igc_garbage = NULL;
    gc->dgc_garbage = NULL;
    gc->cgc_garbage = NULL;
    gc->constants = NULL;
}

static struct ProcedureGenerationResult gc_deinit_success(struct GenerationContext *gc)
{
    struct ProcedureGenerationResult result;
    pgr_init_success(&result, &gc->result_types, gc->bb, gc->constants);

    gc->constants = NULL;
    while (gc->cgc_garbage) {
        gc_pop_cgc(gc, gc->cgc_garbage);
    }
    while (gc->dgc_garbage) {
        gc_pop_dgc(gc, gc->dgc_garbage);
    }
    while (gc->igc_garbage) {
        gc_pop_igc(gc, gc->igc_garbage);
    }
    if (gc->reversion_buffer) {
        my_free(gc->reversion_buffer);
        gc->reversion_buffer = NULL;
    }
    gc->reversion_buffer_size = 0;
    amd64_mem_dispose(&gc->mem, gc->rsp);
    amd64_mem_dispose(&gc->mem, gc->rbp);
    amd64_mem_deinit(&gc->mem);
    error_deinit(&gc->error_context);

    return result;
}

static struct ProcedureGenerationResult gc_deinit_failure(struct GenerationContext *gc)
{
    struct ProcedureGenerationResult result;
    pgr_init_failure(&result, str_copy(gc->error_context.message));

    while (gc->constants) {
        gc->constants = const_destroy(gc->constants);
    }
    while (gc->cgc_garbage) {
        gc_pop_cgc(gc, gc->cgc_garbage);
    }
    while (gc->dgc_garbage) {
        gc_pop_dgc(gc, gc->dgc_garbage);
    }
    while (gc->igc_garbage) {
        gc_pop_igc(gc, gc->igc_garbage);
    }
    dtl_deinit(&gc->result_types);
    bb_destroy(gc->bb);
    if (gc->reversion_buffer) {
        my_free(gc->reversion_buffer);
        gc->reversion_buffer = NULL;
    }
    gc->reversion_buffer_size = 0;
    amd64_mem_dispose(&gc->mem, gc->rsp);
    amd64_mem_dispose(&gc->mem, gc->rbp);
    amd64_mem_deinit(&gc->mem);
    error_deinit(&gc->error_context);

    return result;
}

static void gc_on_write_reg(struct GenerationContext *gc, enum AMD64Register reg)
{
    gc->reg_write_map |= reg;
}

static void gc_on_write(struct GenerationContext *gc, struct AMD64Location *dst)
{
    if (dst->type == AMD64_LT_REGISTER_GP) {
        gc->reg_write_map |= amd64_reg_from_gpreg(dst->gpreg.gpreg);

    } else if (dst->type == AMD64_LT_REGISTER_SSE) {
        gc->reg_write_map |= amd64_reg_from_ssereg(dst->ssereg.ssereg);

    } else if (dst->type == AMD64_LT_REGISTER_INDIRECT) {
        gc->reg_write_map |= amd64_reg_from_ireg(dst->ireg.ireg);

    } else if (dst->type == AMD64_LT_MEMORY_SIB) {
        gc->reg_write_map |= amd64_reg_from_sreg(dst->msib.base);
        gc->reg_write_map |= amd64_reg_from_sreg(dst->msib.index);
    }
}

// Generation time machine
// -----------------------

static void gc_revert(struct GenerationContext *gc,
                      int offset)
{
    if (gc->reversion_buffer) {
        error_throw(&gc->error_context,
                    "Nested reversion not supported");
    }

    gc->reversion_buffer_size = gc->bb->size - offset;
    if (gc->reversion_buffer_size < 0) {
        error_throw(&gc->error_context,
                    "Revert requested beyond the code start");
    }

    gc->reversion_buffer = my_malloc(gc->reversion_buffer_size);
    memcpy(gc->reversion_buffer,
           gc->bb->data + offset,
           gc->reversion_buffer_size);
    gc->bb->size = offset;
}

static void gc_restore(struct GenerationContext *gc)
{
    int i;

    if (!gc->reversion_buffer) {
        error_throw(&gc->error_context,
                    "Restoration requested without prior reversion");
    }

    for (i = 0; i != gc->reversion_buffer_size; ++i) {
        bb_append_1(gc->bb, gc->reversion_buffer[i]);
    }

    if (gc->reversion_buffer) {
        my_free(gc->reversion_buffer);
        gc->reversion_buffer = NULL;
    }
    gc->reversion_buffer_size = 0;
}

// Error detecting encoder API wrappers
// ------------------------------------

static void generate_ret(struct GenerationContext *gc)
{
    if (!amd64_enc_ret(&gc->aec)) {
        error_throwf(&gc->error_context,
                     "Failed generating a RET instruction: %s",
                     gc->aec.error_message);
    }
}

static void generate_syscall(struct GenerationContext *gc)
{
    if (!amd64_enc_syscall(&gc->aec)) {
        error_throwf(&gc->error_context,
                     "Failed generating a SYSCALL instruction: %s",
                     gc->aec.error_message);
    }
}

static void generate_inc(struct GenerationContext *gc,
                         struct AMD64Location operand)
{
    if (!amd64_enc_inc(&gc->aec, operand)) {
        error_throwf(&gc->error_context,
                     "Failed generating a INC instruction: %s",
                     gc->aec.error_message);
    }
    gc_on_write(gc, &operand);
}

static void generate_neg(struct GenerationContext *gc,
                         struct AMD64Location operand)
{
    if (!amd64_enc_neg(&gc->aec, operand)) {
        error_throwf(&gc->error_context,
                     "Failed generating a NEG instruction: %s",
                     gc->aec.error_message);
    }
    gc_on_write(gc, &operand);
}

static void generate_push(struct GenerationContext *gc,
                          struct AMD64Location operand)
{
    if (!amd64_enc_push(&gc->aec, operand)) {
        error_throwf(&gc->error_context,
                     "Failed generating a PUSH instruction: %s",
                     gc->aec.error_message);
    }
    gc_on_write_reg(gc, AMD64_REG_SP);
}

static void generate_pop(struct GenerationContext *gc,
                         struct AMD64Location operand)
{
    if (!amd64_enc_pop(&gc->aec, operand)) {
        error_throwf(&gc->error_context,
                     "Failed generating a POP instruction: %s",
                     gc->aec.error_message);
    }
    gc_on_write(gc, &operand);
    gc_on_write_reg(gc, AMD64_REG_SP);
}

static void generate_mov(struct GenerationContext *gc,
                         struct AMD64Location src,
                         struct AMD64Location dst)
{
    if (!amd64_enc_mov(&gc->aec, src, dst)) {
        error_throwf(&gc->error_context,
                     "Failed generating a MOV instruction: %s",
                     gc->aec.error_message);
    }
    gc_on_write(gc, &dst);
}

static void generate_movd(struct GenerationContext *gc,
                          struct AMD64Location src,
                          struct AMD64Location dst)
{
    if (!amd64_enc_movd(&gc->aec, src, dst)) {
        error_throwf(&gc->error_context,
                     "Failed generating a MOVD instruction: %s",
                     gc->aec.error_message);
    }
    gc_on_write(gc, &dst);
}

static void generate_movsd(struct GenerationContext *gc,
                           struct AMD64Location src,
                           struct AMD64Location dst)
{
    if (!amd64_enc_movsd(&gc->aec, src, dst)) {
        error_throwf(&gc->error_context,
                     "Failed generating a MOVSD instruction: %s",
                     gc->aec.error_message);
    }
    gc_on_write(gc, &dst);
}

static void generate_movzx(struct GenerationContext *gc,
                           struct AMD64Location src,
                           struct AMD64Location dst)
{
    if (!amd64_enc_movzx(&gc->aec, src, dst)) {
        error_throwf(&gc->error_context,
                     "Failed generating a MOVZX instruction: %s",
                     gc->aec.error_message);
    }
    gc_on_write(gc, &dst);
}

static void generate_add(struct GenerationContext *gc,
                         struct AMD64Location src,
                         struct AMD64Location dst)
{
    if (!amd64_enc_add(&gc->aec, src, dst)) {
        error_throwf(&gc->error_context,
                     "Failed generating an ADD instruction: %s",
                     gc->aec.error_message);
    }
    gc_on_write(gc, &dst);
}

static void generate_sub(struct GenerationContext *gc,
                         struct AMD64Location src,
                         struct AMD64Location dst)
{
    if (!amd64_enc_sub(&gc->aec, src, dst)) {
        error_throwf(&gc->error_context,
                     "Failed generating a SUB instruction: %s",
                     gc->aec.error_message);
    }
    gc_on_write(gc, &dst);
}

static void generate_xor(struct GenerationContext *gc,
                         struct AMD64Location src,
                         struct AMD64Location dst)
{
    if (!amd64_enc_xor(&gc->aec, src, dst)) {
        error_throwf(&gc->error_context,
                     "Failed generating a XOR instruction: %s",
                     gc->aec.error_message);
    }
    gc_on_write(gc, &dst);
}

static void generate_cmp(struct GenerationContext *gc,
                         struct AMD64Location src,
                         struct AMD64Location dst)
{
    if (!amd64_enc_cmp(&gc->aec, src, dst)) {
        error_throwf(&gc->error_context,
                     "Failed generating a CMP instruction: %s",
                     gc->aec.error_message);
    }
}

static void generate_idiv(struct GenerationContext *gc,
                          struct AMD64Location operand)
{
    if (!amd64_enc_idiv(&gc->aec, operand)) {
        error_throwf(&gc->error_context,
                     "Failed generating a IDIV instruction: %s",
                     gc->aec.error_message);
    }
    gc_on_write_reg(gc, AMD64_REG_AX);
    if (operand.size != AMD64_LS_8_BIT) {
        gc_on_write_reg(gc, AMD64_REG_DX);
    }
}

static void generate_imul2(struct GenerationContext *gc,
                           struct AMD64Location src,
                           struct AMD64Location dst)
{
    // Note: we swap the operands intentionally here, as the API of the IMUL
    //       generator takes, respectively, the register and the reg/mem
    //       operands, which are, again respectively, the destination and the
    //       source for the instruction.
    if (!amd64_enc_imul(&gc->aec, 2, dst, src)) {
        error_throwf(&gc->error_context,
                     "Failed generating a 2-arg IMUL instruction: %s",
                     gc->aec.error_message);
    }
}

static void generate_imul3(struct GenerationContext *gc,
                           struct AMD64Location reg,
                           struct AMD64Location rm,
                           struct AMD64Location imm)
{
    if (!amd64_enc_imul(&gc->aec, 3, reg, rm, imm)) {
        error_throwf(&gc->error_context,
                     "Failed generating a 3-arg IMUL instruction: %s",
                     gc->aec.error_message);
    }
}

static void generate_lea(struct GenerationContext *gc,
                         struct AMD64Location src,
                         struct AMD64Location dst)
{
    if (!amd64_enc_lea(&gc->aec, src, dst)) {
        error_throwf(&gc->error_context,
                     "Failed generating a LEA instruction: %s",
                     gc->aec.error_message);
    }
    gc_on_write(gc, &dst);
}

static void generate_addsd(struct GenerationContext *gc,
                           struct AMD64Location src,
                           struct AMD64Location dst)
{
    if (!amd64_enc_addsd(&gc->aec, src, dst)) {
        error_throwf(&gc->error_context,
                     "Failed generating an ADDSD instruction: %s",
                     gc->aec.error_message);
    }
    gc_on_write(gc, &dst);
}

static void generate_subsd(struct GenerationContext *gc,
                           struct AMD64Location src,
                           struct AMD64Location dst)
{
    if (!amd64_enc_subsd(&gc->aec, src, dst)) {
        error_throwf(&gc->error_context,
                     "Failed generating a SUBSD instruction: %s",
                     gc->aec.error_message);
    }
    gc_on_write(gc, &dst);
}

static void generate_mulsd(struct GenerationContext *gc,
                           struct AMD64Location src,
                           struct AMD64Location dst)
{
    if (!amd64_enc_mulsd(&gc->aec, src, dst)) {
        error_throwf(&gc->error_context,
                     "Failed generating a MULSD instruction: %s",
                     gc->aec.error_message);
    }
    gc_on_write(gc, &dst);
}

static void generate_divsd(struct GenerationContext *gc,
                           struct AMD64Location src,
                           struct AMD64Location dst)
{
    if (!amd64_enc_divsd(&gc->aec, src, dst)) {
        error_throwf(&gc->error_context,
                     "Failed generating a DIVSD instruction: %s",
                     gc->aec.error_message);
    }
    gc_on_write(gc, &dst);
}

static void generate_comisd(struct GenerationContext *gc,
                            struct AMD64Location src,
                            struct AMD64Location dst)
{
    if (!amd64_enc_comisd(&gc->aec, src, dst)) {
        error_throwf(&gc->error_context,
                     "Failed generating a COMISD instruction: %s",
                     gc->aec.error_message);
    }
}

// Defered jump generation
// -----------------------

struct JumpRecord {
    int address_offset; // where the address value is stored in the code
    int base_offset;    // from where the jump offset is counted
};

static struct JumpRecord generate_jcc_32(struct GenerationContext *gc,
                                         char *op_name,
                                         bool (*encoder)(struct AMD64EncContext *,
                                                         struct AMD64RelOffset))
{
    struct JumpRecord jr;
    struct AMD64RelOffset dummy_offset = {
        .size = AMD64_ROS_32,
        .value32 = 0x0F0E0E0B
    };

    // Now, generically encode the jump with a dummy address to be filled in
    // later:
    if (!encoder(&gc->aec, dummy_offset)) {
        error_throwf(&gc->error_context,
                     "Failed generating a %s instruction",
                     op_name);
    }

    // Let's record the offset under which the address was encoded:
    jr.address_offset = gc->aec.last_address_entry.offset;
    assert(gc->aec.last_address_entry.size == 4);

    // Base offset (from where the jump is counted) is right after the instruction:
    jr.base_offset = gc->bb->size;

    return jr;
}

static struct JumpRecord generate_jnae_32(struct GenerationContext *gc)
{
    return generate_jcc_32(gc, "JNAE", amd64_enc_jnae);
}

static struct JumpRecord generate_jnb_32(struct GenerationContext *gc)
{
    return generate_jcc_32(gc, "JNB", amd64_enc_jnb);
}

static struct JumpRecord generate_je_32(struct GenerationContext *gc)
{
    return generate_jcc_32(gc, "JE", amd64_enc_je);
}

static struct JumpRecord generate_jne_32(struct GenerationContext *gc)
{
    return generate_jcc_32(gc, "JNE", amd64_enc_jne);
}

static struct JumpRecord generate_jna_32(struct GenerationContext *gc)
{
    return generate_jcc_32(gc, "JNA", amd64_enc_jna);
}

static struct JumpRecord generate_jnbe_32(struct GenerationContext *gc)
{
    return generate_jcc_32(gc, "JNBE", amd64_enc_jnbe);
}

static struct JumpRecord generate_jnge_32(struct GenerationContext *gc)
{
    return generate_jcc_32(gc, "JL", amd64_enc_jl);
}

static struct JumpRecord generate_jnl_32(struct GenerationContext *gc)
{
    return generate_jcc_32(gc, "JNL", amd64_enc_jnl);
}

static struct JumpRecord generate_jng_32(struct GenerationContext *gc)
{
    return generate_jcc_32(gc, "JNG", amd64_enc_jng);
}

static struct JumpRecord generate_jnle_32(struct GenerationContext *gc)
{
    return generate_jcc_32(gc, "JG", amd64_enc_jg);
}

static struct JumpRecord generate_jmp_32(struct GenerationContext *gc)
{
    struct JumpRecord jr;
    struct AMD64RelOffset dummy_offset = {
        .size = AMD64_ROS_32,
        .value32 = 0x0F0E0E0B
    };

    if (!amd64_enc_jmp_rel(&gc->aec, dummy_offset)) {
        error_throwf(&gc->error_context, "Failed generating a JMP instruction");
    }

    // Let's record the offset under which the address was encoded:
    jr.address_offset = gc->aec.last_address_entry.offset;
    assert(gc->aec.last_address_entry.size == 4);

    // Base offset (from where the jump is counted) is right after the instruction:
    jr.base_offset = gc->bb->size;

    return jr;
}

static void jr_set(struct GenerationContext *gc,
                   struct JumpRecord *jr,
                   int32_t target)
{
    int32_t offset = target - jr->base_offset;
    memcpy(bb_at_offset(gc->bb, jr->address_offset), &offset, sizeof(offset));
}

static void jr_set_to_current(struct GenerationContext *gc,
                              struct JumpRecord *jr)
{
    jr_set(gc, jr, bb_current_offset(gc->bb));
}

// High level semantics
// --------------------

static void generate_eviction(struct GenerationContext *gc,
                              enum AMD64Register reg);

static struct AMD64Object *create_compatible_destination(
        struct GenerationContext *gc,
        struct DataType *data_type)
{
    enum AMD64Register reg =
        amd64_mem_find_free_compatible_register(&gc->mem, data_type);

    if (reg != AMD64_REG_MAX) {
        return amd64_mem_create_reg(&gc->mem, reg, dt_copy(data_type));

    } else {
        return amd64_mem_create_stack_object(&gc->mem, dt_copy(data_type));

    }
}

// ### RIP-relative allocated constants handling

static struct AMD64Object *procure_rip_object(struct GenerationContext *gc,
                                              struct DataType *data_type,
                                              uint8_t *buffer,
                                              int size)
{
    struct PGRRipOffsetList *rip_offset;

    // 1. See if we've already got a constant with the exact same binary
    //    footprint. If not, register and fill in a new one:
    struct PGRConstant *constant = gc_const_find(gc, buffer, size);
    if (!constant) {
        constant = gc_const_push(gc);
        bb_append_n(constant->bb, buffer, size);
    }

    // 2. Register a ner RIP offset for the constant:
    rip_offset = const_new_offset(constant);

    // 3. Next, let's create a new object with a RIP-relative location, that can
    //    be given later, after we've actually placed the constants in the code
    //    buffer:
    return amd64_mem_create_object(
        &gc->mem,
        data_type,
        amd64_loc_make_deferred_memory_rip_offset(
            amd64_loc_size_from_bytes(dt_effective_size(data_type)),
            &rip_offset->record));
}

static struct AMD64Object *procure_rip_double(struct GenerationContext *gc,
                                              double value)
{
    return
        procure_rip_object(
            gc, dt_create_primitive(DT_PRIM_DOUBLE), (uint8_t *)&value, 8);
}

static struct AMD64Object *procure_rip_cstring(struct GenerationContext *gc,
                                               char *value)
{
    return procure_rip_object(gc,
                              dt_create_cstring(),
                              (uint8_t *)value,
                              strlen(value) + 1);
}

// ### Type promotions

static struct AMD64Object *promote_word_to_double(struct GenerationContext *gc,
                                                  struct AMD64Object *obj)
{
    if (obj->data_type->type != DT_PRIMITIVE ||
        obj->data_type->primitive != DT_PRIM_WORD) {
        ABORT("Algorythmic corruption");
    }

    if (obj->location.type == AMD64_LT_IMMEDIATE) {
        struct AMD64Object *new_obj =
            create_compatible_destination(gc, obj->data_type);
        amd64_obj_swap(obj, new_obj);
        amd64_mem_dispose(&gc->mem, new_obj);
    }

    if (amd64_loc_is_gp_rm(&obj->location)) {
        enum AMD64Register new_reg =
            amd64_mem_find_free_compatible_register(&gc->mem, obj->data_type);

        struct AMD64Location new_loc;
        if (new_reg == AMD64_REG_MAX) {
            new_reg = AMD64_REG_XMM0;
            generate_eviction(gc, new_reg);
        }
        new_loc = amd64_loc_make_register_sse(amd64_reg_to_ssereg(new_reg));
        generate_movd(gc, obj->location, new_loc);
        amd64_mem_dispose(&gc->mem, obj);
        return amd64_mem_create_object(
            &gc->mem, dt_create_primitive(DT_PRIM_DOUBLE), new_loc);

    } else {
        ABORT("Algorythmic corruption");

    }
}

static struct AMD64Object *promote_byte_to_word(struct GenerationContext *gc,
                                                struct AMD64Object *obj)
{
    if (obj->data_type->type != DT_PRIMITIVE ||
        obj->data_type->primitive != DT_PRIM_BYTE) {
        ABORT("Algorythmic corruption");
    }

    if (obj->location.type == AMD64_LT_IMMEDIATE) {
        obj->data_type->primitive = DT_PRIM_WORD;
        obj->location.size = AMD64_LS_64_BIT;
        return obj;

    } else if (obj->location.type == AMD64_LT_REGISTER_GP) {
        struct AMD64Location new_loc = obj->location;
        new_loc.size = AMD64_LS_64_BIT;
        generate_movzx(gc, obj->location, new_loc);
        obj->data_type->primitive = DT_PRIM_WORD;
        obj->location = new_loc;
        return obj;

    } else if (amd64_loc_is_memory(&obj->location)) {
        enum AMD64Register new_reg =
            amd64_mem_find_free_compatible_register(&gc->mem, obj->data_type);

        struct AMD64Location new_loc;
        if (new_reg == AMD64_REG_MAX) {
            new_reg = AMD64_REG_AX;
            generate_eviction(gc, new_reg);
        }
        new_loc = amd64_loc_make_register_gp(obj->location.size,
                                             amd64_reg_to_gpreg(new_reg));
        generate_movzx(gc, obj->location, new_loc);
        amd64_mem_dispose(&gc->mem, obj);
        return amd64_mem_create_object(
            &gc->mem, dt_create_primitive(DT_PRIM_WORD), new_loc);

    } else {
        ABORT("Algorythmic corruption");

    }
}

static struct AMD64Object *promote_byte_to_double(struct GenerationContext *gc,
                                                  struct AMD64Object *obj)
{
    // Cannot MOVD from byte location, so we must first MOV from 8-bit to
    // a 32- or 64- bit location. Since we already have a byte-word promotion
    // algorithm, and we aren't saving space by going through a 32-bit register
    // rather than a 64-bit one, let's reuse the other promotion code here.
    return promote_word_to_double(gc, promote_byte_to_word(gc, obj));
}

// ### Immediate promotion/demotion:

static void try_adjust_immediate_type(
        struct GenerationContext *gc,
        struct AMD64Object *immediate_obj,
        struct DataType *dst_type)
{
    struct AMD64ImmediateLocation *imm = &immediate_obj->location.imm;
    struct DataType *imm_type = immediate_obj->data_type;

    // 1. Check if there is anything that we can do:
    if (imm_type->type != DT_PRIMITIVE || dst_type->type != DT_PRIMITIVE) {
        return;
    }

    // At this pobjnt both dst and the immediate are of primitive type.

    // 2. Adjust:
    if (imm_type->primitive == DT_PRIM_BYTE &&
        dst_type->primitive == DT_PRIM_WORD) {

        // 2.a) Promote byte immediate to word:
        immediate_obj = promote_byte_to_word(gc, immediate_obj);

    } else if (imm_type->primitive == DT_PRIM_WORD &&
               dst_type->primitive == DT_PRIM_BYTE &&
               imm->value <= INT8_MAX &&
               imm->value >= INT8_MIN) {

        // 2.b) Demote word immediate to byte:
        immediate_obj->data_type->primitive = DT_PRIM_BYTE;
        immediate_obj->location.size =
            amd64_loc_size_from_bytes(
                dt_effective_size(immediate_obj->data_type));

    } else if ((imm_type->primitive == DT_PRIM_BYTE ||
                imm_type->primitive == DT_PRIM_WORD) &&
               dst_type->primitive == DT_PRIM_DOUBLE) {

        // 2.c) Promote integral immediate to double:
        struct AMD64Object *rip_obj = procure_rip_double(gc, imm->value);
        amd64_obj_swap(rip_obj, immediate_obj);
        amd64_mem_dispose(&gc->mem, rip_obj);
    }
}

// ### Abstract commands

static void generate_copy(struct GenerationContext *gc,
                          struct AMD64Object *src,
                          struct AMD64Object *dst)
{
    // 1. We really cannot copy into an immediate, and there's nothing we can do
    //    to work around this:
    if (dst->location.type == AMD64_LT_IMMEDIATE) {
        error_throwf(
            &gc->error_context,
            "Cannot copy into an immediate");
    }

    // 2. Make sure the types are fine:
    if (!dt_equal(src->data_type, dst->data_type)) {
        error_throwf(
            &gc->error_context,
            "Cannot copy when types don't match");
    }
    if (src->data_type->type == DT_BUFFER) {
        error_throwf(
            &gc->error_context,
            "Copy of buffer objects not implemented yet");
    }

    // 3. Check if we can perform a MOV-like copy btween the given locations:
    if ((amd64_loc_is_memory(&src->location) &&
         amd64_loc_is_memory(&dst->location))
        ||
        (src->location.type == AMD64_LT_IMMEDIATE &&
         src->location.size == AMD64_LS_64_BIT &&
         amd64_loc_is_memory(&dst->location))) {

        // 3.a) We cannot copy:
        //      - btween two memory locations,
        //      - from a 64-bit immediate to a memory location,
        //      so in such cases, we copy through an intermediate register:
        enum AMD64Register temp_reg;
        struct AMD64Location temp_loc;

        // Note: The SSE variant is pretty close to the GP one, but they differ
        //       in subtle and non-local ways. Therefore, they have been
        //       implemented separately and explicitly, without forcing any
        //       sharing of code between them:
        if (src->data_type->primitive == DT_PRIM_DOUBLE) {

            temp_reg = amd64_mem_find_free_ssereg(&gc->mem);
            if (temp_reg == AMD64_REG_MAX) {
                temp_reg = AMD64_REG_XMM0;
                generate_eviction(gc, temp_reg);
            }
            temp_loc = amd64_loc_make_register_sse(amd64_reg_to_ssereg(temp_reg));

            generate_movsd(gc, src->location, temp_loc);
            generate_movsd(gc, temp_loc, dst->location);

        } else {

            temp_reg = amd64_mem_find_free_gpreg(&gc->mem);
            if (temp_reg == AMD64_REG_MAX) {
                temp_reg = AMD64_REG_AX;
                generate_eviction(gc, temp_reg);
            }
            temp_loc = amd64_loc_make_register_gp(
                amd64_loc_size_from_bytes(dt_effective_size(src->data_type)),
                amd64_reg_to_gpreg(temp_reg));

            generate_mov(gc, src->location, temp_loc);
            generate_mov(gc, temp_loc, dst->location);
        }

    } else {
        // 3.b) Otherwise, we select a proper MOV-like command and use it to
        //      perform a direct copy:
        if (src->data_type->primitive == DT_PRIM_DOUBLE) {
            generate_movsd(gc, src->location, dst->location);
        } else {
            generate_mov(gc, src->location, dst->location);
        }

    }

}

static void generate_eviction(
        struct GenerationContext *gc,
        enum AMD64Register reg)
{
    struct AMD64Object *src, *dst;

    src = amd64_mem_find_by_reg(&gc->mem, reg);
    if (!src) {
        ABORT("Algorithmic corruption");
    }

    dst = amd64_mem_create_stack_object(&gc->mem, dt_copy(src->data_type));
    generate_copy(gc, src, dst);
    amd64_loc_swap(&src->location, &dst->location);
    amd64_mem_dispose(&gc->mem, dst);
}

static struct AMD64Object *generate_gp_reg_duplicate(
        struct GenerationContext *gc,
        struct AMD64Object *obj,
        enum AMD64GPRegister fallback_gp_reg)
{
    struct AMD64Object *new_obj;
    enum AMD64Register fallback_reg = amd64_reg_from_gpreg(fallback_gp_reg);
    enum AMD64Register free_reg = amd64_mem_find_free_gpreg(&gc->mem);
    if (free_reg == AMD64_REG_MAX) {
        generate_eviction(gc, fallback_reg);
        free_reg = fallback_reg;
    }

    new_obj = amd64_mem_create_reg(&gc->mem, free_reg, dt_copy(obj->data_type));
    generate_copy(gc, obj, new_obj);

    return new_obj;
}

static struct AMD64Object *generate_sse_reg_duplicate(
        struct GenerationContext *gc,
        struct AMD64Object *obj,
        enum AMD64SSERegister fallback_sse_reg)
{
    struct AMD64Object *new_obj;
    enum AMD64Register fallback_reg = amd64_reg_from_ssereg(fallback_sse_reg);
    enum AMD64Register free_reg = amd64_mem_find_free_ssereg(&gc->mem);
    if (free_reg == AMD64_REG_MAX) {
        generate_eviction(gc, fallback_reg);
        free_reg = fallback_reg;
    }

    new_obj = amd64_mem_create_reg(&gc->mem, free_reg, dt_copy(obj->data_type));
    generate_copy(gc, obj, new_obj);

    return new_obj;
}

static struct AMD64Object *generate_reg_mem_duplicate(struct GenerationContext *gc,
                                                      struct AMD64Object *obj)
{
    struct AMD64Object *new_obj =
        create_compatible_destination(gc, obj->data_type);
    generate_copy(gc, obj, new_obj);
    return new_obj;
}

static struct AMD64Object *generate_gp_reg_promotion(
        struct GenerationContext *gc,
        struct AMD64Object *obj,
        enum AMD64GPRegister fallback_gp_reg)
{
    struct AMD64Object *duplicate =
        generate_gp_reg_duplicate(gc, obj, fallback_gp_reg);
    amd64_mem_dispose(&gc->mem, obj);
    return duplicate;
}

static struct AMD64Object *generate_sse_reg_promotion(
        struct GenerationContext *gc,
        struct AMD64Object *obj,
        enum AMD64SSERegister fallback_sse_reg)
{
    struct AMD64Object *duplicate =
        generate_sse_reg_duplicate(gc, obj, fallback_sse_reg);
    amd64_mem_dispose(&gc->mem, obj);
    return duplicate;
}

static struct AMD64Object *generate_reg_promotion(
        struct GenerationContext *gc,
        struct AMD64Object *obj,
        enum AMD64Register fallback_reg)
{
    assert(obj->data_type->type == DT_PRIMITIVE);
    if (obj->data_type->primitive == DT_PRIM_DOUBLE) {
        return generate_sse_reg_promotion(
            gc, obj, amd64_reg_to_ssereg(fallback_reg));
    } else {
        return generate_gp_reg_promotion(
            gc, obj, amd64_reg_to_gpreg(fallback_reg));
    }
}

static struct AMD64Object *generate_reg_mem_promotion(struct GenerationContext *gc,
                                                      struct AMD64Object *obj)
{
    struct AMD64Object *duplicate = generate_reg_mem_duplicate(gc, obj);
    amd64_mem_dispose(&gc->mem, obj);
    return duplicate;
}

// ### Unary commands

static struct AMD64Object *generate_negation(struct GenerationContext *gc,
                                             struct AMD64Object *operand)
{
    // Prepare constant for the case of floating point negation:
    struct AMD64Object *neg_one;

    // Only allow negation on primitive types:
    if (operand->data_type->type != DT_PRIMITIVE) {
        error_throw(
            &gc->error_context,
            "Can only negate a primitive type");
    }

    // AMD64 negation must be implemented in a destructive way, as for all the
    // data types we handle here, the instruction modify the negated operand.
    // Therefore, in case the operand is not a temporary, i.e. can be found in
    // the scope, let's create a duplicate and operate on that.
    if (amd64_mem_find_by_obj(&gc->mem, operand)) {
        struct AMD64Object *duplicate = generate_reg_mem_duplicate(gc, operand);
        amd64_obj_swap(operand, duplicate);
        amd64_mem_dispose(&gc->mem, duplicate);
    }

    // We cannot operate in-place on an immediate, so if the operand is one,
    // move it into a register.
    if (operand->location.type == AMD64_LT_IMMEDIATE) {
        operand = generate_reg_promotion(
            gc,
            operand,
            operand->data_type->primitive == DT_PRIM_DOUBLE
            ? AMD64_REG_XMM0 : AMD64_REG_AX);
    }

    // Main dispatch selecting the proper negation instruction:
    switch (operand->data_type->primitive) {
    case DT_PRIM_UNIT:
        error_throw(
            &gc->error_context,
            "Can not negate a unit value");
        break;

    case DT_PRIM_BOOLEAN:
        generate_xor(gc, IMM8(1), operand->location);
        break;

    case DT_PRIM_BYTE:
    case DT_PRIM_WORD:
        generate_neg(gc, operand->location);
        break;

    case DT_PRIM_DOUBLE:
        neg_one = procure_rip_double(gc, -1.0);
        generate_mulsd(gc, neg_one->location, operand->location);
        amd64_mem_dispose(&gc->mem, neg_one);
        break;
    }

    return operand;
}

static struct AMD64Object *generate_increment(struct GenerationContext *gc,
                                              struct AMD64Object *operand)
{
    // Prepare constant for the case of floating point increment:
    struct AMD64Object *one;

    // Only allow negation on primitive types:
    if (operand->data_type->type != DT_PRIMITIVE) {
        error_throw(
            &gc->error_context,
            "Can only negate a primitive type");
    }

    // We need to save the operand's value, if it's a variable in the current
    // scope as all the possible increment implementations will overwrite the
    // original value.
    if (amd64_mem_find_by_obj(&gc->mem, operand)) {
        struct AMD64Object *duplicate = generate_reg_mem_duplicate(gc, operand);
        amd64_obj_swap(operand, duplicate);
        amd64_mem_dispose(&gc->mem, duplicate);
    }

    // We cannot operate in-place on an immediate, so if the operand is one,
    // move it into a register.
    if (operand->location.type == AMD64_LT_IMMEDIATE) {
        operand = generate_reg_promotion(
            gc,
            operand,
            operand->data_type->primitive == DT_PRIM_DOUBLE
            ? AMD64_REG_XMM0 : AMD64_REG_AX);
    }

    // Main dispatch selecting the proper increment instruction:
    switch (operand->data_type->primitive) {
    case DT_PRIM_UNIT:
        error_throw(
            &gc->error_context,
            "Can not negate a unit value");
        break;

    case DT_PRIM_BOOLEAN:
        error_throw(
            &gc->error_context,
            "Can not increment a boolean value");
        break;

    case DT_PRIM_BYTE:
    case DT_PRIM_WORD:
        generate_inc(gc, operand->location);
        break;

    case DT_PRIM_DOUBLE:
        one = procure_rip_double(gc, 1.0);
        generate_addsd(gc, one->location, operand->location);
        amd64_mem_dispose(&gc->mem, one);
        break;
    }

    return operand;
}

static struct AMD64Object *generate_dereference(
        struct GenerationContext *gc,
        struct AMD64Object *operand)
{
    struct AMD64Displacement disp0 = { .value = 0 };
    enum AMD64Register dst_reg;
    enum AMD64GPRegister dst_gpreg;
    enum AMD64IndirectRegister dst_ireg;
    struct DataType *dst_data_type;
    struct AMD64Object *dst_obj;

    // 1. Validate input type:
    if (operand->data_type->type != DT_POINTER) {
        error_throw(&gc->error_context, "Dereferenced a non-pointer object");
    }

    // 2. Copy the address to a new location valid for the indirect access:
    dst_reg = amd64_mem_find_free_gpreg(&gc->mem);
    if (dst_reg == AMD64_REG_MAX) {
        generate_eviction(gc, AMD64_REG_AX);
        dst_reg = AMD64_REG_AX;
    }
    dst_gpreg = amd64_reg_to_gpreg(dst_reg);
    generate_mov(gc,
                 operand->location,
                 amd64_loc_make_register_gp(AMD64_LS_64_BIT, dst_gpreg));

    // 3. Prepare the result object with proper location definition:
    if (!amd64_try_conv_gpreg_to_ireg(dst_gpreg, &dst_ireg)) {
        ABORT("Algorithmic corruption");
    }
    dst_data_type = dt_copy(operand->data_type->pointer.subtype);
    dst_obj = amd64_mem_create_object(
        &gc->mem,
        dst_data_type,
        amd64_loc_make_register_indirect(
            amd64_loc_size_from_bytes(dt_effective_size(dst_data_type)),
            dst_ireg,
            AMD64_IAS_64,
            disp0));

    amd64_mem_dispose(&gc->mem, operand);
    return dst_obj;
}

// ### Binary commands

static bool try_equalizing_numeric_types(struct GenerationContext *gc,
                                         struct AMD64Object *dst,
                                         struct AMD64Object *src)
{
    // 1. Try promote or demote immediates:
    if (src->location.type == AMD64_LT_IMMEDIATE) {
        try_adjust_immediate_type(gc, src, dst->data_type);
    } else if(dst->location.type == AMD64_LT_IMMEDIATE) {
        try_adjust_immediate_type(gc, dst, src->data_type);
    }

    // 2. Try promote non-immediates:
    switch (src->data_type->primitive) {
    case DT_PRIM_UNIT:
    case DT_PRIM_BOOLEAN:
        return false;

    case DT_PRIM_BYTE:
        switch (dst->data_type->primitive) {
        case DT_PRIM_UNIT:
        case DT_PRIM_BOOLEAN:
        return false;

        case DT_PRIM_BYTE:
            break;

        case DT_PRIM_WORD:
            src = promote_byte_to_word(gc, src);
            break;

        case DT_PRIM_DOUBLE:
            src = promote_byte_to_double(gc, src);
            break;
        }
        break;

    case DT_PRIM_WORD:
        switch (dst->data_type->primitive) {
        case DT_PRIM_UNIT:
        case DT_PRIM_BOOLEAN:
            return false;

        case DT_PRIM_BYTE:
            dst = promote_byte_to_word(gc, dst);
            break;

        case DT_PRIM_WORD:
            break;

        case DT_PRIM_DOUBLE:
            src = promote_word_to_double(gc, src);
            break;
        }
        break;

    case DT_PRIM_DOUBLE:
        switch (dst->data_type->primitive) {
        case DT_PRIM_UNIT:
        case DT_PRIM_BOOLEAN:
            return false;

        case DT_PRIM_BYTE:
            dst = promote_byte_to_double(gc, dst);
            break;

        case DT_PRIM_WORD:
            dst = promote_word_to_double(gc, dst);
            break;

        case DT_PRIM_DOUBLE:
            break;
        }
        break;
    }
    return true;
}

static struct AMD64Object *generate_arithmetic_gp(
        struct GenerationContext *gc,
        struct AMD64Object *src,
        struct AMD64Object *dst,
        bool is_destructive,
        bool is_commutative,
        void (*generator)(struct GenerationContext *,
                          struct AMD64Location,
                          struct AMD64Location))
{
    // 1. Ensure right (destination) is not in scope, as it will be
    //    overwritten:
    if (!is_destructive || !amd64_mem_find_by_obj(&gc->mem, dst)) {
        // 1.a) Do nothing - this is what we want.

    } else if ((!is_destructive ||
                !amd64_mem_find_by_obj(&gc->mem, src)) &&
               is_commutative) {
        // 1.b) Almost there - left is fine, so swap if we can:
        amd64_obj_swap(src, dst);

    } else {
        // 1.c) Both arguments in scope => duplicate right into reasonable
        //      location:
        if (src->location.type == AMD64_LT_REGISTER_GP) {
            dst = generate_reg_mem_promotion(gc, dst);
        } else {
            dst = generate_reg_promotion(gc, dst, AMD64_REG_AX);
        }
    }

    // 2. Ensure not both arguments are memory locations:
    if (amd64_loc_is_memory(&src->location) &&
        amd64_loc_is_memory(&dst->location)) {
        dst = generate_reg_promotion(gc, dst, AMD64_REG_AX);
    }

    // 3. Ensure the destination is not an immediate:
    if (dst->location.type == AMD64_LT_IMMEDIATE) {
        if (src->location.type == AMD64_LT_REGISTER_GP) {
            dst = generate_reg_mem_promotion(gc, dst);
        } else {
            dst = generate_reg_promotion(gc, dst, AMD64_REG_AX);
        }
    }

    // 4. Prevent 64-bit immediate source:
    if (src->location.type == AMD64_LT_IMMEDIATE &&
        src->location.size == AMD64_LS_64_BIT) {
        if (dst->location.type == AMD64_LT_REGISTER_GP) {
            src = generate_reg_mem_promotion(gc, src);
        } else {
            src = generate_reg_promotion(gc, src, AMD64_REG_BX);
        }
    }

    // 5. Generate the instruction:
    generator(gc, src->location, dst->location);

    // 6. Cleanup and return the result:
    amd64_mem_dispose(&gc->mem, src);
    return dst;
}

static struct AMD64Object *generate_arithmetic_sse(
        struct GenerationContext *gc,
        struct AMD64Object *src,
        struct AMD64Object *dst,
        bool is_destructive,
        bool is_commutative,
        void (*generator)(struct GenerationContext *,
                          struct AMD64Location,
                          struct AMD64Location))
{
    // 1. Ensure destination operand is valid:
    if (dst->location.type == AMD64_LT_REGISTER_SSE &&
        (!is_destructive ||
         !amd64_mem_find_by_obj(&gc->mem, dst))) {
        // 1.a) Do nothing - this is what we want.

    } else if (src->location.type == AMD64_LT_REGISTER_SSE &&
               (!is_destructive ||
                !amd64_mem_find_by_obj(&gc->mem, src)) &&
               is_commutative) {
        // 1.b) Almost there - left is fine, so swap if we can:
        amd64_obj_swap(src, dst);

    } else {
        // 1.c) Neither operand will cut it as the destination, force
        //      destination into a register:
        dst = generate_sse_reg_promotion(gc, dst, AMD64_SSEREG_XMM0);
    }

    // 2. Ensure source operand is valid:
    if (src->location.type == AMD64_LT_IMMEDIATE) {
        src = generate_reg_mem_promotion(gc, src);
    }

    // 3. Generate the instruction:
    generator(gc, src->location, dst->location);

    // 4. Cleanup and return the result:
    amd64_mem_dispose(&gc->mem, src);
    return dst;
}

static struct AMD64Object *generate_arithmetic(
        struct GenerationContext *gc,
        struct AMD64Object *src,
        struct AMD64Object *dst,
        bool is_destructive,
        bool is_commutative,
        void (*gp_generator)(struct GenerationContext *,
                             struct AMD64Location,
                             struct AMD64Location),
        void (*sse_generator)(struct GenerationContext *,
                              struct AMD64Location,
                              struct AMD64Location))
{
    // 1. Try equalize data types:
    if (!try_equalizing_numeric_types(gc, src, dst)) {
        error_throw(
            &gc->error_context,
            "Arithmetic operation only allowed on compatible numeric types");
    }
    assert(dt_equal(src->data_type, dst->data_type));

    // 2. Dispatch based on the data type:
    if (src->data_type->primitive == DT_PRIM_DOUBLE) {

        // 2.a) Floating point instruction generation:
        return generate_arithmetic_sse(
            gc, src, dst, is_destructive, is_commutative, sse_generator);

    } else {

        // 2.b) Integral instruction generation:
        return generate_arithmetic_gp(
            gc, src, dst, is_destructive, is_commutative, gp_generator);

    }

    // 3. Clean up and return the result:
    amd64_mem_dispose(&gc->mem, src);
    return dst;
}

static struct AMD64Object *generate_multiplication_gp_imm(
        struct GenerationContext *gc,
        struct AMD64Object *gp,
        struct AMD64Object *imm)
{
    // This is a very specific case, so we expect the client to already
    // determine, which argument is in an immediate and which is in a GP
    // register.

    // 1. Assert the input location types:
    if (gp->location.type != AMD64_LT_REGISTER_GP) {
        ABORT("Algorythmic corruption");
    }
    if (imm->location.type != AMD64_LT_IMMEDIATE) {
        ABORT("Algorythmic corruption");
    }

    // 2. Generate the instruction:
    generate_imul3(gc, gp->location, gp->location, imm->location);

    // 3. Clean up and return the result:
    amd64_mem_dispose(&gc->mem, imm);
    return gp;
}

static struct AMD64Object *generate_multiplication_gp_rm(
        struct GenerationContext *gc,
        struct AMD64Object *src,
        struct AMD64Object *dst)
{
    bool was_8_bit = false;

    // 1. Account 8-bit operand case:
    if (src->location.size == AMD64_LS_8_BIT) {

        // Here we convert the operands to non-8bit ones. There are the following
        // options for the new size:
        // a) 16-bit - we don't want this as it requires an operand size prefix,
        // b) 64-bit - we don't want this as it requires REX prefix,
        // c) 32-bit - we chose this as it requires no additional prefixes.

        // 1.1. Remember that we transitioned away from 8-bit operands:
        was_8_bit = true;

        // 1.2. Select a new location for the operands:

        // 1.2.1. Handle source transition:
        if (src->location.type == AMD64_LT_REGISTER_GP) {
            struct AMD64Location new_location = src->location;
            new_location.size = AMD64_LS_32_BIT;
            generate_movzx(gc, src->location, new_location);
            amd64_loc_swap(&src->location, &new_location);

        } else {
            struct AMD64Object *new_src =
                generate_gp_reg_duplicate(gc, src, AMD64_GPREG_BX);
            new_src->location.size = AMD64_LS_32_BIT;
            generate_movzx(gc, src->location, new_src->location);
            amd64_obj_swap(src, new_src);
            amd64_mem_dispose(&gc->mem, new_src);
        }

        // 1.2.2. Handle destination transition:
        if (dst->location.type == AMD64_LT_REGISTER_GP) {
            struct AMD64Location new_location = dst->location;
            new_location.size = AMD64_LS_32_BIT;
            generate_movzx(gc, dst->location, new_location);
            amd64_loc_swap(&dst->location, &new_location);

        } else {
            struct AMD64Object *new_dst =
                generate_gp_reg_duplicate(gc, dst, AMD64_GPREG_AX);
            new_dst->location.size = AMD64_LS_32_BIT;
            generate_movzx(gc, dst->location, new_dst->location);
            amd64_obj_swap(dst, new_dst);
            amd64_mem_dispose(&gc->mem, new_dst);
        }
    }

    // 2. Handle the general case:

    // 2.1 Make sure destination is a register:
    if (dst->location.type == AMD64_LT_REGISTER_GP) {
        // 2.1.a) Do nothing - this is what we want.

    } else if (src->location.type == AMD64_LT_REGISTER_GP &&
               !amd64_mem_find_by_obj(&gc->mem, src)) {
        // 2.1.b) Almost there - source is fine, so swap operands:
        amd64_obj_swap(src, dst);

    } else {
        // 2.1.c) Neither operand will cut it as the destination, so move
        //        destination into a register:
        dst = generate_gp_reg_promotion(gc, dst, AMD64_GPREG_AX);
    }

    // 2.2 Make sure source is not an immediate:
    if (src->location.type == AMD64_LT_IMMEDIATE) {
        src = generate_reg_mem_promotion(gc, src);
    }

    // 3. Generate the instruction:
    generate_imul2(gc, src->location, dst->location);

    // 3. Adjust the result type if the operands were of 8-bit size:
    if (was_8_bit) {
        if (src->location.type != AMD64_LT_REGISTER_GP ||
            dst->location.type != AMD64_LT_REGISTER_GP ||
            src->location.size != AMD64_LS_32_BIT ||
            dst->location.size != AMD64_LS_32_BIT) {
            ABORT("Algorythmic corruption");
        }
        src->location.size = AMD64_LS_8_BIT;
        dst->location.size = AMD64_LS_8_BIT;
    }

    // 4. Clean up and return the result:
    amd64_mem_dispose(&gc->mem, src);
    return dst;
}

static struct AMD64Object *generate_multiplication(
        struct GenerationContext *gc,
        struct AMD64Object *src,
        struct AMD64Object *dst)
{
    // 1. Try equalize data types:
    if (!try_equalizing_numeric_types(gc, src, dst)) {
        error_throw(
            &gc->error_context,
            "Arithmetic operation only allowed on compatible numeric types");
    }
    assert(dt_equal(src->data_type, dst->data_type));

    // 2. Dispatch between the integral and floating point variant:
    if (src->data_type->primitive == DT_PRIM_DOUBLE) {

        // 2.a) Floating point instruction generation:
        return generate_arithmetic_sse(gc, src, dst, true, true, generate_mulsd);

    } else {

        // 2.b) Integral instruction generation:

        // There are three variants of the integral multiplication instruction:
        // - AX * r/m -> AX:DX
        // - reg * r/m -> <destination location>
        // - imm * r/m -> reg
        //
        // Here, for the integral variant, we will implement a dispatch between
        // the latter two, as the first one is not very useful for now. For that
        // we will semantics of the type expansion, where the operation result
        // is bigger than its operands.

        if (src->location.type == AMD64_LT_IMMEDIATE &&
            dst->location.type == AMD64_LT_REGISTER_GP) {
            return generate_multiplication_gp_imm(gc, dst, src);

        } else if (dst->location.type == AMD64_LT_IMMEDIATE &&
                   src->location.type == AMD64_LT_REGISTER_GP) {
            return generate_multiplication_gp_imm(gc, src, dst);

        } else {
            return generate_multiplication_gp_rm(gc, src, dst);

        }
    }
}

static struct AMD64Object *generate_division_gp(
        struct GenerationContext *gc,
        struct AMD64Object *dividend,
        struct AMD64Object *divisor)
{
    // 1. Make sure the dividend is in rAX:
    if (dividend->location.type != AMD64_LT_REGISTER_GP ||
        dividend->location.gpreg.gpreg != AMD64_GPREG_AX) {
        struct AMD64Object *new_dividend;
        if (amd64_mem_find_by_reg(&gc->mem, AMD64_REG_AX)) {
            generate_eviction(gc, AMD64_REG_AX);
        }
        new_dividend = amd64_mem_create_reg(&gc->mem,
                                            AMD64_REG_AX,
                                            dt_copy(dividend->data_type));
        generate_copy(gc, dividend, new_dividend);
        amd64_obj_swap(dividend, new_dividend);
        amd64_mem_dispose(&gc->mem, new_dividend);
    }

    // 2. Make sure the divisor is not an immediate:
    if (divisor->location.type == AMD64_LT_IMMEDIATE) {
        divisor = generate_reg_mem_promotion(gc, divisor);
    }

    // 3. Make sure everything is fine with rAX and rDX:
    if (dividend->location.size == AMD64_LS_8_BIT) {

        // 3.a) For 8-bit division we still use AX rather than AL, so we perform
        //      a MOVZX to make sure no garbage resides in AH:
        generate_movzx(gc, AL, AX);

    } else {

        // 3.b)1. Make sure rDX isn't used:
        if (amd64_mem_find_by_reg(&gc->mem, AMD64_REG_DX)) {
            generate_eviction(gc, AMD64_REG_DX);
        }

        // 3.b)2. Make sure rDX holds zero:
        generate_xor(gc, RDX, RDX);
    }

    // 4. Generate the instruction:
    generate_idiv(gc, divisor->location);

    // 5. Clean up and return the result:
    amd64_mem_dispose(&gc->mem, divisor);
    return dividend;
}

static struct AMD64Object *generate_division(
        struct GenerationContext *gc,
        struct AMD64Object *dividend,
        struct AMD64Object *divisor)
{
    // 1. Try equalize data types:
    if (!try_equalizing_numeric_types(gc, dividend, divisor)) {
        error_throw(
            &gc->error_context,
            "Arithmetic operation only allowed on compatible numeric types");
    }
    assert(dt_equal(dividend->data_type, divisor->data_type));

    // 2. Dispatch between the integral and floating point variant:
    if (dividend->data_type->primitive == DT_PRIM_DOUBLE) {

        // 2.a) Floating point instruction generation:
        return generate_arithmetic_sse(gc,
                                       dividend,
                                       divisor,
                                       true,
                                       true,
                                       generate_divsd);
    } else {
        // 2.a) Floating point instruction generation:
        return generate_division_gp(gc, dividend, divisor);

    }
}

enum ComparisonDomain {
    CD_SIGNED_LIKE,
    CD_UNSIGNED_LIKE
};

static enum ComparisonDomain generate_comparison(struct GenerationContext *gc,
                                                 struct AMD64Object *src,
                                                 struct AMD64Object *dst)
{
    enum ComparisonDomain domain;
    if ((src->data_type->type == DT_PRIMITIVE &&
         src->data_type->primitive == DT_PRIM_DOUBLE) ||
        (dst->data_type->type == DT_PRIMITIVE &&
         dst->data_type->primitive == DT_PRIM_DOUBLE)) {
        domain = CD_UNSIGNED_LIKE;
    } else {
        domain = CD_SIGNED_LIKE;
    }

    amd64_mem_dispose(
        &gc->mem,
        generate_arithmetic(
            gc, src, dst, false, false, generate_cmp, generate_comisd));

    return domain;
}

static struct AMD64Object *generate_mod_8(
        struct GenerationContext *gc,
        struct AMD64Object *left,
        struct AMD64Object *right)
{
    struct AMD64Object *result;

    // 1. Put 8-bit dividend in AX:
    if (!amd64_mem_register_free(&gc->mem, AMD64_REG_AX)) {
        generate_eviction(gc, AMD64_REG_AX);
    }
    generate_movzx(gc, left->location, AX);

    // 2. Encode the 8-bit IDIV command:
    generate_idiv(gc, right->location);

    // 3. Prepare the result object:

    // 3.1. Just reuse the quotient register, which is not used in this context:
    generate_mov(gc, AH, AL);

    // 3.2. Clear the high part of the register, that we no longer use:
    generate_xor(gc, AH, AH);

    // 3.3. Now the result resides in AL, which we encode:
    result = amd64_mem_create_object(&gc->mem, dt_copy(left->data_type), AL);

    // 4. Clean up and return:
    amd64_mem_dispose(&gc->mem, left);
    amd64_mem_dispose(&gc->mem, right);
    return result;
}

static struct AMD64Object *generate_mod_non8(
        struct GenerationContext *gc,
        struct AMD64Object *left,
        struct AMD64Object *right)
{
    enum AMD64LocationSize size = left->location.size;
    struct AMD64Object *result;

    // 0. Assert input validity:
    if (left->location.size == AMD64_LS_8_BIT) {
        ABORT("Algorithmic corruption");
    }

    // 1. Put the dividend in rAX:
    if (!amd64_mem_register_free(&gc->mem, AMD64_REG_AX)) {
        generate_eviction(gc, AMD64_REG_AX);
    }
    generate_mov(gc,
                 left->location,
                 amd64_loc_make_register_gp(size, AMD64_GPREG_AX));

    // 2. Clear the high part of dividend, which we don't have in this case:
    if (!amd64_mem_register_free(&gc->mem, AMD64_REG_DX)) {
        generate_eviction(gc, AMD64_REG_DX);
    }
    generate_xor(gc, RDX, RDX);

    // 3. Encode the X-bit IDIV command:
    generate_idiv(gc, right->location);

    // 4. Prepare the result:
    result = amd64_mem_create_object(
        &gc->mem,
        dt_copy(left->data_type),
        amd64_loc_make_register_gp(size, AMD64_GPREG_DX));

    // 5. Clean up and return:
    amd64_mem_dispose(&gc->mem, left);
    amd64_mem_dispose(&gc->mem, right);
    return result;
}

static struct AMD64Object *generate_mod(
        struct GenerationContext *gc,
        struct AMD64Object *left,
        struct AMD64Object *right)
{
    // 1. Account for operations on immediates:
    if (right->location.type == AMD64_LT_IMMEDIATE) {
        try_adjust_immediate_type(gc, right, left->data_type);
    } else if(left->location.type == AMD64_LT_IMMEDIATE) {
        try_adjust_immediate_type(gc, left, right->data_type);
    }

    // 2. Assert types:
    // 2.1. Test types for equality:
    if (!dt_equal(right->data_type, left->data_type)) {
        error_throwf(
            &gc->error_context,
            "Different types passed to modulo");
    }
    // 2.2. Check if types are valid for the operation:
    if (left->data_type->type != DT_PRIMITIVE ||
        (left->data_type->primitive != DT_PRIM_BYTE &&
         left->data_type->primitive != DT_PRIM_WORD)) {
        error_throwf(
            &gc->error_context,
            "Invalid types for the modulo operator");
    }

    // 3. Use an appropriate algorithm based on the operands' size:
    if (left->location.size == AMD64_LS_8_BIT) {
        return generate_mod_8(gc, left, right);

    } else {
        return generate_mod_non8(gc, left, right);

    }
}

// ### Procedure call generation

// #### Syscall generation

static void generate_syscall_exit(struct GenerationContext *gc) {
    generate_mov(gc, IMM32(60), RAX);
    generate_mov(gc, IMM32(123), RDI);
    generate_syscall(gc);
}

// #### Built-in procedures generation

static struct AMD64Object *generate_builtin_length(
        struct GenerationContext *gc,
        int argc,
        struct AMD64Object **argv)
{
    struct AMD64Object *src, *dst;
    struct DataType *new_data_type;

    // 1. Evaluate arguments:
    if (argc != 1) {
        error_throw(&gc->error_context, "Invalid arguments count passed to length");
    }
    src = argv[0];
    if (!dt_is_buffer_ptr(src->data_type)) {
        error_throw(&gc->error_context, "Length only implemented for buffers");
    }

    // 2. Reinterpret cast from pointer-to-buffer into pointer-to-word:
    //    (this peeks the size word stored at the beginning of the buffer)
    new_data_type = dt_create_pointer(dt_create_primitive(DT_PRIM_WORD));
    dt_swap(&src->data_type, &new_data_type);
    dt_destroy(new_data_type);

    // 3. Turn a local pointer object into a remote pointee object:
    src = generate_dereference(gc, src);

    // 4. Store the size word in RAX:
    if (!amd64_mem_register_free(&gc->mem, AMD64_REG_AX)) {
        generate_eviction(gc, AMD64_REG_AX);
    }
    dst = amd64_mem_create_reg_word(&gc->mem, AMD64_REG_AX);
    generate_mov(gc, src->location, dst->location);

    // 5. Cleanup:
    amd64_mem_dispose(&gc->mem, src);

    return dst;
}

static struct AMD64Object *generate_builtin_print_buffer(
        struct GenerationContext *gc,
        struct AMD64Object *src)
{
    assert(dt_is_buffer_ptr(src->data_type));

    // 1. Make room...:
    if (!amd64_mem_register_free(&gc->mem, AMD64_REG_DI)) {
        generate_eviction(gc, AMD64_REG_DI);
    }
    if (!amd64_mem_register_free(&gc->mem, AMD64_REG_SI)) {
        generate_eviction(gc, AMD64_REG_SI);
    }
    if (!amd64_mem_register_free(&gc->mem, AMD64_REG_AX)) {
        generate_eviction(gc, AMD64_REG_AX);
    }
    if (!amd64_mem_register_free(&gc->mem, AMD64_REG_DX)) {
        generate_eviction(gc, AMD64_REG_DX);
    }

    // 2. Generate syscall code:
    generate_mov(gc, IMM32(0x1), RAX);    // syscall ID
    generate_mov(gc, IMM32(0x1), RDI);    // stdout file descriptor
    generate_mov(gc, src->location, RSI); // %rsi is now ptr to size
    generate_mov(gc, QWPTR64(SI), RDX);   // load size
    generate_add(gc, IMM8(0x08), RSI);    // %rsi is now ptr to ptr to buffer
    generate_mov(gc, QWPTR64(SI), RSI);   // %rsi is not ptr to buffer
    generate_syscall(gc);

    // 3. Set the result:
    return amd64_mem_create_object(
        &gc->mem, dt_create_primitive(DT_PRIM_WORD), RAX);
}

static struct AMD64Object *generate_builtin_print_cstring(
        struct GenerationContext *gc,
        struct AMD64Object *src)
{
    int loop_base;
    struct JumpRecord jr;

    assert(src->data_type->type == DT_CSTRING);

    // 1. Make room...:
    if (!amd64_mem_register_free(&gc->mem, AMD64_REG_DI)) {
        generate_eviction(gc, AMD64_REG_DI);
    }
    if (!amd64_mem_register_free(&gc->mem, AMD64_REG_SI)) {
        generate_eviction(gc, AMD64_REG_SI);
    }
    if (!amd64_mem_register_free(&gc->mem, AMD64_REG_AX)) {
        generate_eviction(gc, AMD64_REG_AX);
    }
    if (!amd64_mem_register_free(&gc->mem, AMD64_REG_DX)) {
        generate_eviction(gc, AMD64_REG_DX);
    }

    // 2. Generate code:

    // 2.1. Compute string length:

    // 2.1.1. Initialize loop:
    generate_xor(gc, RDX, RDX);           // counter
    generate_xor(gc, RDI, RDI);           // zero for reference
    generate_lea(gc, src->location, RSI); // lookup base

    // 2.1.2. Generate loop:
    loop_base = bb_current_offset(gc->bb);
    generate_movzx(gc, BSIB64(1, DX, SI, 0), RAX);
    generate_inc(gc, RDX);
    generate_cmp(gc, RAX, RDI);
    jr = generate_jne_32(gc);
    jr_set(gc, &jr, loop_base);

    // At this point:
    // * RSI (still) points to the beginning of the C-string
    // * RDX contains the string size (including the terminating NUL character)

    // 2.2. Generate syscall:
    generate_mov(gc, IMM32(0x1), RAX); // syscall ID
    generate_mov(gc, IMM32(0x1), RDI); // stdout file descriptor
                                       // the other arguments are already in
    generate_syscall(gc);

    // 3. Set the result:
    return amd64_mem_create_object(
        &gc->mem, dt_create_primitive(DT_PRIM_WORD), RAX); // Reuse syscal result
}

static struct AMD64Object *generate_builtin_print(
        struct GenerationContext *gc,
        int argc,
        struct AMD64Object **argv)
{
    // TODO: Delegate the functions from here to the amd64_linux module, as
    //       they aren't amd64 specific.

    // 1. Evaluate arguments:
    if (argc != 1) {
        error_throw(&gc->error_context, "Invalid arguments count passed to print");
        return NULL;
    }
    if (dt_is_buffer_ptr(argv[0]->data_type)) {
        return generate_builtin_print_buffer(gc, argv[0]);

    } else if (argv[0]->data_type->type == DT_CSTRING) {
        return generate_builtin_print_cstring(gc, argv[0]);

    } else {
        error_throw(&gc->error_context, "Invalid argument type passed to print");
        return NULL;
    }
}

static struct AMD64Object *generate_builtin(
        struct GenerationContext *gc,
        char *symbol,
        int argc,
        struct AMD64Object **argv)
{
    if (strcmp(symbol, "length") == 0) {
        return generate_builtin_length(gc, argc, argv);

    } else if (strcmp(symbol, "print") == 0) {
        return generate_builtin_print(gc, argc, argv);

    } else {
        return NULL;
    }
}

// Procedure related code generation
// ---------------------------------

static const enum AMD64GPRegister int_arg_locations[] = {
    AMD64_GPREG_DI,
    AMD64_GPREG_SI,
    AMD64_GPREG_DX,
    AMD64_GPREG_CX,
    AMD64_GPREG_8,
    AMD64_GPREG_9,
};
static const enum AMD64IndirectRegister int_arg_indir_locations[] = {
    AMD64_IREG_DI,
    AMD64_IREG_SI,
    AMD64_IREG_DX,
    AMD64_IREG_CX,
    AMD64_IREG_8,
    AMD64_IREG_9,
};
static const int int_arg_locations_count = 6;

static const enum AMD64SSERegister float_arg_locations[] = {
    AMD64_SSEREG_XMM0,
    AMD64_SSEREG_XMM1,
    AMD64_SSEREG_XMM2,
    AMD64_SSEREG_XMM3,
    AMD64_SSEREG_XMM4,
    AMD64_SSEREG_XMM5,
    AMD64_SSEREG_XMM6,
    AMD64_SSEREG_XMM7,
};
static const int float_arg_locations_count = 8;

static const enum AMD64GPRegister int_result_locations [] = {
    AMD64_GPREG_AX
};
static const enum AMD64IndirectRegister int_result_indir_locations [] = {
    AMD64_IREG_AX
};
static const int int_result_locations_count = 1;

static const enum AMD64SSERegister float_result_locations [] = {
    AMD64_SSEREG_XMM0
};
static const int float_result_locations_count = 1;

static bool sysv_is_implicitly_callee_preserved(enum AMD64Register reg)
{
    return
        reg & (
            AMD64_REG_BX |
            AMD64_REG_12 |
            AMD64_REG_13 |
            AMD64_REG_14 |
            AMD64_REG_15);
}

static bool sysv_is_gp_argument(struct DataType *data_type)
{
    return
        (data_type->type == DT_POINTER) ||
        (data_type->type == DT_BUFFER) ||
        (data_type->type == DT_CSTRING) ||
        (data_type->type == DT_PRIMITIVE && (
            data_type->primitive == DT_PRIM_UNIT ||
            data_type->primitive == DT_PRIM_BOOLEAN ||
            data_type->primitive == DT_PRIM_BYTE ||
            data_type->primitive == DT_PRIM_WORD));
}

static bool sysv_is_sse_argument(struct DataType *data_type)
{
    return
        data_type->type == DT_PRIMITIVE &&
        data_type->primitive == DT_PRIM_DOUBLE;
}

struct StackArgIndices {
    int *data, cap, size;
};

static void sysv_handle_gp_procedure_argument(struct GenerationContext *gc,
                                              char *proc_name,
                                              int arg_index,
                                              char *arg_name,
                                              struct DataType *arg_type,
                                              int *int_arg_index,
                                              struct StackArgIndices *stack_args) {

    if (*int_arg_index == int_arg_locations_count) {
        // a) Ran out of registers, put the argument on the stack:
        array_append(*stack_args, arg_index);

    } else {
        // b) Registers still available, use the next one:
        struct AMD64Displacement disp0 = { .value = 0 };
        struct AMD64Object *obj;
        struct AMD64Location loc;

        switch (arg_type->type) {
        case DT_PRIMITIVE:
        case DT_POINTER:
            // For the primitive and pointer type, we create a direct register
            // location:
            loc = amd64_loc_make_register_gp(
                amd64_loc_size_from_bytes(dt_effective_size(arg_type)),
                int_arg_locations[*int_arg_index]);
            ++(*int_arg_index);
            break;

        case DT_BUFFER:
            // TODO: Clarify this after changing the buffer implementation method:
            loc = amd64_loc_make_register_indirect(
                amd64_loc_size_from_bytes(dt_effective_size(arg_type->buffer.subtype)),
                int_arg_indir_locations[*int_arg_index],
                AMD64_IAS_64,
                disp0);
            ++(*int_arg_index);
            break;

        case DT_CSTRING:
            // For a C-string, we create an indirect location pointing to an
            // 8-bit object - a C character:
            loc = amd64_loc_make_register_indirect(
                AMD64_LS_8_BIT,
                int_arg_indir_locations[*int_arg_index],
                AMD64_IAS_64,
                disp0);
            ++(*int_arg_index);
            break;
        }

        obj = amd64_mem_create_object(&gc->mem, dt_copy(arg_type), loc);
        if (!amd64_mem_try_put(&gc->mem, arg_name, obj)) {
            array_free(*stack_args);
            error_throwf(
                &gc->error_context,
                "Failed registering argument %s of %s",
                arg_name,
                proc_name);
        }
    }

}

static void sysv_handle_sse_procedure_argument(struct GenerationContext *gc,
                                               char *proc_name,
                                               int arg_index,
                                               char *arg_name,
                                               struct DataType *arg_type,
                                               int *float_arg_index,
                                               struct StackArgIndices *stack_args) {
    if (*float_arg_index == float_arg_locations_count) {
        // 1.2.b)a) Ran out of registers, put the argument on the stack:
        array_append(*stack_args, arg_index);

    } else {
        // 1.2.b)b) Registers still available, use the next one:
        struct AMD64Object *obj = amd64_mem_create_object(
            &gc->mem,
            dt_copy(arg_type),
            amd64_loc_make_register_sse(
                float_arg_locations[*float_arg_index]));

        ++(*float_arg_index);
        if (!amd64_mem_try_put(&gc->mem, arg_name, obj)) {
            array_free(*stack_args);
            error_throwf(
                &gc->error_context,
                "Failed registering argument %s of %s",
                arg_name,
                proc_name);
        }
    }
}

static void sysv_handle_procedure_arguments(
        struct GenerationContext *gc,
        struct ProcedurePrototype *pp,
        struct DataTypeList *argument_types)
{
    int int_arg_index = 0;   // Count the integral arguments so far
    int float_arg_index = 0; // Count the floating point arguments so far
    int i;

    // We may need a registry of the arguments that didn't fit in the registers
    struct StackArgIndices stack_arguments = { NULL, 0, 0 };

    // 1. Attempt to put the argument in the registers:
    for (i = 0; i < argument_types->size; ++i) {

        // 1.1. Look up the argument name:
        char *argument_name = pp_argument_name(pp, i);
        struct DataType *argument_type = argument_types->data[i];
        if (!argument_name) {
            array_free(stack_arguments);
            error_throwf(
                &gc->error_context,
                "Failed resolving %d-th argument of %s",
                i, pp->name);
        }

        // 1.2. Dispatch based on the type group integral vs. floating point:
        //      (regardless of the type group we either put the argument in an
        //       appropriate register or leave to be allocated later)
        if (sysv_is_gp_argument(argument_type)) {

            // 1.2.a) Argument belongs in a general purpose register:
            sysv_handle_gp_procedure_argument(gc,
                                              pp->name,
                                              i,
                                              argument_name,
                                              argument_type,
                                              &int_arg_index,
                                              &stack_arguments);

        } else if (sysv_is_sse_argument(argument_type)) {

            // 1.2.b) Argument belongs in the SSE register:
            sysv_handle_sse_procedure_argument(gc,
                                               pp->name,
                                               i,
                                               argument_name,
                                               argument_type,
                                               &float_arg_index,
                                               &stack_arguments);
        }
    }

    // 2. Whichever arguments didn't fit in the registers, must have been
    //    allocated on the stack, so we register them here, in the reverse
    //    order of appearance:
    for (i = stack_arguments.size - 1; i >= 1; --i) {
        char *argument_name = pp_argument_name(pp, i);
        struct DataType *argument_type = argument_types->data[i];
        struct AMD64Object *obj = amd64_mem_create_stack_object(
            &gc->mem, dt_copy(argument_type));
        // Note: Buffers are passed in by pointers
        if (!amd64_mem_try_put(&gc->mem, argument_name, obj)) {
            array_free(stack_arguments);
            error_throwf(
                &gc->error_context,
                "Failed registering argument %s of %s",
                argument_name,
                pp->name);
        }
    }

    // 3. Cleanup:
    array_free(stack_arguments);
}

static void sysv_handle_procedure_results(
        struct GenerationContext *gc,
        struct ProcedurePrototype *pp)
{
    int result_index, gp_index = 0, sse_index = 0;

    // 0. Special case for procedures that return nothing:
    if (pp->results.size == 1 &&
        ast_is_literal_spec(pp->results.data[0], AST_LIT_UNIT)) {
        array_append(gc->result_types, dt_create_primitive(DT_PRIM_UNIT));
        return;
    }

    for (result_index = 0; result_index != pp->results.size; ++result_index) {

        char *name;
        struct AMD64Object *src;
        enum AMD64Register dst_reg;
        struct AMD64Location loc_dst;

        // 1. Look up the current result in scope:
        if (!(name = pp_result_name(pp, result_index))) {
            ABORT("Algorythmic corruption");
        }
        if (!(src = amd64_mem_find_by_name(&gc->mem, name))) {
            error_throwf(
                &gc->error_context,
                "%d-th argument of %s has not been assigned",
                result_index, pp->name);
        }

        // 2. Record the current result's type:
        array_append(gc->result_types, dt_copy(src->data_type));

        // 3. Define the target location for the current result:
        if (src->data_type->type == DT_POINTER) {
            // Pointer objects are put directly in the register:
            dst_reg = amd64_reg_from_gpreg(int_result_locations[gp_index]);
            loc_dst = amd64_loc_make_register_gp(
                amd64_loc_size_from_bytes(dt_effective_size(src->data_type)),
                int_result_locations[gp_index]);
            ++gp_index;

        } else if (src->data_type->type == DT_PRIMITIVE) {
            // Primitive objects are put directly in an appropriate register:
            if (src->data_type->primitive == DT_PRIM_DOUBLE) {
                dst_reg = amd64_reg_from_ssereg(
                    float_result_locations[sse_index]);
                loc_dst = amd64_loc_make_register_sse(
                    float_result_locations[sse_index]);
                ++sse_index;

            } else {
                dst_reg = amd64_reg_from_gpreg(
                    int_result_locations[gp_index]);
                loc_dst = amd64_loc_make_register_gp(
                    amd64_loc_size_from_bytes(dt_effective_size(src->data_type)),
                    int_result_locations[gp_index]);
                ++gp_index;
            }

        } else {
            // Objects of other type land into indirect locations:
            struct AMD64Displacement disp0 = { .value = 0 };
            dst_reg = amd64_reg_from_ireg(
                int_result_indir_locations[gp_index]);
            loc_dst = amd64_loc_make_register_indirect(
                amd64_loc_size_from_bytes(dt_effective_size(src->data_type)),
                int_result_indir_locations[gp_index],
                AMD64_IAS_64,
                disp0);
            gp_index++;
        }

        // 4. Stupid (for now) check for running out of result locations:
        if (gp_index == int_result_locations_count + 1 ||
            sse_index == float_result_locations_count + 1) {
            error_throwf(&gc->error_context,
                         "Too many results to %s procedure",
                         name);
        }

        // 5. Move the current argument to the required location unless it's
        //    already there:
        if (!amd64_loc_equal(&src->location, &loc_dst)) {
            struct AMD64Object *dst;
            if (!amd64_mem_register_free(&gc->mem, dst_reg)) {
                generate_eviction(gc, dst_reg);
            }
            dst = amd64_mem_create_object(
                &gc->mem, dt_copy(src->data_type), loc_dst);
            generate_copy(gc, src, dst);
            amd64_mem_dispose(&gc->mem, dst);
        }
        amd64_mem_dispose(&gc->mem, src);
    }
}

static void procedure_begin(struct GenerationContext *gc,
                            struct ProcedurePrototype *pp,
                            struct DataTypeList *argument_types)
{
    sysv_handle_procedure_arguments(gc, pp, argument_types);
    generate_push(gc, gc->rbp->location);
    gc->preservation_code_offset = bb_current_offset(gc->bb);
    generate_mov(gc, gc->rsp->location, gc->rbp->location);
    gc_on_write_reg(gc, AMD64_REG_BP);
}

static void procedure_end(struct GenerationContext *gc,
                          struct ProcedurePrototype *pp)
{
    enum AMD64Register reg;
    struct PGRConstant *constant;
    int old_size, size_increase;

    // 1. Make sure result variables are properly assigned:
    sysv_handle_procedure_results(gc, pp);

    // 2. Insert code for callee saved registers saving:

    // 2.1. Return to the beginning of the procedure (remember its offset):
    gc_revert(gc, gc->preservation_code_offset);
    old_size = gc->bb->size;

    // 2.2. Generate register saving code if necessary:
    for (reg = AMD64_REG_MAX >> 1; reg != 0; reg >>= 1) {
        if (sysv_is_implicitly_callee_preserved(reg) &&
            (gc->reg_write_map & reg)) {
            struct AMD64Location loc =
                amd64_loc_make_register_gp(
                    AMD64_LS_64_BIT, amd64_reg_to_gpreg(reg));
            generate_push(gc, loc);
        }
    }

    // 2.3. Compute the size of the extra code - all the remembered offsets need
    //      to be adjusted to account for this:
    size_increase = gc->bb->size - old_size;

    // 3. Copy the procedure code back together with the callee saved
    //    registers restoration:
    gc_restore(gc);
    for (reg = AMD64_REG_AX; reg != AMD64_REG_MAX; reg <<= 1) {
        if (sysv_is_implicitly_callee_preserved(reg) &&
            (gc->reg_write_map & reg)) {
            struct AMD64Location loc =
                amd64_loc_make_register_gp(
                    AMD64_LS_64_BIT, amd64_reg_to_gpreg(reg));
            generate_pop(gc, loc);
        }
    }

    // 4. Generate the procedure epilogue:
    generate_pop(gc, gc->rbp->location);
    generate_ret(gc);

    // 5. Adjust all the remembered offsets to account for the inserted code:
    if (!size_increase) {
        return;
    }
    constant = gc->constants;
    while (constant) {
        struct PGRRipOffsetList *offset = constant->rip_offsets;
        while (offset) {
            // Both: the location of the address and the base from which the
            // address is computed have moved, so adjust their values:
            offset->record.base += size_increase;
            offset->record.offset += size_increase;
            offset = offset->next;
        }
        constant = constant->next;
    }
}

static void command_begin(struct GenerationContext *gc)
{
    (void)gc;
}

static void command_end(struct GenerationContext *gc)
{
    // TODO: Introduce a mechanism to "lock" registers so that they cannot be
    //       evicted. If we acquire a specific register for an operand thatn
    //       needs to reside in one, we lock it so that it cannot be evicted
    //       e.g. as a location for the other operand. Here we will be
    //       releasing all the locks, as they are supposed to only last for the
    //       time of a single command generation.

    // Expect only the RSP and RBP to be acquired at the command end:
    if (gc->mem.temps.size > 2) {
        ABORT("Garbage left after command generation");
    }
}

// Utility function
// ================

static char *create_unique_name(void)
{
    // Why is this needed?
    static uint64_t counter;
    char *buffer = my_malloc(128);
    sprintf(buffer, "%" PRIu64, counter++);
    return buffer;
}

// Procedure generation
// ====================
//
// This is where the implementation of the high level generation algorithm
// starts.

// AST based generation code
// -------------------------

// (forward declaration)
static struct AMD64Object *generate_from_ast(
        struct GenerationContext *gc,
        struct AstNode *node);

// ### Assignment operator

static struct AMD64Object *generate_from_bin_assignment(
        struct GenerationContext *gc,
        struct AstNode *left,
        struct AstNode *right)
{
    struct AMD64Object *dst;
    struct AMD64Object *src;

    // 1. Assert left argument validity:
    if (ast_is_literal(left)) {
        error_throw(&gc->error_context, "Cannot assign to a literal");
    }

    // 2. Evaluate the right argument:
    src = generate_from_ast(gc, right);

    // 3. Establish the location of the destination argument:
    //    Note: this section ensures "dst" is assigned and not NULL.
    if (ast_is_symbol(left)) {
        // 3.a) Assign to a symbol:
        struct AMD64Object *obj = amd64_mem_find_by_name(
            &gc->mem, left->symbol.string);

        if (obj) {
            // 3.a)a) Variable already in scope:
            if (!dt_equal(src->data_type, obj->data_type)) {
                // TODO: What about assigning a small literal integer to a byte
                //       variable?
                error_throw(&gc->error_context, "Type mismatch on assignment");
            }
            dst = obj;

        } else {
            // 3.a)b) Variable not in scope yet, create:
            dst = create_compatible_destination(gc, src->data_type);
            amd64_mem_put(&gc->mem, left->symbol.string, dst);
        }

    } else {
        // 3.b) If we have a non-symbol on the left, just evaluate it:
        dst = generate_from_ast(gc, left);
    }

    // TODO: Enable mov promotion from 8 and 32 bit to 64 bit register (maintain type!):

    // 4. Copy the data:
    // TODO: Check if src == dst here and react accordingly.
    generate_copy(gc, src, dst);
    amd64_mem_dispose(&gc->mem, src);

    // 5. Return the remaining result:
    return dst;
}

// ### Procedure call generation

struct CallGenerationContext {
    struct CallGenerationContext *next;
    struct GenerationContext *gc;
    struct {
        struct AMD64Object **data;
        int size, cap;
    } arguments;
};

static struct CallGenerationContext *gc_push_cgc(struct GenerationContext *gc)
{
    struct CallGenerationContext *result = my_malloc(sizeof(*result));

    result->next = gc->cgc_garbage;
    result->gc = gc;
    result->arguments.data = NULL;
    result->arguments.size = result->arguments.cap = 0;

    gc->cgc_garbage = result;

    return result;
}

static void gc_pop_cgc(
        struct GenerationContext *gc,
        struct CallGenerationContext *cgc)
{
    int i;

    if (gc->cgc_garbage != cgc || !cgc) {
        ABORT("Algorythmic corruption");
    }

    gc->cgc_garbage = gc->cgc_garbage->next;

    for (i = 0; i != cgc->arguments.size; ++i) {
        amd64_mem_dispose(&gc->mem, cgc->arguments.data[i]);
    }
    array_free(cgc->arguments);
    my_free(cgc);
}

static void evaluate_argument_list_callback(void *data, struct AstNode *node)
{
    struct CallGenerationContext *cgc = (struct CallGenerationContext *)data;
    struct AMD64Object *oi = generate_from_ast(cgc->gc, node);
    array_append(cgc->arguments, oi);
}

static struct AMD64Object *generate_from_bin_proc_call(
        struct GenerationContext *gc,
        struct AstNode *left,
        struct AstNode *right)
{
    struct AMD64Object *result = NULL;

    if (left->type == AST_SYMBOL) {
        // a) Call to a procedure represented by a symbol:
        struct CallGenerationContext *cgc = gc_push_cgc(gc);
        char *symbol = left->symbol.string;

        // a)1. Evaluate the arguments' list:
        ast_for_each_left(right,
                          AST_BIN_LIST_SEQUENCE,
                          evaluate_argument_list_callback,
                          cgc);

        // a)2. Try resolving the call as a syscall or a call to the built in
        //      function:
        if (!(result = generate_builtin(gc,
                                        symbol,
                                        cgc->arguments.size,
                                        cgc->arguments.data))) {
            error_throw(&gc->error_context, "Not implemented yet");
        }

        // a)3. Dispose the argument objects:
        gc_pop_cgc(gc, cgc);

    } else {
        // b) Call to an object resulting from an expression evaluation:
        error_throw(&gc->error_context, "Not implemented yet");
    }

    return result;
}

// ### Guarded commands

static void generate_command_sequence(struct GenerationContext *gc,
                                      struct AstNode *node);

static struct JumpRecord generate_jump_if_not(
        struct GenerationContext *gc,
        struct AstNode *guard_node)
{
    /*
     * If this has successfully generated code, a CMP/COMISD and a jump to a yet
     * unspecified offset (upon guard failure) have been generated.
     */

    if (guard_node->type == AST_BINARY) {

        // a) Binary comparison guard:
        struct AstBinary *binary = &guard_node->binary;
        struct AMD64Object *dst = generate_from_ast(gc, binary->left);
        struct AMD64Object *src = generate_from_ast(gc, binary->right);

        // a)1. Handle comparison:
        switch (generate_comparison(gc, src, dst)) {
        case CD_SIGNED_LIKE:
            // a)1.a) Reaction to signed like comparison:
            switch (binary->type) {
            case AST_BIN_EQUAL:
                return generate_jne_32(gc);

            case AST_BIN_NOT_EQUAL:
                return generate_je_32(gc);

            case AST_BIN_LESS_THAN:
                return generate_jnl_32(gc);

            case AST_BIN_GREATER_THAN:
                return generate_jng_32(gc);

            case AST_BIN_LESS_THAN_OR_EQUAL:
                return generate_jnle_32(gc);

            case AST_BIN_GREATER_THAN_OR_EQUAL:
                return generate_jnge_32(gc);

            default:
                error_throw(
                    &gc->error_context,
                    "Guard must evaluate to Boolean value");
            }
            break;

        case CD_UNSIGNED_LIKE:
            // a)1.b) Reaction to unsigned like comparison:
            switch (binary->type) {
            case AST_BIN_EQUAL:
                return generate_jne_32(gc);

            case AST_BIN_NOT_EQUAL:
                return generate_je_32(gc);

            case AST_BIN_LESS_THAN:
                return generate_jnb_32(gc);

            case AST_BIN_GREATER_THAN:
                return generate_jna_32(gc);

            case AST_BIN_LESS_THAN_OR_EQUAL:
                return generate_jnbe_32(gc);

            case AST_BIN_GREATER_THAN_OR_EQUAL:
                return generate_jnae_32(gc);

            default:
                error_throw(
                    &gc->error_context,
                    "Guard must evaluate to Boolean value");
            }
            break;
        }

        ABORT("Impossible state");

    } else {

        // b) Arbitrary guard expression:

        // b)1. Evaluate and check the guard expression:
        struct AMD64Object *dst = generate_from_ast(gc, guard_node);
        struct AMD64Object *src = amd64_mem_create_imm_boolean(&gc->mem, false);

        if (dst->data_type->type != DT_PRIMITIVE ||
            dst->data_type->primitive != DT_PRIM_BOOLEAN) {
            error_throw(
                &gc->error_context,
                "Guard must evaluate to Boolean value");
        }

        // b)2. Generate comparison code:
        (void)generate_comparison(gc, src, dst);

        // b)3. Generate the reaction code:
        return generate_je_32(gc);
    }
}

struct IffiGenerationContext {
    struct IffiGenerationContext *next;
    struct GenerationContext *gc;
    struct { struct JumpRecord *data; int size, cap; } end_jumps;
};

static struct IffiGenerationContext *gc_push_igc(struct GenerationContext *gc)
{
    struct IffiGenerationContext *result = my_malloc(sizeof(*result));

    result->next = gc->igc_garbage;
    result->gc = gc;
    result->end_jumps.data = NULL;
    result->end_jumps.size = result->end_jumps.cap = 0;

    gc->igc_garbage = result;

    return result;
}

static void gc_pop_igc(
        struct GenerationContext *gc,
        struct IffiGenerationContext *igc)
{
    if (gc->igc_garbage != igc || !igc) {
        ABORT("Algorythmic corruption");
    }

    gc->igc_garbage = gc->igc_garbage->next;

    array_free(igc->end_jumps);
    my_free(igc);
}

static void generate_from_block_iffi_callback(
       void *data,
       struct AstNode *node)
{
    struct IffiGenerationContext *igc = (struct IffiGenerationContext *)data;
    struct GenerationContext *gc = igc->gc;
    struct AstNode *guard, *command;
    struct JumpRecord skip_jump, end_jump;

    // 1. Analyze the input AST node:
    if (node->type != AST_BINARY || node->binary.type != AST_BIN_IMPLY) {
        error_throw(
            &gc->error_context,
            "Non-guard expression in an if-fi block");
    }
    guard = node->binary.left;
    command = node->binary.right;

    // 2. Generate guard code:
    amd64_mem_subscope_create(&gc->mem);
    skip_jump = generate_jump_if_not(gc, guard);

    // 3. Generate the command code:
    generate_command_sequence(gc, command);
    amd64_mem_subscope_destroy(&gc->mem);

    // 4. Generate the jump towards the end:
    end_jump = generate_jmp_32(gc);
    array_append(igc->end_jumps, end_jump);

    // 5. Update the skip jump:
    jr_set_to_current(gc, &skip_jump);
}

static void generate_from_block_iffi(
        struct GenerationContext *gc,
        struct AstNode *body)
{
    struct IffiGenerationContext *igc;
    int i;

    // 1. Initialize the if-fi generation context:
    igc = gc_push_igc(gc);

    // 2. Generate code for the guarded commands:
    ast_for_each_left(
        body,
        AST_BIN_BLOCK_SEQUENCE,
        generate_from_block_iffi_callback,
        igc);

    if (igc->end_jumps.size == 0) {
        error_throw(
            &gc->error_context,
            "No guarded commands in an if-fi block");
    }

    // 3. Abort here in case no guard passed:
    generate_syscall_exit(gc);

    // 4. Fill in the end jumps:
    for (i = 0; i != igc->end_jumps.size; ++i) {
        jr_set_to_current(gc, igc->end_jumps.data + i);
    }

    // 5. Return the result of the successful guarded command:
    gc_pop_igc(gc, igc);
}

struct DoodGenerationContext {
    struct DoodGenerationContext *next;
    struct GenerationContext *gc;
    char *counter_name;
    struct AMD64Object *counter;
    int front_offset;
};

static struct DoodGenerationContext *gc_push_dgc(struct GenerationContext *gc)
{
    struct DoodGenerationContext *result = my_malloc(sizeof(*result));

    result->next = gc->dgc_garbage;
    result->gc = gc;
    result->counter_name = create_unique_name();
    result->counter = amd64_mem_create_stack_word(&gc->mem);
    result->front_offset = bb_current_offset(gc->bb);
    amd64_mem_put(&gc->mem, result->counter_name, result->counter);

    gc->dgc_garbage = result;

    return result;
}

static void gc_pop_dgc(
        struct GenerationContext *gc,
        struct DoodGenerationContext *dgc)
{
    if (gc->dgc_garbage != dgc || !dgc) {
        ABORT("Algorythmic corruption");
    }

    gc->dgc_garbage = gc->dgc_garbage->next;

    // Don't dispose the counter, as it is inserted into the scope
    my_free(dgc->counter_name);
    my_free(dgc);
}

static void generate_dood_counter_init(struct DoodGenerationContext *dgc)
{
    struct AMD64Object *src = amd64_mem_create_imm_word(&dgc->gc->mem, 0);
    generate_copy(dgc->gc, src, dgc->counter);
    amd64_mem_dispose(&dgc->gc->mem, src);
}

static void generate_dood_counter_increment(struct DoodGenerationContext *dgc)
{
    dgc->counter = generate_increment(dgc->gc, dgc->counter);
}

static void generate_dood_counter_jump(struct DoodGenerationContext *dgc)
{
    struct JumpRecord jr;
    struct AMD64Object *zero = amd64_mem_create_imm_word(&dgc->gc->mem, 0);
    (void)generate_comparison(dgc->gc, zero, dgc->counter);
    jr = generate_jne_32(dgc->gc);
    jr_set(dgc->gc, &jr, dgc->front_offset);
}

static void generate_from_block_dood_callback(
       void *data,
       struct AstNode *node)
{
    struct DoodGenerationContext *dgc = (struct DoodGenerationContext *)data;
    struct GenerationContext *gc = dgc->gc;
    struct AstNode *guard, *command;
    struct JumpRecord skip_jump;

    // 1. Analyze the input AST node:
    if (node->type != AST_BINARY || node->binary.type != AST_BIN_IMPLY) {
        error_throw(
            &gc->error_context,
            "Non-guard expression in an do-od block");
    }
    guard = node->binary.left;
    command = node->binary.right;

    // 2. Generate guard code:
    amd64_mem_subscope_create(&gc->mem);
    skip_jump = generate_jump_if_not(gc, guard);

    // 3. Generate the command code:
    generate_command_sequence(gc, command);
    amd64_mem_subscope_destroy(&gc->mem);

    // 4. Generate the counter increment if the guard test passed:
    generate_dood_counter_increment(dgc);

    // 5. Update the skip jump:
    jr_set_to_current(gc, &skip_jump);
}

static void generate_from_block_dood(
        struct GenerationContext *gc,
        struct AstNode *body)
{
    struct DoodGenerationContext *dgc;

    // 1. Initialize the do-od generation context:
    dgc = gc_push_dgc(gc);

    // 2. Setup the counter:
    generate_dood_counter_init(dgc);

    // 3. Generate code for the guarded commands:
    ast_for_each_left(
        body,
        AST_BIN_BLOCK_SEQUENCE,
        generate_from_block_dood_callback,
        dgc);

    // 4. Jump if any command executed:
    generate_dood_counter_jump(dgc);

    // 5. Return the result of the successful guarded command:
    gc_pop_dgc(gc, dgc);
}

// ### Main AST Node types

static struct AMD64Object *generate_from_literal(
        struct GenerationContext *gc,
        struct AstLiteral *literal)
{
    /*
     * This function dispatches between different types of literals.
     */

    switch (literal->type) {
    case AST_LIT_VOID:
    case AST_LIT_UNIT:
    case AST_LIT_BOOLEAN:
        error_throw(&gc->error_context, "Not implemented yet");
        return NULL;

    case AST_LIT_INTEGER:
        return amd64_mem_create_imm_word(&gc->mem, literal->integer);

    case AST_LIT_REAL:
        return procure_rip_double(gc, literal->real);

    case AST_LIT_CHARACTER:
        error_throw(&gc->error_context, "Not implemented yet");
        return NULL;

    case AST_LIT_STRING:
        return procure_rip_cstring(gc, literal->string);

    }
    ABORT("Impossible state");
}

static struct AMD64Object *generate_from_symbol(
        struct GenerationContext *gc,
        struct AstSymbol *symbol)
{
    struct AMD64Object *result = amd64_mem_find_by_name(&gc->mem, symbol->string);
    if (!result) {
        error_throwf(
            &gc->error_context,
            "Failed looking up symbol: %s",
            symbol->string);
    }
    return result;
}

static struct AMD64Object *generate_from_unary(
        struct GenerationContext *gc,
        struct AstUnary *unary)
{
    switch (unary->type) {
    case AST_UN_POSITIVE:
        return generate_from_ast(gc, unary->operand);

    case AST_UN_NEGATIVE:
        return generate_negation(
            gc, generate_from_ast(gc, unary->operand));

    case AST_UN_DEREFERENCE:
        return generate_dereference(
            gc, generate_from_ast(gc, unary->operand));

    case AST_UN_ADDRESS:
        error_throw(&gc->error_context, "Not implemented yet");
    }
    ABORT("Impossible state");
}

static struct AMD64Object *generate_from_binary(
        struct GenerationContext *gc,
        struct AstBinary *binary)
{
    switch (binary->type) {
    case AST_BIN_ADD:
        return generate_arithmetic(gc,
                                   generate_from_ast(gc, binary->right),
                                   generate_from_ast(gc, binary->left),
                                   true,
                                   true,
                                   generate_add,
                                   generate_addsd);
    case AST_BIN_SUBTRACT:
        return generate_arithmetic(gc,
                                   generate_from_ast(gc, binary->right),
                                   generate_from_ast(gc, binary->left),
                                   true,
                                   false,
                                   generate_sub,
                                   generate_subsd);
    case AST_BIN_MULTIPLY:
        return generate_multiplication(gc,
                                       generate_from_ast(gc, binary->right),
                                       generate_from_ast(gc, binary->left));
    case AST_BIN_DIVIDE:
        return generate_division(gc,
                                 generate_from_ast(gc, binary->left),
                                 generate_from_ast(gc, binary->right));
    case AST_BIN_MODULO:
        return generate_mod(gc,
                            generate_from_ast(gc, binary->left),
                            generate_from_ast(gc, binary->right));

    case AST_BIN_POWER:
        error_throw(&gc->error_context, "Not implemented yet");
        return NULL;

    case AST_BIN_ASSIGN:
        return generate_from_bin_assignment(gc, binary->left, binary->right);

    case AST_BIN_IMPLY:
    case AST_BIN_SEQUENCE:
    case AST_BIN_BLOCK_SEQUENCE:
    case AST_BIN_LIST_SEQUENCE:
    case AST_BIN_EQUAL:
    case AST_BIN_NOT_EQUAL:
    case AST_BIN_LESS_THAN:
    case AST_BIN_GREATER_THAN:
    case AST_BIN_LESS_THAN_OR_EQUAL:
    case AST_BIN_GREATER_THAN_OR_EQUAL:
    case AST_BIN_CONCATENATE:
        error_throw(&gc->error_context, "Not implemented yet");
        return NULL;

    case AST_BIN_PROC_CALL:
        return generate_from_bin_proc_call(gc, binary->left, binary->right);

    case AST_BIN_SUBSCRIPT:
    case AST_BIN_PROC_HEAD:
        error_throw(&gc->error_context, "Not implemented yet");
        return NULL;
    }
    ABORT("Impossible state");
}

static struct AMD64Object *generate_from_block(
        struct GenerationContext *gc,
        struct AstBlock *block)
{
    switch (block->type) {
    case AST_BLOCK_IFFI:
        generate_from_block_iffi(gc, block->body);
        return amd64_mem_create_imm_unit(&gc->mem);

    case AST_BLOCK_DOOD:
        generate_from_block_dood(gc, block->body);
        return amd64_mem_create_imm_unit(&gc->mem);

    case AST_BLOCK_PROCCORP:
        error_throw(&gc->error_context, "Not implemented yet");
        break;
    }
    ABORT("Impossible state");
}

// ### Main AST type based dispatch

static struct AMD64Object *generate_from_ast(
        struct GenerationContext *gc,
        struct AstNode *node)
{
    switch (node->type) {
    case AST_LITERAL:
        return generate_from_literal(gc, &node->literal);

    case AST_SYMBOL:
        return generate_from_symbol(gc, &node->symbol);

    case AST_UNARY:
        return generate_from_unary(gc, &node->unary);

    case AST_BINARY:
        return generate_from_binary(gc, &node->binary);

    case AST_BLOCK:
        return generate_from_block(gc, &node->block);
    }

    ABORT("Impossible state");
}

// Core generation code
// --------------------

static void generate_command_callback(void *data, struct AstNode *node)
{
    struct GenerationContext *gc = (struct GenerationContext *)data;
    command_begin(gc);
    amd64_mem_dispose(&gc->mem, generate_from_ast(gc, node));
    command_end(gc);
}

static void generate_command_sequence(struct GenerationContext *gc,
                                      struct AstNode *node)
{
    ast_for_each_left(
        node,
        AST_BIN_SEQUENCE,
        generate_command_callback,
        gc);
}

struct ProcedureGenerationResult amd64_generate_procedure_code(
        struct ProcedurePrototype *pp,
        struct DataTypeList *argument_types,
        struct SourceDebugInfo *sdi)
{
    // 1. Initialize the generation context:
    struct GenerationContext gc;
    gc_init(&gc, sdi);

    // 2. Prepare for "exceptions":
    if (error_catch(&gc.error_context)) {
        return gc_deinit_failure(&gc);
    }

    // 3. Generate code:
    procedure_begin(&gc, pp, argument_types);
    generate_command_sequence(&gc, pp->body);
    procedure_end(&gc, pp);

    // 4. Cleanup and return:
    return gc_deinit_success(&gc);
}

// Tests
// =====

void amd64_generator_test(struct TestContext *tc)
{
    (void)tc;
}
