/*
 * Copyright (C) 2020 Krzysztof Stachowiak
 *
 * This file is part of the Language Programming Language compiler.
 *
 * Language Programming Language is free software: you can redistribute it
 * and/or modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Language Programming Language compiler is distributed in the hope that it
 * will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "binary.h"
#include "common.h"

#include <assert.h>

// Binary buffer
// =============

struct BinaryBuffer *bb_create(void)
{
    struct BinaryBuffer *result = my_malloc(sizeof(*result));
    result->data = NULL;
    result->size = result->cap = 0;
    return result;
}

void bb_destroy(struct BinaryBuffer *bb)
{
    if (!bb) {
        return;
    }
    array_free(*bb);
    my_free(bb);
}

void bb_append_1(struct BinaryBuffer *bb, uint8_t byte)
{
    array_append(*bb, byte);
}

void bb_append_2(struct BinaryBuffer *bb, uint16_t two_bytes)
{
    bb_append_1(bb, (two_bytes >> 8) & 0xff);
    bb_append_1(bb, (two_bytes >> 0) & 0xff);
}

void bb_append_4(struct BinaryBuffer *bb, uint32_t four_bytes)
{
    bb_append_1(bb, (four_bytes >> 24) & 0xff);
    bb_append_1(bb, (four_bytes >> 16) & 0xff);
    bb_append_1(bb, (four_bytes >> 8) & 0xff);
    bb_append_1(bb, (four_bytes >> 0) & 0xff);
}

void bb_append_n(struct BinaryBuffer *bb, uint8_t *buffer, int n)
{
    int i;
    for (i = 0; i != n; ++i) {
        bb_append_1(bb, buffer[i]);
    }
}

void bb_inject(struct BinaryBuffer *bb, struct BinaryBuffer *src, int offset)
{
    uint8_t *new_buffer;

    assert(offset <= bb->size);

    new_buffer = my_malloc(bb->size + src->size);

    memcpy(new_buffer, bb->data, offset);
    memcpy(new_buffer + offset, src->data, src->size);
    memcpy(new_buffer + offset + src->size, bb->data + offset, bb->size - offset);

    my_free(bb->data);
    bb->data = new_buffer;
    bb->size = bb->cap = bb->size + src->size;
}

size_t bb_current_offset(struct BinaryBuffer *bb)
{
    return bb->size;
}

uint8_t *bb_at_offset(struct BinaryBuffer *bb, int offset)
{
    return bb->data + offset;
}

void bb_debug_print(struct BinaryBuffer *bb, FILE *out)
{
    int i;
    for (i = 0; i != bb->size; ++i) {
        fprintf(out, "%02x ", bb->data[i]);
    }
}
