/*
 * Copyright (C) 2020 Krzysztof Stachowiak
 *
 * This file is part of the Language Programming Language compiler.
 *
 * Language Programming Language is free software: you can redistribute it
 * and/or modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Language Programming Language compiler is distributed in the hope that it
 * will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "test.h"
#include "common.h"

#include <inttypes.h>
#include <math.h>

#define tc_make_cmp_description(DESCR, FORMAT, EXPECTED, OPERATOR, ACTUAL, RESULT) \
    do {                                                                \
        str_append(                                                     \
            (DESCR),                                                    \
            "Comparison " RESULT ": " FORMAT " " OPERATOR " " FORMAT,   \
            EXPECTED,                                                   \
            ACTUAL);                                                    \
    } while(0)


static void tc_report_rec(
        struct TestContext *tc,
        char *prefix,
        int *performed,
        int *successes,
        FILE *output,
        bool verbose)
{
    int i = 0;
    struct TestEntry *te;
    if (tc->name) {
        str_append(prefix, "[%s]", tc->name);
    }
    char *prefix_copy;
    for (i = 0, te = tc->entries.data; i < tc->entries.size; ++i, ++te) {
        if (te->result) {
            ++*successes;
            if (verbose) {
                fprintf(
                    output,
                    "[%s]%s : %s\n",
                    te->result ? " OK " : "FAIL",
                    prefix,
                    te->description);
            }
        }
        else {
            fprintf(
                output,
                "[%s]%s : %s\n",
                te->result ? " OK " : "FAIL",
                prefix,
                te->description);
        }
        ++*performed;
    }
    for (i = 0; i < tc->children.size; ++i) {
        prefix_copy = str_copy(prefix);
        tc_report_rec(
                tc->children.data[i],
                prefix_copy,
                performed,
                successes,
                output,
                verbose);
    }
    my_free(prefix);
}

struct TestContext *tc_create_root(char *name)
{
    struct TestContext *result = my_malloc(sizeof(*result));
    result->name = name;
    result->children.data = NULL;
    result->children.cap = 0;
    result->children.size = 0;
    result->entries.data = NULL;
    result->entries.cap = 0;
    result->entries.size = 0;
    return result;
}

struct TestContext *tc_create_child(char *name, struct TestContext *parent)
{
    struct TestContext *result = my_malloc(sizeof(*result));
    result->name = name;
    result->children.data = NULL;
    result->children.cap = 0;
    result->children.size = 0;
    result->entries.data = NULL;
    result->entries.cap = 0;
    result->entries.size = 0;
    array_append(parent->children, result);
    return result;
}

void tc_destroy_root(struct TestContext *tc)
{
    int i;
    if (!tc) {
        return;
    }
    for (i = 0; i < tc->children.size; ++i) {
        tc_destroy_root(tc->children.data[i]);
    }
    array_free(tc->children);
    for (i = 0; i < tc->entries.size; ++i) {
        my_free(tc->entries.data[i].description);
    }
    array_free(tc->entries);
    my_free(tc);
}

void tc_generic(
        struct TestContext *tc,
        char *description,
        bool result)
{
    struct TestEntry te = { NULL, result };
    str_append(te.description, "%s", description);
    array_append(tc->entries, te);
}

void tc_assert_equal_int8(
        struct TestContext *tc,
        char *description,
        int8_t expected,
        int8_t actual)
{
    struct TestEntry te = { NULL, expected == actual };
    str_append(te.description, "%s. ", description);
    if (te.result) {
        tc_make_cmp_description(te.description, "%d", expected, "=", actual, "success");
    } else {
        tc_make_cmp_description(te.description, "%d", expected, "=", actual, "failure");
    }
    array_append(tc->entries, te);
}

void tc_assert_equal_int64(
        struct TestContext *tc,
        char *description,
        int64_t expected,
        int64_t actual)
{
    struct TestEntry te = { NULL, expected == actual };
    str_append(te.description, "%s. ", description);
    if (te.result) {
        tc_make_cmp_description(te.description, "%"PRId64, expected, "=", actual, "success");
    } else {
        tc_make_cmp_description(te.description, "%"PRId64, expected, "=", actual, "failure");
    }
    array_append(tc->entries, te);
}

void tc_assert_equal_double(
        struct TestContext *tc,
        char *description,
        double expected,
        double actual)
{
    struct TestEntry te = { NULL, expected == actual };
    str_append(te.description, "%s. ", description);
    if (te.result) {
        tc_make_cmp_description(te.description, "%f", expected, "=", actual, "success");
    } else {
        tc_make_cmp_description(te.description, "%f", expected, "=", actual, "failure");
    }
    array_append(tc->entries, te);
}

void tc_assert_near_double(
        struct TestContext *tc,
        char *description,
        double expected,
        double actual,
        double epsilon)
{
    struct TestEntry te = { NULL, fabs(expected - actual) <= epsilon };
    str_append(te.description, "%s. ", description);
    if (te.result) {
        tc_make_cmp_description(te.description, "%f", expected, "=", actual, "success");
    } else {
        tc_make_cmp_description(te.description, "%f", expected, "=", actual, "failure");
    }
    array_append(tc->entries, te);
}

void tc_assert_equal_ptr(
        struct TestContext *tc,
        char *description,
        void *expected,
        void *actual)
{
    struct TestEntry te = { NULL, expected == actual };
    str_append(te.description, "%s. ", description);
    if (te.result) {
        tc_make_cmp_description(te.description, "%p", expected, "=", actual, "success");
    } else {
        tc_make_cmp_description(te.description, "%p", expected, "=", actual, "failure");
    }
    array_append(tc->entries, te);
}

void tc_failure(struct TestContext *tc, char *description) {
    tc_generic(tc, description, false);
}

void tc_report(struct TestContext *tc, FILE *output, bool verbose)
{
    char *prefix = NULL;
    int performed = 0, successes = 0;
    tc_report_rec(tc, prefix, &performed, &successes, output, verbose);
    printf("%d/%d tests passed.\n", successes, performed);
}
