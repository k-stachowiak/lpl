/*
 * Copyright (C) 2020 Krzysztof Stachowiak
 *
 * This file is part of the Language Programming Language compiler.
 *
 * Language Programming Language is free software: you can redistribute it
 * and/or modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Language Programming Language compiler is distributed in the hope that it
 * will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "parser.h"
#include "common.h"
#include "illiterate-tokenizer/token_stream.h"
#include "error.h"

#include <stdio.h>
#include <stdlib.h>
#include <stdbool.h>
#include <string.h>
#include <ctype.h>

// Debug info
// ==========

static uint64_t ast_hash(void *void_ast)
{
    return (uint64_t)void_ast;
}

static struct SrcRange *src_range_create(size_t first, size_t last)
{
    struct SrcRange *result = my_malloc(sizeof(*result));
    result->first = first;
    result->last = last;
    return result;
}

static void src_range_destroy(struct SrcRange *src_range)
{
    my_free(src_range);
}

static void src_range_destroy_v(void *void_src_range)
{
    src_range_destroy((struct SrcRange *)void_src_range);
}

static struct SourceDebugInfo *sdi_create(void)
{
    struct SourceDebugInfo *result = my_malloc(sizeof(*result));
    ht_init(&result->map, ast_hash);
    return result;
}

static void sdi_destroy(struct SourceDebugInfo *sdi)
{
    if (!sdi) {
        return;
    }
    ht_deinit(&sdi->map, src_range_destroy_v);
    my_free(sdi);
}

static void sdi_put(struct SourceDebugInfo *sdi,
                    struct AstNode *ast,
                    size_t first,
                    size_t last)
{
    ht_put(&sdi->map, (void *)ast, src_range_create(first, last));
}

struct SrcRange *sdi_get(struct SourceDebugInfo *sdi, struct AstNode *ast)
{
    return (struct SrcRange *)ht_get(&sdi->map, (void *)ast);
}

struct DebugPrintContext {
    char *source;
    FILE *out;
};

static void sdi_debug_print_callback(struct HashTableNode *node, void *data)
{
    char *temp_source;
    char *temp_ast;
    struct DebugPrintContext *ctx = (struct DebugPrintContext *)data;
    struct AstNode *ast_node = (struct AstNode *)node->key;
    struct SrcRange *src_range = (struct SrcRange *)node->value;

    temp_source = str_copy_range(ctx->source + src_range->first,
                                 ctx->source + src_range->last);

    temp_ast = ast_to_string_tree(ast_node);

    fprintf(ctx->out, "Source code: %s\n", temp_source);
    fprintf(ctx->out, "AST subtree: %s\n", temp_ast);

    my_free(temp_ast);
    my_free(temp_source);
}

void sdi_debug_print(struct SourceDebugInfo *sdi, char *source, FILE *out)
{
    struct DebugPrintContext ctx = {
        .source = source,
        .out = out
    };
    ht_for_each(&sdi->map,
                sdi_debug_print_callback,
                &ctx);
}

// Parser
// ======

struct Parser {
    bool gather_debug_info;
    struct ErrorContext error_context;
    struct SourceDebugInfo *sdi;
    struct TokenStream ts;
    struct PtrStack operand_stack;
    struct PtrStack operator_stack;
    struct PtrStack delimiter_stack;
};

// Lexical analysis
// ----------------

static int is_allowed_in_symbol(int c)
{
    return isalnum(c) || c == '_';
}

static bool is_literal_void(char *token)
{
    return strcmp(token, "void") == 0;
}

static bool is_literal_unit(char *token)
{
    return strcmp(token, "unit") == 0;
}

static bool is_literal_boolean(char *token)
{
    return (strcmp(token, "true") == 0) || (strcmp(token, "false") == 0);
}

static bool is_literal_integer(char *token)
{
    if (*token == '+' || *token == '-') {
        ++token;
    }
    return all_of(token, token + strlen(token), isdigit);
}

static bool is_literal_real(char *token)
{
    char *first = token, *last = token + strlen(token);
    char *dot = find(first, last, '.');

    if (*first == '+' || *first == '-') {
        ++first;
    }

    if (dot == last) {
        return false;
    }

    return
        all_of(first, dot, isdigit) &&
        all_of(dot + 1, last, isdigit);
}

static bool is_literal_character(char *token)
{
    int len = strlen(token);
    switch (len) {
    case 3:
        return
            token[0] == '\'' &&
            token[2] == '\'' &&
            token[1] != '\\';
    case 4:
        return
            token[0] == '\'' &&
            token[1] == '\\' &&
            token[3] == '\'' &&
            token[2] != '\\';
    default:
        return false;
    }
}

static bool is_literal_string(char *token)
{
    int len = strlen(token);
    return len >= 2 && token[0] == '"' && token[len - 1] == '"';
}

static bool is_literal(char *token)
{
    return
        is_literal_void(token) ||
        is_literal_unit(token) ||
        is_literal_boolean(token) ||
        is_literal_integer(token) ||
        is_literal_real(token) ||
        is_literal_character(token) ||
        is_literal_string(token);
}

static bool is_symbol(char *token)
{
    char *first = token, *last = token + strlen(token);
    if (isdigit(*first)) {
        return false;
    }
    return all_of(first, last, is_allowed_in_symbol);
}

static bool is_terminal(char *token)
{
    return
        is_literal(token) ||
        is_symbol(token);
}

static bool is_unary(char *token)
{
    return
        strcmp(token, "-") == 0 ||
        strcmp(token, "+") == 0 ||
        strcmp(token, "^") == 0 ||
        strcmp(token, "@") == 0;
}

static bool is_delimiter_opening(char *token)
{
    return
        strcmp(token, "(") == 0 ||
        strcmp(token, "[") == 0 ||
        strcmp(token, "if") == 0 ||
        strcmp(token, "do") == 0 ||
        strcmp(token, "proc") == 0;
}

static bool is_delimiter_closing(char *token)
{
    return
        strcmp(token, ")") == 0 ||
        strcmp(token, "]") == 0 ||
        strcmp(token, "fi") == 0 ||
        strcmp(token, "od") == 0 ||
        strcmp(token, "corp") == 0;
}

static bool is_matching_delimiter_closing(char *token, char *opening)
{
    return
        (strcmp(opening, "(") == 0 && strcmp(token, ")") == 0) ||
        (strcmp(opening, "[") == 0 && strcmp(token, "]") == 0) ||
        (strcmp(opening, "if") == 0 && strcmp(token, "fi") == 0) ||
        (strcmp(opening, "do") == 0 && strcmp(token, "od") == 0) ||
        (strcmp(opening, "proc") == 0 && strcmp(token, "corp") == 0);
}

// Operator descriptors
// --------------------

enum OperatorDescriptorType {
    OD_UNARY,
    OD_BINARY,
    OD_BLOCK
};

struct OperatorDescriptor {
    enum OperatorDescriptorType type;
    size_t source_first, source_last;
    union {
        enum AstUnaryType unary_type;
        enum AstBinaryType binary_type;
        enum AstBlockType block_type;
    };
};

static struct OperatorDescriptor *descr_create_unary(
        enum AstUnaryType type,
        size_t source_first,
        size_t source_last)
{
    struct OperatorDescriptor *result = my_malloc(sizeof(*result));
    result->type = OD_UNARY;
    result->source_first = source_first;
    result->source_last = source_last;
    result->unary_type = type;
    return result;
}

static struct OperatorDescriptor *descr_create_binary(
        enum AstBinaryType type,
        size_t source_first,
        size_t source_last)
{
    struct OperatorDescriptor *result = my_malloc(sizeof(*result));
    result->type = OD_BINARY;
    result->source_first = source_first;
    result->source_last = source_last;
    result->binary_type = type;
    return result;
}

static struct OperatorDescriptor *descr_create_block(
        enum AstBlockType type,
        size_t source_first,
        size_t source_last)
{
    struct OperatorDescriptor *result = my_malloc(sizeof(*result));
    result->type = OD_BLOCK;
    result->source_first = source_first;
    result->source_last = source_last;
    result->block_type = type;
    return result;
}

static void descr_destroy(struct OperatorDescriptor *descr)
{
    my_free(descr);
}

// Parser specific operations
// --------------------------

static void prs_put_debug_info(struct Parser *parser,
                               struct AstNode *last_node,
                               size_t source_first,
                               size_t source_last)
{
    sdi_put(parser->sdi, last_node, source_first, source_last);
}

static struct AstNode *prs_create_terminal(struct Token *token)
{
    // We assume thet the input token has already been verified to be a proper
    // terminal token. Therefore if none of the cases matches below it is a
    // serious error.
    if (is_literal_void(token->string)) {
        return ast_create_literal_void();
    } else if (is_literal_unit(token->string)) {
        return ast_create_literal_unit();
    } else if (is_literal_boolean(token->string)) {
        if (token->string[0] == 't') {
            return ast_create_literal_bool(true);
        } else {
            return ast_create_literal_bool(false);
        }
    } else if (is_literal_integer(token->string)) {
        return ast_create_literal_integer(atoll(token->string));
    } else if (is_literal_real(token->string)) {
        return ast_create_literal_real(atof(token->string));
    } else if (is_literal_character(token->string)) {
        return ast_create_literal_character(token->string[1]);
    } else if (is_literal_string(token->string)) {
        return ast_create_literal_string(token->string);
    } else if (is_symbol(token->string)) {
        return ast_create_symbol(token->string);
    } else {
        ABORT("Impossible state");
    }
}

static struct OperatorDescriptor *prs_descr_create_unary(struct Token *token)
{
    // We assume that the input token has already been verified to be a valid
    // unary operator, therefore if no cases is matched here, it is a serious
    // error.
    if (strcmp(token->string, "+") == 0) {
        return descr_create_unary(AST_UN_POSITIVE,
                                  token->source_offset,
                                  token->source_offset + 1);
    } else if (strcmp(token->string, "-") == 0) {
        return descr_create_unary(AST_UN_NEGATIVE,
                                  token->source_offset,
                                  token->source_offset + 1);
    } else if (strcmp(token->string, "^") == 0) {
        return descr_create_unary(AST_UN_DEREFERENCE,
                                  token->source_offset,
                                  token->source_offset + 1);
    } else if (strcmp(token->string, "@") == 0) {
        return descr_create_unary(AST_UN_ADDRESS,
                                  token->source_offset,
                                  token->source_offset + 1);
    }
    ABORT("Impossible state");
}

static struct OperatorDescriptor *prs_descr_create_block(
        enum AstBlockType type,
        size_t source_first,
        size_t source_last)
{
    return descr_create_block(type, source_first, source_last);
}

static struct OperatorDescriptor *prs_descr_create_binary(struct Token *token)
{
    // We assume that the input token has NOT yet been verified to be a valid
    // binary operator, therefore, if no case is matched here, we return NULL
    // and allow the caller handle this problem.
    size_t source_first = token->source_offset;
    size_t source_last = source_first = strlen(token->string);
    enum AstBinaryType type;
    if (strcmp(token->string, "+") == 0) {
        type = AST_BIN_ADD;
    } else if (strcmp(token->string, "-") == 0) {
        type = AST_BIN_SUBTRACT;
    } else if (strcmp(token->string, "*") == 0) {
        type = AST_BIN_MULTIPLY;
    } else if (strcmp(token->string, "/") == 0) {
        type = AST_BIN_DIVIDE;
    } else if (strcmp(token->string, "%") == 0) {
        type = AST_BIN_MODULO;
    } else if (strcmp(token->string, "^") == 0) {
        type = AST_BIN_POWER;
    } else if (strcmp(token->string, ":=") == 0) {
        type = AST_BIN_ASSIGN;
    } else if (strcmp(token->string, "=>") == 0) {
        type = AST_BIN_IMPLY;
    } else if (strcmp(token->string, ";") == 0) {
        type = AST_BIN_SEQUENCE;
    } else if (strcmp(token->string, "#") == 0) {
        type = AST_BIN_BLOCK_SEQUENCE;
    } else if (strcmp(token->string, ",") == 0) {
        type = AST_BIN_LIST_SEQUENCE;
    } else if (strcmp(token->string, "=") == 0) {
        type = AST_BIN_EQUAL;
    } else if (strcmp(token->string, "!=") == 0) {
        type = AST_BIN_NOT_EQUAL;
    } else if (strcmp(token->string, "<") == 0) {
        type = AST_BIN_LESS_THAN;
    } else if (strcmp(token->string, ">") == 0) {
        type = AST_BIN_GREATER_THAN;
    } else if (strcmp(token->string, "<=") == 0) {
        type = AST_BIN_LESS_THAN_OR_EQUAL;
    } else if (strcmp(token->string, ">=") == 0) {
        type = AST_BIN_GREATER_THAN_OR_EQUAL;
    } else if (strcmp(token->string, "->") == 0) {
        type = AST_BIN_PROC_HEAD;
    } else if (strcmp(token->string, "~") == 0) {
        type = AST_BIN_CONCATENATE;
    } else {
        return NULL;
    }
    return descr_create_binary(type, source_first, source_last);
}

static void prs_ast_destroy(void *node)
{
    ast_destroy((struct AstNode *)node);
}

static void prs_descr_destroy(void *descr)
{
    descr_destroy((struct OperatorDescriptor *)descr);
}

static void prs_delimiter_destroy(void *delim)
{
    my_free((char*)delim);
}

// Operator properties
// -------------------

static int precedence(struct OperatorDescriptor *x)
{
    switch (x->binary_type) {
    case AST_BIN_BLOCK_SEQUENCE:
        return 0;
    case AST_BIN_PROC_HEAD:
        return 1;
    case AST_BIN_IMPLY:
        return 2;
    case AST_BIN_SEQUENCE:
        return 3;
    case AST_BIN_ASSIGN:
        return 4;
    case AST_BIN_LIST_SEQUENCE:
        return 5;
    case AST_BIN_LESS_THAN:
    case AST_BIN_GREATER_THAN:
    case AST_BIN_LESS_THAN_OR_EQUAL:
    case AST_BIN_GREATER_THAN_OR_EQUAL:
        return 6;
    case AST_BIN_EQUAL:
    case AST_BIN_NOT_EQUAL:
        return 7;
    case AST_BIN_ADD:
    case AST_BIN_SUBTRACT:
    case AST_BIN_CONCATENATE:
        return 8;
    case AST_BIN_MULTIPLY:
    case AST_BIN_DIVIDE:
    case AST_BIN_MODULO:
        return 9;
    case AST_BIN_POWER:
        return 10;
    case AST_BIN_SUBSCRIPT:
        return 11;
    case AST_BIN_PROC_CALL:
        return 12;
    }

    ABORT("Impossible state");
}

static bool is_left_associative(struct OperatorDescriptor *x)
{
    switch (x->binary_type) {
    case AST_BIN_ADD:
    case AST_BIN_SUBTRACT:
    case AST_BIN_MULTIPLY:
    case AST_BIN_DIVIDE:
    case AST_BIN_MODULO:
    case AST_BIN_SEQUENCE:
    case AST_BIN_BLOCK_SEQUENCE:
    case AST_BIN_EQUAL:
    case AST_BIN_NOT_EQUAL:
    case AST_BIN_LESS_THAN:
    case AST_BIN_GREATER_THAN:
    case AST_BIN_LESS_THAN_OR_EQUAL:
    case AST_BIN_GREATER_THAN_OR_EQUAL:
    case AST_BIN_CONCATENATE:
    case AST_BIN_IMPLY:
    case AST_BIN_PROC_CALL:
    case AST_BIN_SUBSCRIPT:
    case AST_BIN_PROC_HEAD:
        return true;
    case AST_BIN_POWER:
    case AST_BIN_ASSIGN:
    case AST_BIN_LIST_SEQUENCE:
        return false;
    }

    ABORT("Impossible state");
}

// This predicate helps establishing the operator precedence. In the precedence
// resolution the block operaetors are treated just like unary operators
// therefore there is division into binary and unary-like (rather than just
// unary) operators and this predicate enables the proper recognition. This
// means that the operators for which this predicate isn't satisfied are binary.
static bool is_unary_or_block(struct OperatorDescriptor *x)
{
    return x->type == OD_UNARY || x->type == OD_BLOCK;
}

bool op_less(
        struct OperatorDescriptor *x,
        struct OperatorDescriptor *y)
{
    if (!x) {
        // x is a sentinel
        return true;
    }

    if (!y) {
        // y is a sentinel
        return false;
    }

    if (is_unary_or_block(x)) {
        if (is_unary_or_block(y)) {
            // Both unary-like
            return false;
        } else {
            // x unary-like, y binary
            return false;
        }
    } else {
        if (is_unary_or_block(y)) {
            // x binary, y unary-like
            return true;
        } else {
            // Both binary
            int x_precedence = precedence(x);
            int y_precedence = precedence(y);
            if (x_precedence < y_precedence) {
                return true;
            } else if (x_precedence > y_precedence) {
                return false;
            } else {
                return is_left_associative(y);
            }
        }
    }
}

// The shunting yard algorithm:
// ----------------------------

static void parse_result_init_success(struct ParseResult *pr,
                                      struct AstNode *result,
                                      struct SourceDebugInfo *sdi)
{
    pr->error_message = NULL;
    pr->result = result;
    pr->sdi = sdi;
}

static void parse_result_init_failure(struct ParseResult *pr,
                                      char *error_message,
                                      struct SourceDebugInfo *sdi)
{
    pr->error_message = str_copy(error_message);
    pr->result = NULL;
    pr->sdi = sdi;
}

void parse_result_deinit(struct ParseResult *pr)
{
    if (pr->sdi) {
        sdi_destroy(pr->sdi);
    }
    if (pr->result) {
        ast_destroy(pr->result);
    }
    my_free(pr->error_message);
}

static void init_token_stream(struct TokenStream *ts, struct TokCharStream *cs)
{
    char *hard_tokens[] = {
        ":=",
        "=>",
        ">=",
        "<=",
        "!=",
        "->",
        ",",
        "#",
        "=",
        ";",
        "*",
        "/",
        "%",
        "+",
        "-",
        "^",
        "(",
        ")",
        "[",
        "]",
        "<",
        ">",
        "~",
        "^",
        "@"
    };

    char *delimiters[][2] = {
        { "\"", "\"" },
        { "'", "'" }
    };

    tok_ts_init(ts, cs);
    tok_ts_add_hard_tokens(ts, hard_tokens, sizeof(hard_tokens)/sizeof(hard_tokens[0]));
    tok_ts_add_delimiter_pairs(ts, delimiters, sizeof(delimiters)/sizeof(delimiters[0]));
}

static void parser_init(struct Parser *parser,
                        struct TokCharStream *cs,
                        bool gather_debug_info)
{
    parser->gather_debug_info = gather_debug_info;
    error_init(&parser->error_context);
    parser->sdi = gather_debug_info ? sdi_create() : NULL;
    init_token_stream(&parser->ts, cs);
    ptr_stack_init(&parser->operand_stack);
    ptr_stack_init(&parser->operator_stack);
    ptr_stack_init(&parser->delimiter_stack);
}

static struct ParseResult parser_deinit_success(struct Parser *parser)
{
    struct ParseResult pr;

    parse_result_init_success(&pr, ptr_stack_top(&parser->operand_stack), parser->sdi);
    ptr_stack_pop(&parser->operand_stack);

    ptr_stack_deinit(&parser->delimiter_stack, prs_delimiter_destroy);
    ptr_stack_deinit(&parser->operator_stack, prs_descr_destroy);
    ptr_stack_deinit(&parser->operand_stack, prs_ast_destroy);
    tok_ts_deinit(&parser->ts);
    error_deinit(&parser->error_context);

    return pr;
}

static struct ParseResult parser_deinit_failure(struct Parser *parser)
{
    struct ParseResult pr;

    parse_result_init_failure(&pr, parser->error_context.message, parser->sdi);

    ptr_stack_deinit(&parser->delimiter_stack, prs_delimiter_destroy);
    ptr_stack_deinit(&parser->operator_stack, prs_descr_destroy);
    ptr_stack_deinit(&parser->operand_stack, prs_ast_destroy);
    tok_ts_deinit(&parser->ts);
    error_deinit(&parser->error_context);

    return pr;
}

static void promote_operator(struct Parser *parser)
{
    // Promoting an operator consists in:
    // 1. Taking an operator descriptor off the operator stack and an
    //    appropriate amount of operands off the operand stack,
    // 2. creation of an operator node that includes the aforementioned operands,
    // 3. putting the new node on the operand stack.

    struct AstNode *new_operand;

    // 1. Acquisition of the operator descriptor:
    struct OperatorDescriptor *descr = ptr_stack_top(&parser->operator_stack);
    ptr_stack_pop(&parser->operator_stack);

    // 2. Creation of the new operand node:
    if (descr->type == OD_UNARY) {
        struct AstNode *x;

        if (parser->operand_stack.size < 1) {
            error_throw(&parser->error_context, "Algebraic structure error");
        }

        // 2.a)1. For the unary node we take one operand off the stack:
        x = ptr_stack_top(&parser->operand_stack);
        ptr_stack_pop(&parser->operand_stack);

        // 2.a)2. We create the new compound node:
        new_operand = ast_create_unary(descr->unary_type, x);

    } else if (descr->type == OD_BINARY) {
        struct AstNode *x, *y;

        if (parser->operand_stack.size < 2) {
            error_throw(&parser->error_context, "Algebraic structure error");
        }

        // 2.b)1. For the binary node we take two operands off the stack:
        y = ptr_stack_top(&parser->operand_stack);
        ptr_stack_pop(&parser->operand_stack);
        x = ptr_stack_top(&parser->operand_stack);
        ptr_stack_pop(&parser->operand_stack);

        // 2.b)2. We create the new compound node:
        new_operand = ast_create_binary(descr->binary_type, x, y);

    } else if (descr->type == OD_BLOCK) {
        struct AstNode *x;

        if (parser->operand_stack.size < 1) {
            error_throw(&parser->error_context, "Algebraic structure error");
        }

        // 2.c)1. The block operator only takes one operand:
        x = ptr_stack_top(&parser->operand_stack);
        ptr_stack_pop(&parser->operand_stack);

        // 2.c)2. We create the new compound node:
        new_operand = ast_create_block(descr->block_type, x);

    } else {
        ABORT("Impossible parser");
    }

    // 3. Try registering the new operand in the debug info structure:
    if (parser->gather_debug_info) {
        prs_put_debug_info(parser,
                           new_operand,
                           descr->source_first,
                           descr->source_last);
    }

    // 4. We put the new operand on the stack:
    ptr_stack_push(&parser->operand_stack, new_operand);

    // 5. Cleanup:
    descr_destroy(descr);
}

static void push_operator(
        struct Parser *parser,
        struct OperatorDescriptor *op)
{
    // 1. Handle top of the operator stack if higher priority operator present:
    while (op_less(op, ptr_stack_top(&parser->operator_stack))) {
        promote_operator(parser);
    }

    // 2. Put the input operator on the appropriate stack:
    ptr_stack_push(&parser->operator_stack, op);
}

static void push_delimiter(
        struct Parser *parser,
        char *delimiter)
{
    ptr_stack_push(&parser->delimiter_stack, str_copy(delimiter));
}

static char *top_delimiter(struct Parser *parser)
{
    return ptr_stack_top(&parser->delimiter_stack);
}

static void pop_delimiter(struct Parser *parser)
{
    my_free(top_delimiter(parser));
    ptr_stack_pop(&parser->delimiter_stack);
}

static void parse_expression(struct Parser *parser);

static void parse_block(struct Parser *parser)
{
    struct Token token;
    int len;

    // 1. Store and skip the opening delimiter:
    token = tok_ts_get_next(&parser->ts);
    push_delimiter(parser, token.string);
    tok_deinit(&token);

    // 2. Parse block recursively:
    ptr_stack_push(&parser->operator_stack, NULL);
    parse_expression(parser);
    if (ptr_stack_top(&parser->operator_stack) != NULL) {
        error_throw(&parser->error_context, "Failed parsing block internals");
    }
    ptr_stack_pop(&parser->operator_stack);

    // 3. Detect source end after the recursion:
    if (!tok_ts_has_next(&parser->ts)) {
        error_throwf(
            &parser->error_context,
            "End of source before closing of a delimiter \"%s\"",
            top_delimiter(parser));
    }

    // 4. We verify if a proper closing delimiter has been found. If so, a
    //    proper astion is taken. The following reactions are possible:
    //    * For the parentesis and bracket blocks we just leave the expression
    //      on the operands stack,
    //    * some blocks, e.g. iffi, are treated as if they were unary operators
    //      over whatever is inside the block.
    token = tok_ts_get_next(&parser->ts);
    len = strlen(token.string);
    if (!is_matching_delimiter_closing(
            token.string,
            top_delimiter(parser))) {
        tok_deinit(&token);
        error_throwf(
            &parser->error_context,
            "Invalid delimiter \"%s\"; currently open is \"%s\"",
            token.string,
            top_delimiter(parser));
    } else if (strcmp(token.string, ")") == 0) {
        // nop
    } else if (strcmp(token.string, "]") == 0) {
        // nop
    } else if (strcmp(token.string, "fi") == 0) {
        push_operator(parser,
                      prs_descr_create_block(
                          AST_BLOCK_IFFI,
                          token.source_offset,
                          token.source_offset + len));
    } else if (strcmp(token.string, "od") == 0) {
        push_operator(parser,
                      prs_descr_create_block(
                          AST_BLOCK_DOOD,
                          token.source_offset,
                          token.source_offset + len));
    } else if (strcmp(token.string, "corp") == 0) {
        push_operator(parser,
                      prs_descr_create_block(
                          AST_BLOCK_PROCCORP,
                          token.source_offset,
                          token.source_offset + len));
    } else {
        ABORT("Impossible parser");
    }

    // 5. Cleanup:
    tok_deinit(&token);
    pop_delimiter(parser);
}

static void parse_primary(struct Parser *parser)
{
    char *next;
begin:

    // 0. Detect source end:
    if (!tok_ts_has_next(&parser->ts)) {
        error_throw(
            &parser->error_context,
            "End of source encountered instead of a primary expression");
    }

    // 1. Read another token:
    next = tok_ts_peek_next(&parser->ts);

    // 2. Recognize a primary expression:
    if (is_delimiter_opening(next)) {
        // 2.a) delimited expression handled recursively:
        parse_block(parser);

    } else if (is_unary(next)) {
        // 2.b) explicit detection of an unary operator:
        // In this case we reenter the procedure as unary operator is expected
        // to be followed by another primary expression.
        struct Token token = tok_ts_get_next(&parser->ts);
        push_operator(parser, prs_descr_create_unary(&token));
        tok_deinit(&token);
        goto begin;

    } else if (is_terminal(next)) {
        // 2.c) explicit detection of a terminal expression:
        struct Token token = tok_ts_get_next(&parser->ts);
        struct AstNode *node = prs_create_terminal(&token);
        ptr_stack_push(&parser->operand_stack, node);
        if (parser->gather_debug_info) {
            prs_put_debug_info(parser,
                               node,
                               token.source_offset,
                               token.source_offset + strlen(token.string));
        }
        tok_deinit(&token);

    } else {
        // 2.d) unhandled case:
        error_throwf(
            &parser->error_context,
            "Unexpected non-primary expression \"%s\"",
            next);
    }
}

static void parse_binary(struct Parser *parser)
{
    // The binary operator may be detected in two ways:
    // a) explicitly - we recognize a token that represents an operator,
    // b) implicit - if an expression is followed by an opening parenthesis of
    //    we recognize a procedure call, an index operator, etc. In such case the
    //    expression before the parenthesis is taken as the left argument and
    //    the expression in the parentheses is taken as the right argument of
    //    such an implicit operator.

    char *next;
    struct OperatorDescriptor *new_descr;

    // 0. Detect source end:
    if (!tok_ts_has_next(&parser->ts)) {
        error_throwf(
            &parser->error_context,
            "Expected a binary operator but encoutnered the end of the source");
    }

    // 1. Parse the current operator:
    next = tok_ts_peek_next(&parser->ts);
    if (strcmp(next, "(") == 0) {
        // 1.a) implicit procedure call operator:
        size_t offset = tok_ts_peek_offset(&parser->ts);
        new_descr = descr_create_binary(AST_BIN_PROC_CALL, offset, offset + 1);

    } else if (strcmp(next, "[") == 0) {
        // 1.b) implicit subscript operator:
        size_t offset = tok_ts_peek_offset(&parser->ts);
        new_descr = descr_create_binary(AST_BIN_SUBSCRIPT, offset, offset + 1);

    } else {
        // 1.c) explicit operator:
        struct Token token = tok_ts_get_next(&parser->ts);
        new_descr = prs_descr_create_binary(&token);
        if (!new_descr) {
            tok_deinit(&token);
            error_throwf(
                &parser->error_context,
                "Failed creating a binary operator from token \"%s\"",
                token.string);
        }
        tok_deinit(&token);
    }

    // 2. Store operator descriptor on the appropriate stack:
    push_operator(parser, new_descr);
}

static void parse_expression(struct Parser *parser)
{
    // The algebraic expression is a single primary expression or a primary
    // expression followed by a zero or more of pairs: (binary operator,
    // primary expression).

    // 0. Detect source end:
    if (!tok_ts_has_next(&parser->ts)) {
        error_throw(
            &parser->error_context,
            "End of source encountered instead of an expression");
    }

    // 1. Parse a primary expression:
    parse_primary(parser);

    // 2. Parse the operator-operand pairs:
    while (tok_ts_has_next(&parser->ts)) {
        if (is_delimiter_closing(tok_ts_peek_next(&parser->ts))) {
            break;
        }
        parse_binary(parser);
        parse_primary(parser);
    }

    // 3. Handle the remainder of the operator stack:
    while (ptr_stack_top(&parser->operator_stack)) {
        promote_operator(parser);
    }
}

static void assert_postconditions(struct Parser *parser)
{
    // 1. Assert source code depletion:
    // We assume that the entire input string is parsed. If the input source is
    // not depleted after the parser operation it is considered an error.
    if (tok_ts_has_next(&parser->ts)) {
        error_throwf(
            &parser->error_context,
            "Not the entire source has been parserd");
    }

    // 2. Assert result presence:
    // After the parser is done there is only a single element allowed remaining
    // on the operand stack (which is the algorithm's result).
    if (parser->operand_stack.size != 1) {
        error_throw(
            &parser->error_context,
            "Operand stack corruption");
    }
}

struct ParseResult parse(struct TokCharStream *cs, bool gather_debug_info)
{
    struct Parser parser;

    // 1. Initialize parser parser:
    parser_init(&parser, cs, gather_debug_info);

    // 2. Prepare for "exceptions":
    if (error_catch(&parser.error_context)) {
        return parser_deinit_failure(&parser);
    }

    // 3. Detect empty source:
    if (!tok_ts_has_next(&parser.ts)) {
        error_throw(&parser.error_context, "Empty source");
    }

    // 4. Parse the source assuming it is an expression:
    ptr_stack_push(&parser.operator_stack, NULL);
    parse_expression(&parser);
    if (tok_ts_has_next(&parser.ts) &&
        is_delimiter_closing(tok_ts_peek_next(&parser.ts))) {
        error_throw(
            &parser.error_context,
            "Closing delimiter follows non-block expression");
    }
    if (ptr_stack_top(&parser.operator_stack) != NULL) {
        error_throw(
            &parser.error_context,
            "Failed parsing toplevel expression");
    }
    ptr_stack_pop(&parser.operator_stack);

    // 5. Assert the postconditions:
    assert_postconditions(&parser);

    // 6. Cleanup and return the result:
    return parser_deinit_success(&parser);
}

// Tests
// -----

static void expect_predicate(
        struct TestContext *tc,
        bool (*predicate)(char*),
        char *source,
        bool expected)
{
    char *description = NULL;
    bool actual = predicate(source);
    bool result;
    if (actual != expected) {
        str_append(description, "Parser predicate failed analyzing token %s", source);
        result = false;
    } else {
        str_append(description, "Parser predicate succeeded analyzing token %s", source);
        result = true;
    }
    tc_generic(tc, description, result);
    my_free(description);
}

static void semantic_predicates_test(struct TestContext *tc_parent)
{
    struct TestContext *tc = tc_create_child("SEM-PRED", tc_parent);
    expect_predicate(tc, is_literal_boolean, "true", true);
    expect_predicate(tc, is_literal_boolean, "false", true);
    expect_predicate(tc, is_literal_boolean, "123", false);

    expect_predicate(tc, is_literal_integer, "123", true);
    expect_predicate(tc, is_literal_integer, "+123", true);
    expect_predicate(tc, is_literal_integer, "-123", true);
    expect_predicate(tc, is_literal_integer, "true", false);

    expect_predicate(tc, is_literal_real, "1.0", true);
    expect_predicate(tc, is_literal_real, "+2.0", true);
    expect_predicate(tc, is_literal_real, "-3.0", true);
    expect_predicate(tc, is_literal_real, "asdf", false);
    expect_predicate(tc, is_literal_real, "1", false);

    expect_predicate(tc, is_literal_character, "'a'", true);
    expect_predicate(tc, is_literal_character, "'\\n'", true);
    expect_predicate(tc, is_literal_character, "'a", false);
    expect_predicate(tc, is_literal_character, "a'", false);
    expect_predicate(tc, is_literal_character, "a", false);

    expect_predicate(tc, is_symbol, "as_df", true);
    expect_predicate(tc, is_symbol, "asdf_1", true);
    expect_predicate(tc, is_symbol, "1asdf", false);
}

static void test_parse(
        struct TestContext *tc,
        char *source,
        bool expected_success)
{
    struct TokCharStream cs;
    struct ParseResult pr;

    tok_cs_init_string(&cs, source);
    pr = parse(&cs, false);
    tok_cs_deinit(&cs);

    char *description = NULL;
    bool result;
    if (pr.result) {
        if (expected_success) {
            str_append(description, "Source \"%s\" parsed as expected", source);
            result = true;
        } else {
            str_append(description, "Source \"%s\" parsed unexpectedly", source);
            result = false;
        }
    } else {
        if (expected_success) {
            str_append(description, "Source \"%s\" failed to parse unexpectedly", source);
            result = false;
        } else {
            str_append(description, "Source \"%s\" failed to parse as expected", source);
            result = true;
        }
    }
    tc_generic(tc, description, result);
    parse_result_deinit(&pr);
    my_free(description);
}

static void basic_test(struct TestContext *tc_parent)
{
    struct TestContext *tc = tc_create_child("BASIC", tc_parent);
    test_parse(tc, "1", true);
    test_parse(tc, "do", false);
    test_parse(tc, "2; 2", true);
    test_parse(tc, "2;", false);
    test_parse(tc, "2; 2 * 2", true);
    test_parse(tc, "a := 1", true);
    test_parse(tc, "(2; 2) * 2", true);
    test_parse(tc, "1 * 2 * 3", true);
    test_parse(tc, "x := y ; 42", true);
    test_parse(tc, "if true => 1 fi", true);
    test_parse(tc, "main(unit)", true);
    test_parse(tc, "sin(3.1415)", true);
    test_parse(tc, "do true => 1; false => 2 fi", false);
    test_parse(tc, "if 3", false);
    test_parse(tc, "1 od", false);
    test_parse(tc, "+ + +", false);
    test_parse(tc, "+ - + - 1", true);
}

void parser_test(struct TestContext *tc_parent)
{
    struct TestContext *tc = tc_create_child("PARSER", tc_parent);
    semantic_predicates_test(tc);
    basic_test(tc);
}
