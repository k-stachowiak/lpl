# Copyright (C) 2020 Krzysztof Stachowiak
#
# This file is part of the Language Programming Language compiler.
#
# Language Programming Language is free software: you can redistribute it
# and/or modify it under the terms of the GNU General Public License as
# published by the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# Language Programming Language compiler is distributed in the hope that it
# will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with Framework.  If not, see <http://www.gnu.org/licenses/>.

# External commands
# =================
COMMAND-DEPENDENCIES = gcovr valgrind
X := $(foreach Y, $(COMMAND-DEPENDENCIES),\
         $(if $(shell command -v $(Y)), \
              nop,\
              $(warning "No $(Y) in PATH, some targets will not be makable!")))

# Basic build flags
# =================
CFLAGS = -Wall -Wextra -MD -MP
LDFLAGS = -lm

# Optional build flags
# ====================

# Basic builds
# ------------
RELEASE-FLAGS = -O2
DEBUG-FLAGS = -O0 -g3

# Coverage build
# --------------
COV-FLAGS = -fprofile-arcs -ftest-coverage

# Sanitizer build
# ---------------
SAN-FLAGS = -fsanitize=address -fsanitize=leak -fsanitize=undefined -static-libasan

# Auto-define dependencies
# ========================
SOURCES = $(wildcard \
    *.c \
    illiterate-amd64-encoder/amd64*.c \
    illiterate-tokenizer/buffered_char_stream.c \
    illiterate-tokenizer/char_stream.c \
    illiterate-tokenizer/token_stream.c)
OBJECTS = $(SOURCES:%.c=%.o)
DEPENDENCIES = $(SOURCES:%.c=%.d)

DEBUG-OBJECTS = $(SOURCES:%.c=%d.o)
COV-OBJECTS = $(SOURCES:%.c=%c.o)
SAN-OBJECTS = $(SOURCES:%.c=%s.o)
SAN-DEBUG-OBJECTS = $(SOURCES:%.c=%sd.o)

# Manual targets
# ==============

# Target relationships
# --------------------

default: lpld

all: lpl lpld lpls lplsd coverage.html doc

lpl: $(OBJECTS)
	$(CC) $(LDFLAGS) -o $@ $^

lpld: $(DEBUG-OBJECTS)
	$(CC) $(LDFLAGS) -o $@ $^

lplc: $(COV-OBJECTS)
	$(CC) $(LDFLAGS) $(COV-FLAGS) -o $@ $^

lpls: $(SAN-OBJECTS)
	$(CC) $(LDFLAGS) $(SAN-FLAGS) -o $@ $^

lplsd: $(SAN-DEBUG-OBJECTS)
	$(CC) $(LDFLAGS) $(SAN-FLAGS) -o $@ $^

# Specialized builds' details
# ---------------------------

%.o: %.c
	$(CC) $(CFLAGS) $(RELEASE-FLAGS) -c $< -o $@

%d.o: %.c
	$(CC) $(CFLAGS) $(DEBUG-FLAGS) -c $< -o $@

%c.o: %.c
	$(CC) $(CFLAGS) $(DEBUG-FLAGS) $(COV-FLAGS) -c $< -o $@

%s.o: %.c
	$(CC) $(CFLAGS) $(RELEASE-FLAGS) $(SAN-FLAGS) -c $< -o $@

%sd.o: %.c
	$(CC) $(CFLAGS) $(DEBUG-FLAGS) $(SAN-FLAGS) -c $< -o $@

# Coverage report
# ---------------

coverage.html: lplc
	./lplc test
	gcovr -r . --html --html-details -o coverage.html

# Phony targets
# =============

.PHONY: test test-leak test-san doc clean

test-leak: lpld
	valgrind --leak-check=full ./lpld test

test-san: lpls lplsd
	./lpls test
	./lplsd test

test: test-leak test-san

doc:
	$(MAKE) -C doc

clean:
	$(MAKE) -C doc clean
	rm -f *.o lpl lpld lplc lpls
	find . -name '*.o' -exec rm {} \;
	find . -name '*.d' -exec rm {} \;
	find . -name '*.gcno' -exec rm {} \;
	find . -name '*.gcda' -exec rm {} \;
	find . -name '*.html' -exec rm {} \;
	find . -name '*.css' -exec rm {} \;

-include $(DEPENDENCIES)
