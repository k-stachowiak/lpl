/*
 * Copyright (C) 2020 Krzysztof Stachowiak
 *
 * This file is part of the Language Programming Language compiler.
 *
 * Language Programming Language is free software: you can redistribute it
 * and/or modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Language Programming Language compiler is distributed in the hope that it
 * will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef JIT_AMD64_LINUX
#define JIT_AMD64_LINUX

#include "linux.h"
#include "test.h"
#include "data_types.h"

struct JITAMD64LinuxResult {
    char *error_message;
    struct LinuxExecutablePtr lep;
    struct DataTypeList result_types;
};

bool jalr_try_deinit(struct JITAMD64LinuxResult *jalr_deinit);

struct JITAMD64LinuxResult jit_amd64_linux_compile(
        char *source,
        struct DataTypeList *argument_types);

void jit_amd64_linux_test(struct TestContext *tc_parent);

#endif
