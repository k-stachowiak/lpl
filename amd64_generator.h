/*
 * Copyright (C) 2020 Krzysztof Stachowiak
 *
 * This file is part of the Language Programming Language compiler.
 *
 * Language Programming Language is free software: you can redistribute it
 * and/or modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Language Programming Language compiler is distributed in the hope that it
 * will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef AMD64_GENERATOR_H
#define AMD64_GENERATOR_H

#include "common.h"
#include "data_types.h"
#include "scanner.h"
#include "parser.h"
#include "binary.h"
#include "test.h"

#include "illiterate-amd64-encoder/amd64_encoder.h"

// Procedure generation result
// ===========================
//
// This structure represents the generation result:
// - error_message: if error occured, this is non-NULL and gives the reason
// - result_types: types of the returned variables deduced from the input types
// - bb: the generated buffer

struct PGRRipOffsetList {
    struct AMD64RipOffsetRecord record;
    struct PGRRipOffsetList *next;
};

struct PGRConstant {
    struct BinaryBuffer *bb;
    struct PGRRipOffsetList *rip_offsets;
    struct PGRConstant *next;
};

struct ProcedureGenerationResult {
    char *error_message;
    struct DataTypeList result_types;
    struct BinaryBuffer *bb;
    struct PGRConstant *constants;
};

void pgr_deinit(struct ProcedureGenerationResult *gr);

// Generator
// =========
//
// This module represents the generic code generation algorithm. It translates
// an input AST into a sequence of low level commands.

// Code generation algorithm
// -------------------------

char *amd64_mangle_name(char *base_name, struct DataTypeList *argument_list);

struct ProcedureGenerationResult amd64_generate_procedure_code(
        struct ProcedurePrototype *pp,
        struct DataTypeList *argument_types,
        struct SourceDebugInfo *sdi);

void amd64_generator_test(struct TestContext *tc);

#endif
