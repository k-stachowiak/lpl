/*
 * Copyright (C) 2020 Krzysztof Stachowiak
 *
 * This file is part of the Language Programming Language compiler.
 *
 * Language Programming Language is free software: you can redistribute it
 * and/or modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Language Programming Language compiler is distributed in the hope that it
 * will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "jit_amd64_linux.h"
#include "common.h"
#include "parser.h"
#include "scanner.h"
#include "amd64_generator.h"

#include <assert.h>

// Module implementation
// ---------------------

static void jalr_init(struct JITAMD64LinuxResult *jalr)
{
    jalr->error_message = NULL;
    dtl_init(&jalr->result_types);
}

bool jalr_try_deinit(struct JITAMD64LinuxResult *jalr)
{
    dtl_deinit(&jalr->result_types);
    if (jalr->error_message) {
        my_free(jalr->error_message);
        return true;
    } else {
        return lep_try_deinit(&jalr->lep);
    }
}

struct JITAMD64LinuxResult jit_amd64_linux_compile(
        char *source,
        struct DataTypeList *argument_types)
{
    struct JITAMD64LinuxResult jalr;
    struct TokCharStream cs;
    struct ParseResult pr;
    struct ProcedureScanResult psr;
    struct ProcedurePrototype *pp;
    struct ProcedureGenerationResult pgr;
    struct PGRConstant *pgr_constant;

    jalr_init(&jalr);

    // 1. Parse source:
    tok_cs_init_string(&cs, source);
    pr = parse(&cs, true);
    tok_cs_deinit(&cs);
    if (pr.error_message) {
        str_append(jalr.error_message,
                   "Failed parsing source: %s",
                   pr.error_message);
        parse_result_deinit(&pr);
        return jalr;
    }

    // 2. Scan AST:
    psr = scan_procedures(pr.result);
    if (psr.error_message) {
        str_append(jalr.error_message,
                   "Failed scanning source: %s",
                   psr.error_message);
        psr_deinit(&psr);
        parse_result_deinit(&pr);
        return jalr;
    }

    // 3. Process scan result:
    if (psr.ppt.size != 1) {
        str_append(jalr.error_message,
                   "Incorrect number of procedure prototypes");
        psr_deinit(&psr);
        parse_result_deinit(&pr);
        return jalr;
    }
    pp = psr.ppt.data[0];

    // 4. Generate code:

    // 4.1. Perform the generation:
    printf("Compiling %s\n", pp->name);
    pgr = amd64_generate_procedure_code(pp, argument_types, pr.sdi);
    if (pgr.error_message) {
        str_append(jalr.error_message,
                   "Failed generating procedure code: %s",
                   pgr.error_message);
        pgr_deinit(&pgr);
        psr_deinit(&psr);
        parse_result_deinit(&pr);
        return jalr;
    }

    // 4.2. Do some debug printing:
    printf("Source:\n%s\n\n", source);

    printf("Binary before RIP-relative constants hooking up:\n");
    bb_debug_print(pgr.bb, stdout);
    printf("\n\n");

    // 4.3. Hook up constants:
    pgr_constant = pgr.constants;
    while (pgr_constant) {

        // 4.3.1. Iterate over all the references to the current constant:
        struct PGRRipOffsetList *rip_offset = pgr_constant->rip_offsets;
        while (rip_offset) {

            // 4.3.1.1. Compute offset from the reference base to the end of
            //          the binary buffer, which is where we're putting the
            //          constant:
            int32_t offset = pgr.bb->size - rip_offset->record.base;
            assert(rip_offset->record.size == 4);

            // 4.3.1.2. Overwrite the dummy address we placed in the code:
            memcpy(pgr.bb->data + rip_offset->record.offset,
                   (uint8_t*)&offset,
                   rip_offset->record.size);

            // 4.3.1.3. Place the actual constant at the end of the buffer:
            bb_append_n(pgr.bb, pgr_constant->bb->data, pgr_constant->bb->size);

            // 4.3.1.4. Advance RIP reference:
            rip_offset = rip_offset->next;
        }

        // 4.3.4. Advance constant pointer:
        pgr_constant = pgr_constant->next;
    }

    // 4.4. Some more debug printing:
    printf("Binary after RIP-relative constants hooking up:\n");
    bb_debug_print(pgr.bb, stdout);
    printf("\n\n");

    // 4.5. Clean up what is no longer needed:
    psr_deinit(&psr);
    parse_result_deinit(&pr);

    // 5. Setup the executable buffer:

    // 5.1. Initialize the buffer for filling in:
    if (!lep_try_init(&jalr.lep, pgr.bb->size)) {
        str_append(jalr.error_message, "Failed initializing executable buffer");
        pgr_deinit(&pgr);
        return jalr;
    }

    // 5.2. Fill the result_data:
    memcpy(jalr.lep.ptr, pgr.bb->data, pgr.bb->size);
    dtl_copy(&jalr.result_types, &pgr.result_types);
    pgr_deinit(&pgr);

    // 5.3. Prepare the buffer for execution:
    if (!lep_try_lock(&jalr.lep)) {
        if (!lep_try_deinit(&jalr.lep)) {
            str_append(
                jalr.error_message,
                "Failed deinitializing executable buffer while: ");
        }
        str_append(jalr.error_message, "Failed locking buffer for execution");
        return jalr;
    }

    // 6. Return the result:
    return jalr;
}

// Tests
// -----

// ### Common procedure testing code

static bool test_jit(
        struct TestContext *tc,
        char *source,
        struct DataTypeList *argument_types,
        struct DataTypeList *expected_result_types,
        struct JITAMD64LinuxResult *jalr)
{
    // 1. Try generating binary:
    *jalr = jit_amd64_linux_compile(source, argument_types);
    if (jalr->error_message) {
        char *description = NULL;
        str_append(
            description,
            "Failed JIT-compiling source: %s",
            jalr->error_message);
        if (!jalr_try_deinit(jalr)) {
            str_append(
                description,
                " ...and also failed deinitializing the executable buffer");
        }
        tc_failure(tc, description);
        my_free(description);
        return false;
    }

    // 2. Assert result types validity:
    if (!dtl_equal(&jalr->result_types, expected_result_types)) {
        char *description = NULL;
        str_append(
            description,
            "Unexpected result types");
        if (!jalr_try_deinit(jalr)) {
            str_append(
                description,
                " ...and also failed deinitializing the executable buffer");
        }
        tc_failure(tc, description);
        my_free(description);
        return false;
    }

    return true;
}

static void test_void_to_unit(
        struct TestContext *tc_parent,
        char *case_name,
        char *source)
{
    struct TestContext *tc;
    struct DataTypeList argument_types;
    struct DataTypeList expected_result_types;
    struct JITAMD64LinuxResult jalr;
    void (*func_ptr)(void);

    // 1. Initialize the test context:
    tc = tc_create_child(case_name, tc_parent);

    // 2. Initialize input and output data types lists:
    dtl_init(&argument_types);
    dtl_init(&expected_result_types);
    array_append(expected_result_types, dt_create_primitive(DT_PRIM_UNIT));

    // 3. Compile the test source:
    if (!test_jit(
            tc,
            source,
            &argument_types,
            &expected_result_types,
            &jalr)) {
        dtl_deinit(&argument_types);
        dtl_deinit(&expected_result_types);
        return;
    }

    // 4. Execute the test code:
    func_ptr = jalr.lep.ptr;
    func_ptr();

    // 5. Cleanup:
    dtl_deinit(&argument_types);
    dtl_deinit(&expected_result_types);
    tc_generic(
        tc,
        "Release JIT-compiled buffer",
        jalr_try_deinit(&jalr));
}

static void test_byte_to_byte(
        struct TestContext *tc_parent,
        char *case_name,
        char *source,
        int8_t arg1,
        int8_t expected_result)
{
    struct TestContext *tc;
    struct DataTypeList argument_types;
    struct DataTypeList expected_result_types;
    struct JITAMD64LinuxResult jalr;
    int8_t (*func_ptr)(int8_t);
    int8_t actual_result;

    // 1. Initialize the test context:
    tc = tc_create_child(case_name, tc_parent);

    // 2. Initialize input and output data types lists:
    dtl_init(&argument_types);
    array_append(argument_types, dt_create_primitive(DT_PRIM_BYTE));
    dtl_init(&expected_result_types);
    array_append(expected_result_types, dt_create_primitive(DT_PRIM_BYTE));

    // 3. Compile the test source:
    if (!test_jit(
            tc,
            source,
            &argument_types,
            &expected_result_types,
            &jalr)) {
        dtl_deinit(&argument_types);
        dtl_deinit(&expected_result_types);
        return;
    }

    // 4. Execute the test code:
    func_ptr = jalr.lep.ptr;
    actual_result = func_ptr(arg1);
    tc_assert_equal_int8(
        tc,
        "Run JIT-compiled procedure",
        expected_result,
        actual_result);

    // 5. Cleanup:
    dtl_deinit(&argument_types);
    dtl_deinit(&expected_result_types);
    tc_generic(
        tc,
        "Release JIT-compiled buffer",
        jalr_try_deinit(&jalr));
}

static void test_2byte_to_byte(
        struct TestContext *tc_parent,
        char *case_name,
        char *source,
        int8_t arg1,
        int8_t arg2,
        int8_t expected_result)
{
    struct TestContext *tc;
    struct DataTypeList argument_types;
    struct DataTypeList expected_result_types;
    struct JITAMD64LinuxResult jalr;
    int8_t (*func_ptr)(int8_t, int8_t);
    int8_t actual_result;

    // 1. Initialize the test context:
    tc = tc_create_child(case_name, tc_parent);

    // 2. Initialize input and output data types lists:
    dtl_init(&argument_types);
    array_append(argument_types, dt_create_primitive(DT_PRIM_BYTE));
    array_append(argument_types, dt_create_primitive(DT_PRIM_BYTE));
    dtl_init(&expected_result_types);
    array_append(expected_result_types, dt_create_primitive(DT_PRIM_BYTE));

    // 3. Compile the test source:
    if (!test_jit(
            tc,
            source,
            &argument_types,
            &expected_result_types,
            &jalr)) {
        dtl_deinit(&argument_types);
        dtl_deinit(&expected_result_types);
        return;
    }

    // 4. Execute the test code:
    func_ptr = jalr.lep.ptr;
    actual_result = func_ptr(arg1, arg2);
    tc_assert_equal_int8(
        tc,
        "Run JIT-compiled procedure",
        expected_result,
        actual_result);

    // 5. Cleanup:
    dtl_deinit(&argument_types);
    dtl_deinit(&expected_result_types);
    tc_generic(
        tc,
        "Release JIT-compiled buffer",
        jalr_try_deinit(&jalr));
}

static void test_5byte_to_byte(
        struct TestContext *tc_parent,
        char *case_name,
        char *source,
        int8_t arg1,
        int8_t arg2,
        int8_t arg3,
        int8_t arg4,
        int8_t arg5,
        int8_t expected_result)
{
    struct TestContext *tc;
    struct DataTypeList argument_types;
    struct DataTypeList expected_result_types;
    struct JITAMD64LinuxResult jalr;
    int8_t (*func_ptr)(int8_t, int8_t, int8_t, int8_t, int8_t);
    int8_t actual_result;

    // 1. Initialize the test context:
    tc = tc_create_child(case_name, tc_parent);

    // 2. Initialize input and output data types lists:
    dtl_init(&argument_types);
    array_append(argument_types, dt_create_primitive(DT_PRIM_BYTE));
    array_append(argument_types, dt_create_primitive(DT_PRIM_BYTE));
    array_append(argument_types, dt_create_primitive(DT_PRIM_BYTE));
    array_append(argument_types, dt_create_primitive(DT_PRIM_BYTE));
    array_append(argument_types, dt_create_primitive(DT_PRIM_BYTE));
    dtl_init(&expected_result_types);
    array_append(expected_result_types, dt_create_primitive(DT_PRIM_BYTE));

    // 3. Compile the test source:
    if (!test_jit(
            tc,
            source,
            &argument_types,
            &expected_result_types,
            &jalr)) {
        dtl_deinit(&argument_types);
        dtl_deinit(&expected_result_types);
        return;
    }

    // 4. Execute the test code:
    func_ptr = jalr.lep.ptr;
    actual_result = func_ptr(arg1, arg2, arg3, arg4, arg5);
    tc_assert_equal_int8(
        tc,
        "Run JIT-compiled procedure",
        expected_result,
        actual_result);

    // 5. Cleanup:
    dtl_deinit(&argument_types);
    dtl_deinit(&expected_result_types);
    tc_generic(
        tc,
        "Release JIT-compiled buffer",
        jalr_try_deinit(&jalr));
}

static void test_pbyte_to_word(
        struct TestContext *tc_parent,
        char *case_name,
        char *source,
        int8_t arg1,
        int64_t expected_result)
{
    struct TestContext *tc;
    struct DataTypeList argument_types;
    struct DataTypeList expected_result_types;
    struct JITAMD64LinuxResult jalr;
    int64_t (*func_ptr)(int8_t*);
    int64_t actual_result;

    // 1. Initialize the test context:
    tc = tc_create_child(case_name, tc_parent);

    // 2. Initialize input and output data types lists:
    dtl_init(&argument_types);
    array_append(argument_types,
                 dt_create_pointer(dt_create_primitive(DT_PRIM_BYTE)));

    dtl_init(&expected_result_types);
    array_append(expected_result_types,
                 dt_create_primitive(DT_PRIM_WORD));

    // 3. Compile the test source:
    if (!test_jit(
            tc,
            source,
            &argument_types,
            &expected_result_types,
            &jalr)) {
        dtl_deinit(&argument_types);
        dtl_deinit(&expected_result_types);
        return;
    }

    // 4. Execute the test code:
    func_ptr = jalr.lep.ptr;
    actual_result = func_ptr(&arg1);
    tc_assert_equal_int64(
        tc,
        "Run JIT-compiled procedure",
        expected_result,
        actual_result);

    dtl_deinit(&argument_types);
    dtl_deinit(&expected_result_types);
    tc_generic(
        tc,
        "Release JIT-compiled buffer",
        jalr_try_deinit(&jalr));
}

static void test_pbyte_pbyte_to_unit(
        struct TestContext *tc_parent,
        char *case_name,
        char *source,
        int8_t arg1,
        int8_t arg2,
        int8_t arg1_post,
        int8_t arg2_post)
{
    struct TestContext *tc;
    struct DataTypeList argument_types;
    struct DataTypeList expected_result_types;
    struct JITAMD64LinuxResult jalr;
    void (*func_ptr)(int8_t*, int8_t*);

    // 1. Initialize the test context:
    tc = tc_create_child(case_name, tc_parent);

    // 2. Initialize input and output data types lists:
    dtl_init(&argument_types);
    array_append(argument_types,
                 dt_create_pointer(dt_create_primitive(DT_PRIM_BYTE)));
    array_append(argument_types,
                 dt_create_pointer(dt_create_primitive(DT_PRIM_BYTE)));

    dtl_init(&expected_result_types);
    array_append(expected_result_types,
                 dt_create_primitive(DT_PRIM_UNIT));

    // 3. Compile the test source:
    if (!test_jit(
            tc,
            source,
            &argument_types,
            &expected_result_types,
            &jalr)) {
        dtl_deinit(&argument_types);
        dtl_deinit(&expected_result_types);
        return;
    }

    // 4. Execute the test code:
    func_ptr = jalr.lep.ptr;
    func_ptr(&arg1, &arg2);
    tc_assert_equal_int8(tc, "Assert 1st 8-bit pointer arg", arg1, arg1_post);
    tc_assert_equal_int8(tc, "Assert 2nd 8-bit pointer arg", arg2, arg2_post);

    dtl_deinit(&argument_types);
    dtl_deinit(&expected_result_types);
    tc_generic(
        tc,
        "Release JIT-compiled buffer",
        jalr_try_deinit(&jalr));
}

static void test_word_to_word(
        struct TestContext *tc_parent,
        char *case_name,
        char *source,
        int64_t arg1,
        int64_t expected_result)
{
    struct TestContext *tc;
    struct DataTypeList argument_types;
    struct DataTypeList expected_result_types;
    struct JITAMD64LinuxResult jalr;
    int64_t (*func_ptr)(int64_t);
    int64_t actual_result;

    // 1. Initialize the test context:
    tc = tc_create_child(case_name, tc_parent);

    // 2. Initialize input and output data types lists:
    dtl_init(&argument_types);
    array_append(argument_types, dt_create_primitive(DT_PRIM_WORD));
    dtl_init(&expected_result_types);
    array_append(expected_result_types, dt_create_primitive(DT_PRIM_WORD));

    // 3. Compile the test source:
    if (!test_jit(
            tc,
            source,
            &argument_types,
            &expected_result_types,
            &jalr)) {
        dtl_deinit(&argument_types);
        dtl_deinit(&expected_result_types);
        return;
    }

    // 4. Execute the test code:
    func_ptr = jalr.lep.ptr;
    actual_result = func_ptr(arg1);
    tc_assert_equal_int64(
        tc,
        "Run JIT-compiled procedure",
        expected_result,
        actual_result);

    // 5. Cleanup:
    dtl_deinit(&argument_types);
    dtl_deinit(&expected_result_types);
    tc_generic(
        tc,
        "Release JIT-compiled buffer",
        jalr_try_deinit(&jalr));
}

static void test_2word_to_word(
        struct TestContext *tc_parent,
        char *case_name,
        char *source,
        int64_t arg1,
        int64_t arg2,
        int64_t expected_result)
{
    struct TestContext *tc;
    struct DataTypeList argument_types;
    struct DataTypeList expected_result_types;
    struct JITAMD64LinuxResult jalr;
    int64_t (*func_ptr)(int64_t, int64_t);
    int64_t actual_result;

    // 1. Initialize the test context:
    tc = tc_create_child(case_name, tc_parent);

    // 2. Initialize input and output data types lists:
    dtl_init(&argument_types);
    array_append(argument_types, dt_create_primitive(DT_PRIM_WORD));
    array_append(argument_types, dt_create_primitive(DT_PRIM_WORD));
    dtl_init(&expected_result_types);
    array_append(expected_result_types, dt_create_primitive(DT_PRIM_WORD));

    // 3. Compile the test source:
    if (!test_jit(
            tc,
            source,
            &argument_types,
            &expected_result_types,
            &jalr)) {
        dtl_deinit(&argument_types);
        dtl_deinit(&expected_result_types);
        return;
    }

    // 4. Execute the test code:
    func_ptr = jalr.lep.ptr;
    actual_result = func_ptr(arg1, arg2);
    tc_assert_equal_int64(
        tc,
        "Run JIT-compiled procedure",
        expected_result,
        actual_result);

    // 5. Cleanup:
    dtl_deinit(&argument_types);
    dtl_deinit(&expected_result_types);
    tc_generic(
        tc,
        "Release JIT-compiled buffer",
        jalr_try_deinit(&jalr));
}

static void test_5word_to_word(
        struct TestContext *tc_parent,
        char *case_name,
        char *source,
        int64_t arg1,
        int64_t arg2,
        int64_t arg3,
        int64_t arg4,
        int64_t arg5,
        int64_t expected_result)
{
    struct TestContext *tc;
    struct DataTypeList argument_types;
    struct DataTypeList expected_result_types;
    struct JITAMD64LinuxResult jalr;
    int64_t (*func_ptr)(int64_t, int64_t, int64_t, int64_t, int64_t);
    int64_t actual_result;

    // 1. Initialize the test context:
    tc = tc_create_child(case_name, tc_parent);

    // 2. Initialize input and output data types lists:
    dtl_init(&argument_types);
    array_append(argument_types, dt_create_primitive(DT_PRIM_WORD));
    array_append(argument_types, dt_create_primitive(DT_PRIM_WORD));
    array_append(argument_types, dt_create_primitive(DT_PRIM_WORD));
    array_append(argument_types, dt_create_primitive(DT_PRIM_WORD));
    array_append(argument_types, dt_create_primitive(DT_PRIM_WORD));
    dtl_init(&expected_result_types);
    array_append(expected_result_types, dt_create_primitive(DT_PRIM_WORD));

    // 3. Compile the test source:
    if (!test_jit(
            tc,
            source,
            &argument_types,
            &expected_result_types,
            &jalr)) {
        dtl_deinit(&argument_types);
        dtl_deinit(&expected_result_types);
        return;
    }

    // 4. Execute the test code:
    func_ptr = jalr.lep.ptr;
    actual_result = func_ptr(arg1, arg2, arg3, arg4, arg5);
    tc_assert_equal_int64(
        tc,
        "Run JIT-compiled procedure",
        expected_result,
        actual_result);

    // 5. Cleanup:
    dtl_deinit(&argument_types);
    dtl_deinit(&expected_result_types);
    tc_generic(
        tc,
        "Release JIT-compiled buffer",
        jalr_try_deinit(&jalr));
}

static void test_pword_to_word(
        struct TestContext *tc_parent,
        char *case_name,
        char *source,
        int64_t arg1,
        int64_t expected_result)
{
    struct TestContext *tc;
    struct DataTypeList argument_types;
    struct DataTypeList expected_result_types;
    struct JITAMD64LinuxResult jalr;
    int64_t (*func_ptr)(int64_t*);
    int64_t actual_result;

    // 1. Initialize the test context:
    tc = tc_create_child(case_name, tc_parent);

    // 2. Initialize input and output data types lists:
    dtl_init(&argument_types);
    array_append(argument_types,
                 dt_create_pointer(dt_create_primitive(DT_PRIM_WORD)));

    dtl_init(&expected_result_types);
    array_append(expected_result_types,
                 dt_create_primitive(DT_PRIM_WORD));

    // 3. Compile the test source:
    if (!test_jit(
            tc,
            source,
            &argument_types,
            &expected_result_types,
            &jalr)) {
        dtl_deinit(&argument_types);
        dtl_deinit(&expected_result_types);
        return;
    }

    // 4. Execute the test code:
    func_ptr = jalr.lep.ptr;
    actual_result = func_ptr(&arg1);
    tc_assert_equal_int64(
        tc,
        "Run JIT-compiled procedure",
        expected_result,
        actual_result);

    dtl_deinit(&argument_types);
    dtl_deinit(&expected_result_types);
    tc_generic(
        tc,
        "Release JIT-compiled buffer",
        jalr_try_deinit(&jalr));
}

static void test_pword_pword_to_unit(
        struct TestContext *tc_parent,
        char *case_name,
        char *source,
        int64_t arg1,
        int64_t arg2,
        int64_t arg1_post,
        int64_t arg2_post)
{
    struct TestContext *tc;
    struct DataTypeList argument_types;
    struct DataTypeList expected_result_types;
    struct JITAMD64LinuxResult jalr;
    void (*func_ptr)(int64_t*, int64_t*);

    // 1. Initialize the test context:
    tc = tc_create_child(case_name, tc_parent);

    // 2. Initialize input and output data types lists:
    dtl_init(&argument_types);
    array_append(argument_types,
                 dt_create_pointer(dt_create_primitive(DT_PRIM_WORD)));
    array_append(argument_types,
                 dt_create_pointer(dt_create_primitive(DT_PRIM_WORD)));

    dtl_init(&expected_result_types);
    array_append(expected_result_types,
                 dt_create_primitive(DT_PRIM_UNIT));

    // 3. Compile the test source:
    if (!test_jit(
            tc,
            source,
            &argument_types,
            &expected_result_types,
            &jalr)) {
        dtl_deinit(&argument_types);
        dtl_deinit(&expected_result_types);
        return;
    }

    // 4. Execute the test code:
    func_ptr = jalr.lep.ptr;
    func_ptr(&arg1, &arg2);
    tc_assert_equal_int64(tc, "Assert 1st 64-bit pointer arg", arg1, arg1_post);
    tc_assert_equal_int64(tc, "Assert 2nd 64-bit pointer arg", arg2, arg2_post);

    dtl_deinit(&argument_types);
    dtl_deinit(&expected_result_types);
    tc_generic(
        tc,
        "Release JIT-compiled buffer",
        jalr_try_deinit(&jalr));
}

static void test_double_to_double(
        struct TestContext *tc_parent,
        char *case_name,
        char *source,
        double arg1,
        double expected_result,
        double epsilon)
{
    struct TestContext *tc;
    struct DataTypeList argument_types;
    struct DataTypeList expected_result_types;
    struct JITAMD64LinuxResult jalr;
    double (*func_ptr)(double);
    double actual_result;

    // 1. Initialize the test context:
    tc = tc_create_child(case_name, tc_parent);

    // 2. Initialize input and output data types lists:
    dtl_init(&argument_types);
    array_append(argument_types, dt_create_primitive(DT_PRIM_DOUBLE));
    dtl_init(&expected_result_types);
    array_append(expected_result_types, dt_create_primitive(DT_PRIM_DOUBLE));

    // 3. Compile the test source:
    if (!test_jit(
            tc,
            source,
            &argument_types,
            &expected_result_types,
            &jalr)) {
        dtl_deinit(&argument_types);
        dtl_deinit(&expected_result_types);
        return;
    }

    // 4. Execute the test code:
    func_ptr = jalr.lep.ptr;
    actual_result = func_ptr(arg1);
    tc_assert_near_double(
        tc,
        "Run JIT-compiled procedure",
        expected_result,
        actual_result,
        epsilon);

    // 5. Cleanup:
    dtl_deinit(&argument_types);
    dtl_deinit(&expected_result_types);
    tc_generic(
        tc,
        "Release JIT-compiled buffer",
        jalr_try_deinit(&jalr));
}

static void test_5double_to_double(
        struct TestContext *tc_parent,
        char *case_name,
        char *source,
        double arg1,
        double arg2,
        double arg3,
        double arg4,
        double arg5,
        double expected_result,
        double epsilon)
{
    struct TestContext *tc;
    struct DataTypeList argument_types;
    struct DataTypeList expected_result_types;
    struct JITAMD64LinuxResult jalr;
    double (*func_ptr)(double, double, double, double, double);
    double actual_result;

    // 1. Initialize the test context:
    tc = tc_create_child(case_name, tc_parent);

    // 2. Initialize input and output data types lists:
    dtl_init(&argument_types);
    array_append(argument_types, dt_create_primitive(DT_PRIM_DOUBLE));
    array_append(argument_types, dt_create_primitive(DT_PRIM_DOUBLE));
    array_append(argument_types, dt_create_primitive(DT_PRIM_DOUBLE));
    array_append(argument_types, dt_create_primitive(DT_PRIM_DOUBLE));
    array_append(argument_types, dt_create_primitive(DT_PRIM_DOUBLE));
    dtl_init(&expected_result_types);
    array_append(expected_result_types, dt_create_primitive(DT_PRIM_DOUBLE));

    // 3. Compile the test source:
    if (!test_jit(
            tc,
            source,
            &argument_types,
            &expected_result_types,
            &jalr)) {
        dtl_deinit(&argument_types);
        dtl_deinit(&expected_result_types);
        return;
    }

    // 4. Execute the test code:
    func_ptr = jalr.lep.ptr;
    actual_result = func_ptr(arg1, arg2, arg3, arg4, arg5);
    tc_assert_near_double(
        tc,
        "Run JIT-compiled procedure",
        expected_result,
        actual_result,
        epsilon);

    // 5. Cleanup:
    dtl_deinit(&argument_types);
    dtl_deinit(&expected_result_types);
    tc_generic(
        tc,
        "Release JIT-compiled buffer",
        jalr_try_deinit(&jalr));
}

static void test_pdouble_to_word(
        struct TestContext *tc_parent,
        char *case_name,
        char *source,
        double arg1,
        int64_t expected_result)
{
    struct TestContext *tc;
    struct DataTypeList argument_types;
    struct DataTypeList expected_result_types;
    struct JITAMD64LinuxResult jalr;
    int64_t (*func_ptr)(double*);
    int64_t actual_result;

    // 1. Initialize the test context:
    tc = tc_create_child(case_name, tc_parent);

    // 2. Initialize input and output data types lists:
    dtl_init(&argument_types);
    array_append(argument_types,
                 dt_create_pointer(dt_create_primitive(DT_PRIM_DOUBLE)));

    dtl_init(&expected_result_types);
    array_append(expected_result_types,
                 dt_create_primitive(DT_PRIM_WORD));

    // 3. Compile the test source:
    if (!test_jit(
            tc,
            source,
            &argument_types,
            &expected_result_types,
            &jalr)) {
        dtl_deinit(&argument_types);
        dtl_deinit(&expected_result_types);
        return;
    }

    // 4. Execute the test code:
    func_ptr = jalr.lep.ptr;
    actual_result = func_ptr(&arg1);
    tc_assert_equal_int64(
        tc,
        "Run JIT-compiled procedure",
        expected_result,
        actual_result);

    dtl_deinit(&argument_types);
    dtl_deinit(&expected_result_types);
    tc_generic(
        tc,
        "Release JIT-compiled buffer",
        jalr_try_deinit(&jalr));
}

static void test_pdouble_pdouble_to_unit(
        struct TestContext *tc_parent,
        char *case_name,
        char *source,
        double arg1,
        double arg2,
        double arg1_post,
        double arg2_post)
{
    struct TestContext *tc;
    struct DataTypeList argument_types;
    struct DataTypeList expected_result_types;
    struct JITAMD64LinuxResult jalr;
    void (*func_ptr)(double*, double*);

    // 1. Initialize the test context:
    tc = tc_create_child(case_name, tc_parent);

    // 2. Initialize input and output data types lists:
    dtl_init(&argument_types);
    array_append(argument_types,
                 dt_create_pointer(dt_create_primitive(DT_PRIM_DOUBLE)));
    array_append(argument_types,
                 dt_create_pointer(dt_create_primitive(DT_PRIM_DOUBLE)));

    dtl_init(&expected_result_types);
    array_append(expected_result_types,
                 dt_create_primitive(DT_PRIM_UNIT));

    // 3. Compile the test source:
    if (!test_jit(
            tc,
            source,
            &argument_types,
            &expected_result_types,
            &jalr)) {
        dtl_deinit(&argument_types);
        dtl_deinit(&expected_result_types);
        return;
    }

    // 4. Execute the test code:
    func_ptr = jalr.lep.ptr;
    func_ptr(&arg1, &arg2);
    tc_assert_equal_double(tc, "Assert 1st double pointer arg", arg1, arg1_post);
    tc_assert_equal_double(tc, "Assert 2nd double pointer arg", arg2, arg2_post);

    dtl_deinit(&argument_types);
    dtl_deinit(&expected_result_types);
    tc_generic(
        tc,
        "Release JIT-compiled buffer",
        jalr_try_deinit(&jalr));
}

static void test_string_to_word(
        struct TestContext *tc_parent,
        char *case_name,
        char *source,
        char *arg1,
        int64_t expected_result)
{
    struct TestContext *tc;
    struct DataTypeList argument_types;
    struct DataTypeList expected_result_types;
    struct JITAMD64LinuxResult jalr;
    int64_t (*func_ptr)(void *);
    int64_t actual_result;
    uint64_t input_buffer[2]; // 8 bytes for size and 8 for data pointer

    // 1. Initialize the test context:
    tc = tc_create_child(case_name, tc_parent);

    // 2. Initialize input and output data types lists:
    dtl_init(&argument_types);
    array_append(argument_types,
                 dt_create_pointer(
                     dt_create_buffer(
                         dt_create_primitive(DT_PRIM_WORD))));
    dtl_init(&expected_result_types);
    array_append(expected_result_types, dt_create_primitive(DT_PRIM_WORD));

    // 3. Build the input buffer:
    input_buffer[0] = strlen(arg1);
    input_buffer[1] = (uint64_t)arg1;

    // 4. Compile the test source:
    if (!test_jit(
            tc,
            source,
            &argument_types,
            &expected_result_types,
            &jalr)) {
        dtl_deinit(&argument_types);
        dtl_deinit(&expected_result_types);
        return;
    }

    // 5. Execute the test code:
    func_ptr = jalr.lep.ptr;
    actual_result = func_ptr((void*)input_buffer);
    tc_assert_equal_int64(
        tc,
        "Run JIT-compiled procedure",
        expected_result,
        actual_result);

    // 6. Cleanup:
    dtl_deinit(&argument_types);
    dtl_deinit(&expected_result_types);
    tc_generic(
        tc,
        "Release JIT-compiled buffer",
        jalr_try_deinit(&jalr));
}

// ### Integration tests

static void test_inc(struct TestContext *tc_parent)
{
    struct TestContext *tc;
    char *source;

    tc = tc_create_child("INC", tc_parent);
    source =
        "proc inc(x) -> y #\n"
        "    y := x + 1\n"
        "corp";

    // Byte sized cases:
    test_byte_to_byte(tc, "Normal case (byte)", source, 41, 42);
    test_byte_to_byte(tc, "Negative argument (byte)", source, -5, -4);
    test_byte_to_byte(tc, "Edge case (byte)", source, INT8_MAX - 1, INT8_MAX);
    test_byte_to_byte(tc, "Edge case wrap (byte)", source, INT8_MAX, INT8_MIN);

    // Word sized cases:
    test_word_to_word(tc, "Normal case (word)", source, 41, 42);
    test_word_to_word(tc, "Negative argument (word)", source, -5, -4);
    test_word_to_word(tc, "Edge case (word)", source, INT64_MAX - 1, INT64_MAX);
    test_word_to_word(tc, "Edge case wrap(word)", source, INT64_MAX, INT64_MIN);

    // Floating point arguments:
    test_double_to_double(tc, "Normal case (double)", source, 41.0, 42.0, 0.0);
    test_double_to_double(tc, "Negative argument (double)", source, -5.0, -4.0, 0.0);
}

static void test_dec(struct TestContext *tc_parent)
{
    struct TestContext *tc;
    char *source;

    tc = tc_create_child("DEC", tc_parent);
    source =
        "proc dec(x) -> y #\n"
        "    y := x - 1\n"
        "corp";

    // Byte sized cases:
    test_byte_to_byte(tc, "Normal case (byte)", source, 42, 41);
    test_byte_to_byte(tc, "Negative argument (byte)", source, -5, -6);
    test_byte_to_byte(tc, "Edge case (byte)", source, INT8_MAX, INT8_MAX - 1);
    test_byte_to_byte(tc, "Edge case wrap (byte)", source, INT8_MIN, INT8_MAX);

    // Word sized cases:
    test_word_to_word(tc, "Normal case (word)", source, 42, 41);
    test_word_to_word(tc, "Negative argument (word)", source, -5, -6);
    test_word_to_word(tc, "Edge case (word)", source, INT64_MAX, INT64_MAX - 1);
    test_word_to_word(tc, "Edge case wrap(word)", source, INT64_MIN, INT64_MAX);

    // Floating point arguments:
    test_double_to_double(tc, "Normal case (double)", source, 42.0, 41.0, 0.0);
    test_double_to_double(tc, "Negative argument (double)", source, -5.0, -6.0, 0.0);
}

static void test_arithmetic(struct TestContext *tc_parent)
{
    struct TestContext *tc;
    char *source;

    tc = tc_create_child("ARITHMETIC", tc_parent);
    source =
        "proc arithmetic(a, b, c, d, e) -> x #\n"
        "    x := ((a + b) * c - d) / e\n"
        "corp";

    // Byte sized cases:
    test_5byte_to_byte(
        tc,
        "Arbitrary arguments (byte)",
        source,
        1, 2, 3, 4, 5,
        1);

    // Word sized cases:
    test_5word_to_word(
        tc,
        "Arbitrary arguments (word)",
        source,
        1234, 5678, 1234, 5678, 91011,
        93); // Expected: ~93.656

    // Double cases:
    test_5double_to_double(
        tc,
        "Arbitrary arguments (word)",
        source,
        1234.0, 5678.0, 1234.0, 5678.0, 91011.0,
        93.656,
        0.001); // Expected: ~93.656
}

static void test_sign(struct TestContext *tc_parent)
{
    struct TestContext *tc;
    char *source;

    tc = tc_create_child("SIGN", tc_parent);
    source =
        "proc sign(x) -> y #\n"
        "    y := 0;\n" // TODO: Turn this assignment into a constraint, or nothing
        "    if\n"
        "        ^x < 0 => y := -1 #\n"
        "        ^x = 0 => y := 0 #\n"
        "        ^x > 0 => y := 1\n"
        "    fi\n"
        "corp";

    // Flaoting point arguments:
    test_pdouble_to_word(tc, "Negative argument (double)", source, -100.0, -1);
    test_pdouble_to_word(tc, "Zero argument (double)", source, 0.0, 0);
    test_pdouble_to_word(tc, "Positive argument (double)", source, 100.0, 1);


    // Byte sized cases:
    test_pbyte_to_word(tc, "Negative argument (byte)", source, -100, -1);
    test_pbyte_to_word(tc, "Zero argument (byte)", source, 0, 0);
    test_pbyte_to_word(tc, "Positive argument (byte)", source, 100, 1);

    // Word sized cases:
    test_pword_to_word(tc, "Negative argument (word)", source, -1234567890, -1);
    test_pword_to_word(tc, "Zero argument (word)", source, 0, 0);
    test_pword_to_word(tc, "Positive argument (word)", source, 2143658709, 1);
}

static void test_gcd(struct TestContext *tc_parent)
{
    struct TestContext *tc;
    char *source;

    tc = tc_create_child("GCD", tc_parent);
    source =
        "proc gcd(x, y) -> x #\n"
        "    do\n"
        "        y != 0 => \n"
        "            temp := y;\n"
        "            y := x % y;\n"
        "            x := temp\n"
        "    od\n"
        "corp";

    // Byte sized cases:
    test_2byte_to_byte(tc, "gcd(12, 8) = 4 (byte)", source, 12, 8, 4);
    test_2byte_to_byte(tc, "gcd(42, 56) = 14 (byte)", source, 42, 56, 14);

    // Word sized cases:
    test_2word_to_word(tc, "gcd(35123, 67089) = 11 (word)", source, 35123, 67089, 11);
    test_2word_to_word(tc, "gcd(1234567890, 2143658709) = 9 (word)", source, 1234567890, 2143658709, 9);
}

static void test_swap(struct TestContext *tc_parent)
{
    struct TestContext *tc;
    char *source;

    tc = tc_create_child("SWAP", tc_parent);
    source =
        "proc swap(x, y) -> unit #\n"
        "    temp := ^x;\n"
        "    ^x := ^y;\n"
        "    ^y := temp\n"
        "corp";

    test_pdouble_pdouble_to_unit(tc,
                                 "swap(&1.23, &3.21) (double)",
                                 source,
                                 1.23, 3.21,
                                 3.21, 1.23);
    test_pbyte_pbyte_to_unit(tc,
                             "swap(&3, &4) (byte)",
                             source,
                             3, 4,
                             4, 3);
    test_pword_pword_to_unit(tc,
                             "swap(&1234567890, &2143658709) (word)",
                             source,
                             1234567890, 2143658709,
                             2143658709, 1234567890);
}

static void test_buffer_length(struct TestContext *tc_parent)
{
    struct TestContext *tc;
    char *source;

    tc = tc_create_child("BUFFER-LENGTH", tc_parent);
    source =
        "proc test_length(buffer) -> l #\n"
        "    l := length(buffer)\n"
        "corp";

    test_string_to_word(tc,
                        "test_length(\"Hello, world!\\n\")",
                        source,
                        "Hello, world!\n",
                        14);
}

static void test_buffer_print(struct TestContext *tc_parent)
{
    struct TestContext *tc;
    char *source;

    tc = tc_create_child("BUFFER-PRINT", tc_parent);
    source =
        "proc test_print(buffer) -> l #\n"
        "    l := print(buffer)\n"
        "corp";

    test_string_to_word(tc,
                        "test_print(\"Hello, beautiful wife!\\n\")",
                        source,
                        "Hello, beautiful wife!\n",
                        23);
}

static void test_string_print(struct TestContext *tc_parent)
{
    struct TestContext *tc;
    char *source;

    tc = tc_create_child("STRING-PRINT", tc_parent);
    source =
        "proc test_print(Void) -> unit #\n"
        "    print(\"Hello, World!\\n\")\n"
        "corp";

    test_void_to_unit(tc,
                      "test_print(\"Hello, world!\\n\")",
                      source);
}

// ### Tests root

void jit_amd64_linux_test(struct TestContext *tc_parent)
{
    struct TestContext *tc = tc_create_child("JIT-AMD64-LINUX", tc_parent);
    test_inc(tc);
    test_dec(tc);
    test_arithmetic(tc);
    test_sign(tc);
    test_gcd(tc);
    test_swap(tc);
    test_buffer_length(tc);
    test_buffer_print(tc);
    test_string_print(tc);
}
