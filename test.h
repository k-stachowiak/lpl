/*
 * Copyright (C) 2020 Krzysztof Stachowiak
 *
 * This file is part of the Language Programming Language compiler.
 *
 * Language Programming Language is free software: you can redistribute it
 * and/or modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Language Programming Language compiler is distributed in the hope that it
 * will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef TEST_H
#define TEST_H

#include <stdbool.h>
#include <stdint.h>
#include <stdio.h>

struct TestEntry {
    char *description;
    bool result;
};

struct TestContext {
    char *name;
    struct {
        struct TestContext **data;
        int cap, size;
    } children;
    struct {
        struct TestEntry *data;
        int cap, size;
    } entries;
};

struct TestContext *tc_create_root(char *name);
struct TestContext *tc_create_child(char *name, struct TestContext *parent);
void tc_destroy_root(struct TestContext *tc);

void tc_generic(
        struct TestContext *tc,
        char *description,
        bool result);

void tc_assert_equal_int8(
        struct TestContext *tc,
        char *description,
        int8_t expected,
        int8_t actual);

void tc_assert_equal_int64(
        struct TestContext *tc,
        char *description,
        int64_t expected,
        int64_t actual);

void tc_assert_equal_double(
        struct TestContext *tc,
        char *description,
        double expected,
        double actual);

void tc_assert_near_double(
        struct TestContext *tc,
        char *description,
        double expected,
        double actual,
        double epsilon);

void tc_assert_equal_ptr(
        struct TestContext *tc,
        char *description,
        void *expected,
        void *actual);

void tc_failure(struct TestContext *tc, char *description);

void tc_report(struct TestContext *tc, FILE *output, bool verbose);

#endif
