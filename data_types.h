/*
 * Copyright (C) 2020 Krzysztof Stachowiak
 *
 * This file is part of the Language Programming Language compiler.
 *
 * Language Programming Language is free software: you can redistribute it
 * and/or modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Language Programming Language compiler is distributed in the hope that it
 * will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef DATA_TYPES_H
#define DATA_TYPES_H

#include <stdbool.h>

// Data types
// ==========
// This module models the run time data types abstraction over what the machine
// offers.

// Data structures
// ---------------

enum DataTypePrimitiveType {
    DT_PRIM_UNIT,
    DT_PRIM_BOOLEAN,
    DT_PRIM_BYTE,
    DT_PRIM_WORD,
    DT_PRIM_DOUBLE
};

struct DataTypePointer {
    struct DataType *subtype;
};

struct DataTypeBuffer {
    struct DataType *subtype;
};

enum DataTypeType {
    DT_PRIMITIVE,
    DT_POINTER,
    DT_BUFFER,
    DT_CSTRING,
};

struct DataType {
    enum DataTypeType type;
    union {
        enum DataTypePrimitiveType primitive;
        struct DataTypePointer pointer;
        struct DataTypeBuffer buffer;
    };
};

struct DataType *dt_create_primitive(enum DataTypePrimitiveType primitive);
struct DataType *dt_create_pointer(struct DataType *subtype);
struct DataType *dt_create_buffer(struct DataType *subtype);
struct DataType *dt_create_cstring(void);

struct DataType *dt_copy(struct DataType *dt);
void dt_swap(struct DataType **x, struct DataType **y);

void dt_destroy(struct DataType *dt);

bool dt_equal(struct DataType *x, struct DataType *y);

bool dt_is_buffer_ptr(struct DataType *dt);
char *dt_name(struct DataType *dt);
int dt_effective_size(struct DataType *dt);

// Data type list
// ==============
// Represents a sequence of data types, e.g. procedure arguments.

struct DataTypeList { struct DataType **data; int size, cap; };

void dtl_init(struct DataTypeList *dtl);
void dtl_deinit(struct DataTypeList *dtl);
void dtl_copy(struct DataTypeList *dst, struct DataTypeList *src);
bool dtl_equal(struct DataTypeList *x, struct DataTypeList *y);

#endif
