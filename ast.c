﻿/*
 * Copyright (C) 2020 Krzysztof Stachowiak
 *
 * This file is part of the Language Programming Language compiler.
 *
 * Language Programming Language is free software: you can redistribute it
 * and/or modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Language Programming Language compiler is distributed in the hope that it
 * will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "ast.h"
#include "common.h"

#include <inttypes.h>

struct AstNode *ast_create_literal_void(void)
{
    struct AstNode *result = my_malloc(sizeof(*result));
    result->type = AST_LITERAL;
    result->literal.type = AST_LIT_VOID;
    return result;
}

struct AstNode *ast_create_literal_unit(void)
{
    struct AstNode *result = my_malloc(sizeof(*result));
    result->type = AST_LITERAL;
    result->literal.type = AST_LIT_UNIT;
    return result;
}

struct AstNode *ast_create_literal_bool(char value)
{
    struct AstNode *result = my_malloc(sizeof(*result));
    result->type = AST_LITERAL;
    result->literal.type = AST_LIT_BOOLEAN;
    result->literal.boolean = value;
    return result;
}

struct AstNode *ast_create_literal_integer(uint64_t value)
{
    struct AstNode *result = my_malloc(sizeof(*result));
    result->type = AST_LITERAL;
    result->literal.type = AST_LIT_INTEGER;
    result->literal.integer = value;
    return result;
}

struct AstNode *ast_create_literal_real(double value)
{
    struct AstNode *result = my_malloc(sizeof(*result));
    result->type = AST_LITERAL;
    result->literal.type = AST_LIT_REAL;
    result->literal.real = value;
    return result;
}

struct AstNode *ast_create_literal_character(char value)
{
    struct AstNode *result = my_malloc(sizeof(*result));
    result->type = AST_LITERAL;
    result->literal.type = AST_LIT_CHARACTER;
    result->literal.character = value;
    return result;
}

struct AstNode *ast_create_literal_string(char *value)
{
    struct AstNode *result = my_malloc(sizeof(*result));
    result->type = AST_LITERAL;
    result->literal.type = AST_LIT_STRING;
    result->literal.string = str_copy(value);
    return result;
}

struct AstNode *ast_create_symbol(char *value)
{
    struct AstNode *result = my_malloc(sizeof(*result));
    result->type = AST_SYMBOL;
    result->symbol.string = str_copy(value);
    return result;
}

struct AstNode *ast_create_unary(
        enum AstUnaryType type,
        struct AstNode *operand)
{
    struct AstNode *result = my_malloc(sizeof(*result));
    result->type = AST_UNARY;
    result->unary.type = type;
    result->unary.operand = operand;
    return result;
}

struct AstNode *ast_create_binary(
        enum AstBinaryType type,
        struct AstNode *left,
        struct AstNode *right)
{
    struct AstNode *result = my_malloc(sizeof(*result));
    result->type = AST_BINARY;
    result->binary.type = type;
    result->binary.left = left;
    result->binary.right = right;
    return result;
}

struct AstNode *ast_create_block(
        enum AstBlockType type,
        struct AstNode *body)
{
    struct AstNode *result = my_malloc(sizeof(*result));
    result->type = AST_BLOCK;
    result->block.type = type;
    result->block.body = body;
    return result;
}

bool ast_is_literal(struct AstNode *node)
{
    return node->type == AST_LITERAL;
}

bool ast_is_literal_spec(struct AstNode *node, enum AstLiteralType type)
{
    return
        node->type == AST_LITERAL &&
        node->literal.type == type;
}

bool ast_is_symbol(struct AstNode *node)
{
    return node->type == AST_SYMBOL;
}

bool ast_is_symbol_spec(struct AstNode *node, char *value)
{
    if (!ast_is_symbol(node)) {
        return false;
    }
    if (strcmp(node->symbol.string, value) != 0) {
        return false;
    }
    return true;
}

struct AstNode *ast_copy(struct AstNode *node)
{
    struct AstNode *result;

    if (!node) {
        return NULL;
    }

    result = my_malloc(sizeof(*result));
    result->type = node->type;
    switch (node->type) {
    case AST_LITERAL:
        result->literal.type = node->literal.type;
        switch (node->literal.type) {
        case AST_LIT_VOID:
        case AST_LIT_UNIT:
            break;
        case AST_LIT_BOOLEAN:
            result->literal.boolean = node->literal.boolean;
            break;
        case AST_LIT_INTEGER:
            result->literal.integer = node->literal.integer;
            break;
        case AST_LIT_REAL:
            result->literal.real = node->literal.real;
            break;
        case AST_LIT_CHARACTER:
            result->literal.character = node->literal.character;
            break;
        case AST_LIT_STRING:
            result->literal.string = str_copy(node->literal.string);
            break;
        }
        break;
    case AST_SYMBOL:
        result->symbol.string = str_copy(node->symbol.string);
        break;
    case AST_UNARY:
        result->unary.type = node->unary.type;
        result->unary.operand = ast_copy(node->unary.operand);
        break;
    case AST_BINARY:
        result->binary.type = node->binary.type;
        result->binary.left = ast_copy(node->binary.left);
        result->binary.right = ast_copy(node->binary.right);
        break;
    case AST_BLOCK:
        result->block.type = node->block.type;
        result->block.body = ast_copy(node->block.body);
        break;
    }
    return result;
}

void ast_destroy(struct AstNode *node)
{
    if (!node) {
        return;
    }
    switch (node->type) {
    case AST_LITERAL:
        if (node->literal.type == AST_LIT_STRING) {
            my_free(node->literal.string);
        }
        break;
    case AST_SYMBOL:
        my_free(node->symbol.string);
        break;
    case AST_UNARY:
        ast_destroy(node->unary.operand);
        break;
    case AST_BINARY:
        ast_destroy(node->binary.left);
        ast_destroy(node->binary.right);
        break;
    case AST_BLOCK:
        ast_destroy(node->block.body);
        break;
    }
    my_free(node);
}

static void ast_visit_rec(
        int depth,
        struct AstNode *node,
        void (*callback)(void *data, int depth, struct AstNode *x),
        void *data)
{
    callback(data, depth, node);
    if (!node) {
        return;
    }
    switch (node->type) {
    case AST_LITERAL:
    case AST_SYMBOL:
        break;
    case AST_UNARY:
        ast_visit_rec(depth + 1, node->unary.operand, callback, data);
        break;
    case AST_BINARY:
        ast_visit_rec(depth + 1, node->binary.left, callback, data);
        ast_visit_rec(depth + 1, node->binary.right, callback, data);
        break;
    case AST_BLOCK:
        ast_visit_rec(depth + 1, node->block.body, callback, data);
        break;
    }
}

void ast_visit(
        struct AstNode *node,
        void (*callback)(void *data, int depth, struct AstNode *node),
        void *data)
{
    ast_visit_rec(0, node, callback, data);
}

static char *ast_node_to_string(struct AstNode *node)
{
    char *result = NULL;
    switch (node->type) {
    case AST_LITERAL:
        switch (node->literal.type) {
        case AST_LIT_VOID:
            str_append(result, "void");
            break;
        case AST_LIT_UNIT:
            str_append(result, "unit");
            break;
        case AST_LIT_BOOLEAN:
            str_append(result, "%s", node->literal.boolean ? "true" : "false");
            break;
        case AST_LIT_INTEGER:
            str_append(result, "%" PRId64, node->literal.integer);
            break;
        case AST_LIT_REAL:
            str_append(result, "%f", node->literal.real);
            break;
        case AST_LIT_CHARACTER:
            str_append(result, "%c", node->literal.character);
            break;
        case AST_LIT_STRING:
            str_append(result, "%s", node->literal.string);
            break;
        }
        break;
    case AST_SYMBOL:
        str_append(result, "%s", node->symbol.string);
        break;
    case AST_BLOCK:
        switch (node->block.type) {
        case AST_BLOCK_IFFI:
            str_append(result, "if-fi");
            break;
        case AST_BLOCK_DOOD:
            str_append(result, "do-od");
            break;
        case AST_BLOCK_PROCCORP:
            str_append(result, "proc-corp");
            break;
        }
        break;
    case AST_UNARY:
        switch (node->unary.type) {
        case AST_UN_POSITIVE:
            str_append(result, "un+");
            break;
        case AST_UN_NEGATIVE:
            str_append(result, "un-");
            break;
        case AST_UN_DEREFERENCE:
            str_append(result, "^");
            break;
        case AST_UN_ADDRESS:
            str_append(result, "@");
            break;
        }
        break;
    case AST_BINARY:
        switch (node->binary.type) {
        case AST_BIN_ADD:
            str_append(result, "+");
            break;
        case AST_BIN_SUBTRACT:
            str_append(result, "-");
            break;
        case AST_BIN_MULTIPLY:
            str_append(result, "*");
            break;
        case AST_BIN_DIVIDE:
            str_append(result, "/");
            break;
        case AST_BIN_MODULO:
            str_append(result, "%%");
            break;
        case AST_BIN_POWER:
            str_append(result, "^");
            break;
        case AST_BIN_ASSIGN:
            str_append(result, ":=");
            break;
        case AST_BIN_IMPLY:
            str_append(result, "=>");
            break;
        case AST_BIN_SEQUENCE:
            str_append(result, ";");
            break;
        case AST_BIN_BLOCK_SEQUENCE:
            str_append(result, "#");
            break;
        case AST_BIN_LIST_SEQUENCE:
            str_append(result, ",");
            break;
        case AST_BIN_EQUAL:
            str_append(result, "=");
            break;
        case AST_BIN_NOT_EQUAL:
            str_append(result, "!=");
            break;
        case AST_BIN_LESS_THAN:
            str_append(result, "<");
            break;
        case AST_BIN_GREATER_THAN:
            str_append(result, ">");
            break;
        case AST_BIN_LESS_THAN_OR_EQUAL:
            str_append(result, "<=");
            break;
        case AST_BIN_GREATER_THAN_OR_EQUAL:
            str_append(result, ">=");
            break;
        case AST_BIN_CONCATENATE:
            str_append(result, "~");
            break;
        case AST_BIN_PROC_CALL:
            str_append(result, "proc-call");
            break;
        case AST_BIN_SUBSCRIPT:
            str_append(result, "subscript");
            break;
        case AST_BIN_PROC_HEAD:
            str_append(result, "->");
            break;
        }
        break;
    }
    return result;
}

static void ast_to_string_tree_rec(int depth, struct AstNode *node, char **str)
{
    char *node_string;

    str_append(*str, "%*c", depth * 4, ' ');

    if (!node) {
        str_append(*str, "NULL\n");
        return;
    }

    node_string = ast_node_to_string(node);
    str_append(*str, "%s\n", node_string);
    my_free(node_string);

    if (node->type == AST_BLOCK) {
        ast_to_string_tree_rec(depth + 1, node->block.body, str);
    } else if (node->type == AST_UNARY) {
        ast_to_string_tree_rec(depth + 1, node->unary.operand, str);
    } else if (node->type == AST_BINARY) {
        ast_to_string_tree_rec(depth + 1, node->binary.left, str);
        ast_to_string_tree_rec(depth + 1, node->binary.right, str);
    }
}

char *ast_to_string_tree(struct AstNode *node)
{
    char *str = NULL;
    ast_to_string_tree_rec(0, node, &str);
    return str;
}

static void ast_to_string_algebraic_rec(struct AstNode *node, char **str)
{
    char *string = NULL;
    switch (node->type) {
    case AST_LITERAL:
    case AST_SYMBOL:
    case AST_UNARY:
        string = ast_node_to_string(node);
        str_append(*str, "%s", string);
        my_free(string);
        break;
    case AST_BINARY:
        if (node->binary.type == AST_BIN_SUBSCRIPT) {
            ast_to_string_algebraic_rec(node->binary.left, str);
            str_append(*str, "[");
            ast_to_string_algebraic_rec(node->binary.right, str);
            str_append(*str, "]");
            my_free(string);
        } else if (node->binary.type == AST_BIN_PROC_CALL) {
            ast_to_string_algebraic_rec(node->binary.left, str);
            str_append(*str, "(");
            ast_to_string_algebraic_rec(node->binary.right, str);
            str_append(*str, ")");
            my_free(string);
        } else {
            ast_to_string_algebraic_rec(node->binary.left, str);
            string = ast_node_to_string(node);
            str_append(*str, "%s", string);
            my_free(string);
            ast_to_string_algebraic_rec(node->binary.right, str);
        }
        break;
    case AST_BLOCK:
        switch (node->block.type) {
        case AST_BLOCK_IFFI:
            str_append(*str, "if ");
            ast_to_string_algebraic_rec(node->block.body, str);
            str_append(*str, " fi");
            break;
        case AST_BLOCK_DOOD:
            str_append(*str, "do ");
            ast_to_string_algebraic_rec(node->block.body, str);
            str_append(*str, " od");
            break;
        case AST_BLOCK_PROCCORP:
            str_append(*str, "proc ");
            ast_to_string_algebraic_rec(node->block.body, str);
            str_append(*str, " corp");
            break;
        }
        break;
    }
}

char *ast_to_string_algebraic(struct AstNode *node)
{
    char *str = NULL;
    ast_to_string_algebraic_rec(node, &str);
    return str;
}

static int ast_to_dot_rec(struct AstNode *node, char **str, int *node_id)
{
    char *node_string;
    int my_id = ++*node_id;
    node_string = ast_node_to_string(node);
    str_append(*str, "\t%d [ label = \"%s\" ];\n", my_id, node_string);
    my_free(node_string);
    if (node->type == AST_BLOCK) {
        int child_id = ast_to_dot_rec(node->block.body, str, node_id);
        str_append(*str, "\t%d -> %d;\n", my_id, child_id);
    } else if (node->type == AST_UNARY) {
        int child_id = ast_to_dot_rec(node->unary.operand, str, node_id);
        str_append(*str, "\t%d -> %d;\n", my_id, child_id);
    } else if (node->type == AST_BINARY) {
        int child_id;
        child_id = ast_to_dot_rec(node->binary.left, str, node_id);
        str_append(*str, "\t%d -> %d;\n", my_id, child_id);
        child_id = ast_to_dot_rec(node->binary.right, str, node_id);
        str_append(*str, "\t%d -> %d;\n", my_id, child_id);
    }
    return my_id;
}

char *ast_to_string_dot(struct AstNode *node)
{
    char *str = NULL;
    int node_id = 0;
    str_append(str, "digraph Program {\n");
    ast_to_dot_rec(node, &str, &node_id);
    str_append(str, "}\n");
    return str;
}

void ast_for_each_left(
        struct AstNode *node,
        enum AstBinaryType op,
        void (*callback)(void*, struct AstNode*),
        void *data)
{
    if (node && node->type == AST_BINARY && node->binary.type == op) {
        ast_for_each_left(node->binary.left, op, callback, data);
        ast_for_each_left(node->binary.right, op, callback, data);
    } else {
        callback(data, node);
    }
}

// Tests
// -----

static void ast_test_example_tree(struct TestContext *tc_parent)
{
    struct TestContext *tc = tc_create_child("EXAMPLE-TREE", tc_parent);

    struct AstNode
        *literal,
        *unary,
        *symbol,
        *sequence,
        *block,
        *copy;

    // 1. Create a tree rooted in a single node:

    // 1.1. A literal child:
    literal = ast_create_literal_integer(1);
    tc_generic(tc,
               "AST type introspection: int is int",
               ast_is_literal_spec(literal, AST_LIT_INTEGER));
    tc_generic(tc,
               "AST type introspection: int isn't bool ",
               !ast_is_literal_spec(literal, AST_LIT_BOOLEAN));

    // 1.2. An unary operator taking the literal node:
    unary = ast_create_unary(AST_UN_DEREFERENCE, literal);

    // 1.3. Independent symbol node:
    symbol = ast_create_symbol("test");
    tc_generic(tc,
               "AST type introspection: symbol is symbol",
               ast_is_symbol(symbol));
    tc_generic(tc,
               "AST type introspection: check for specific symbol",
               ast_is_symbol_spec(symbol, "test"));
    tc_generic(tc,
               "AST type introspection: check for another specific symbol",
               !ast_is_symbol_spec(symbol, "doot"));

    // 1.4. Bind the sequence and the unary with a binary operator:
    sequence = ast_create_binary(AST_BIN_SEQUENCE, symbol, unary);

    // 1.5. Enclose the binary operator withing a block node:
    block = ast_create_block(AST_BLOCK_DOOD, sequence);

    // 2. Go crazy, copy the entire tree:
    copy = ast_copy(block);

    // 2. Try deallocating everything:
    ast_destroy(block);
    ast_destroy(copy);
}

void ast_test(struct TestContext *tc_parent)
{
    struct TestContext *tc = tc_create_child("AST", tc_parent);
    ast_test_example_tree(tc);
}
