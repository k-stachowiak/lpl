/*
 * Copyright (C) 2020 Krzysztof Stachowiak
 *
 * This file is part of the Language Programming Language compiler.
 *
 * Language Programming Language is free software: you can redistribute it
 * and/or modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Language Programming Language compiler is distributed in the hope that it
 * will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef SCANNER_H
#define SCANNER_H

#include "ast.h"
#include "test.h"

struct ProcedurePrototype {
    char *name;
    struct { struct AstNode **data; int size, cap; } arguments;
    struct { struct AstNode **data; int size, cap; } results;
    struct AstNode *body;
};

struct ProcedurePrototype *pp_create(void);
void pp_destroy(struct ProcedurePrototype *pp);
char *pp_argument_name(struct ProcedurePrototype *pp, int index);
char *pp_result_name(struct ProcedurePrototype *pp, int index);

struct ProcedurePrototypeTable {
    struct ProcedurePrototype **data;
    int size;
    int cap;
};

void ppt_init(struct ProcedurePrototypeTable *ppt);
void ppt_deinit(struct ProcedurePrototypeTable *ppt);
struct ProcedurePrototype *ppt_find(
        struct ProcedurePrototypeTable *ppt,
        char *name);

struct ProcedureScanResult {
    char *error_message;
    struct ProcedurePrototypeTable ppt;
};

void psr_deinit(struct ProcedureScanResult *psr);

struct ProcedureScanResult scan_procedures(struct AstNode *ast);

char *psr_to_string(struct ProcedureScanResult *psr);

void scanner_test(struct TestContext *tc);

#endif
