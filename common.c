/*
 * Copyright (C) 2020 Krzysztof Stachowiak
 *
 * This file is part of the Language Programming Language compiler.
 *
 * Language Programming Language is free software: you can redistribute it
 * and/or modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Language Programming Language compiler is distributed in the hope that it
 * will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "common.h"

// Helper type
struct CharArray { char *data; int size, cap; };

void* my_malloc(size_t size)
{
    void *result = malloc(size);
    if (!result) {
        fprintf(stderr, "Memory allocation error\n");
        exit(1);
    } else {
        return result;
    }
}

void *my_realloc(void *buffer, size_t size)
{
    void *result = realloc(buffer, size);
    if (!result) {
        fprintf(stderr, "Memory reallocation error\n");
        exit(1);
    } else {
        return result;
    }
}

void my_free(void *x)
{
    free(x);
}

uint64_t hash_fast(char *buffer, size_t size)
{
    uint64_t result = 7;
    while (size--) result = result * 31 + *buffer++;
    return result;
}

char *str_copy(char *x)
{
    int len;
    char *result;
    if (!x) {
        return NULL;
    }
    len = strlen(x);
    result = my_malloc(len + 1);
    memcpy(result, x, len + 1);
    return result;
}

char *str_copy_range(char *first, char *last)
{
    int size = last - first;
    char *result = my_malloc(size + 1);
    memcpy(result, first, size);
    result[size] = '\0';
    return result;
}

char *str_escape(char *x)
{
    char *result = NULL;
    while (*x) {
        if (*x == '"') {
            str_append(result, "\\\"");
        } else if (*x == '\\') {
            str_append(result, "\\\\");
        } else {
            str_append(result, "%c", *x);
        }
        ++x;
    }
    return result;
}

char *str_copy_line(char *string, int line)
{
    int current_line = 1;
    char *first, *last;

    /* 1. Find the appropriate line: */
    while (*string && current_line != line) {
        if (*string == '\n') {
            ++current_line;
        }
        ++string;
    }

    /* 2. Account for not enough lines: */
    if (current_line != line) {
        return NULL;
    }

    /* 3. Determine the line range: */
    first = string;
    while (*string && *string != '\n') {
        ++string;
    }
    last = string;

    /* 4. Copy the line we've found: */
    return str_copy_range(first, last);
}

int str_count_range(char *string, size_t offset, char character) {
    int result = 0;
    size_t current_offset;
    for (current_offset = 0;
         *string && (current_offset != offset);
         ++current_offset, ++string) {
        if (*string == character) {
            ++result;
        }
    }
    if (current_offset != offset) {
        return -1;
    } else {
        return result;
    }
}

void ptr_stack_init(struct PtrStack *ptr_stack)
{
    ptr_stack->size = 0;
    ptr_stack->cap = 0;
    ptr_stack->data = NULL;
}

void ptr_stack_deinit(struct PtrStack *ptr_stack, void(*destructor)(void*))
{
    if (destructor) {
        int i;
        void **ptr = ptr_stack->data;
        for (i = 0; i < ptr_stack->size; ++i) {
            destructor(*ptr++);
        }
    }
    array_free(*ptr_stack);
}

void ptr_stack_push(struct PtrStack *ptr_stack, void *x)
{
    array_append(*ptr_stack, x);
}

void *ptr_stack_top(struct PtrStack *ptr_stack)
{
    return ptr_stack->data[ptr_stack->size - 1];
}

void ptr_stack_pop(struct PtrStack *ptr_stack)
{
    array_pop(*ptr_stack);
}

static void trie_node_deinit(struct TrieNode *node)
{
    int i;
    for (i = 0; i < node->children.size; ++i) {
        trie_node_deinit(node->children.data + i);
    }
    my_free(node->children.data);
}

void trie_init(struct Trie *trie)
{
    memset(&trie->root, 0, sizeof(trie->root));
}

static void trie_deinit_callback(char *name, struct TrieNode *node, void *data)
{
    void (*destructor)(void*) = data;
    (void)name;
    destructor(node->data);
}

void trie_deinit(struct Trie *trie, void (*destructor)(void*))
{
    if (destructor) {
        trie_for_each(trie, trie_deinit_callback, destructor);
    }
    trie_node_deinit(&trie->root);
}

void trie_insert(struct Trie *trie, char *key, void *data)
{
    struct TrieNode *node = &trie->root;
    char *current = key;
    while (*current) {
        int i;
        bool found = false;
        for (i = 0; i < node->children.size; ++i) {
            if (node->children.data[i].key == *current) {
                node = node->children.data + i;
                found = true;
                break;
            }
        }
        if (!found) {
            struct TrieNode new_node = {
                0,
                { NULL, 0, 0 },
                false,
                0
            };
            new_node.key = *current;
            array_append(node->children, new_node);
            node = node->children.data + i;
        }
        ++current;
    }
    if (node->is_set) {
        fprintf(stderr, "Warning! Duplicate insertion into trie\n");
    } else {
        node->is_set = true;
        node->data = data;
    }
}

struct TrieNode *trie_find(struct Trie *trie, char* key)
{
    struct TrieNode *node = &trie->root;
    while (*key) {
        int i;
        bool found = false;
        for (i = 0; i < node->children.size; ++i) {
            if (node->children.data[i].key == *key) {
                node = node->children.data + i;
                ++key;
                found = true;
                break;
            }
        }
        if (!found) {
            return NULL;
        }
    }
    if (node->is_set) {
        return node;
    } else {
        return NULL;
    }
}

static void trie_for_each_rec(
        struct TrieNode *node,
        void (*callback)(char*, struct TrieNode*, void*),
        void *data,
        struct CharArray *current_symbol)
{
    /* 0. Overview:
     * This function traverses the symbol map trie recursively
     * calling the callback function for every occurence of a value attached
     * in a given point in the trie.
     */

    int i, count;
    struct TrieNode *child;

    /* 1. Check for the callback condition */
    if (node->is_set) {
        callback(current_symbol->data, node, data);
    }

    /* 2. Traverse the deeper nodes recursively */
    count = node->children.size;
    child = node->children.data;
    for (i = 0; i < count; ++i) {

        /* 2.1. Append the current character */
        array_pop(*current_symbol);
        array_append(*current_symbol, child->key);
        array_append(*current_symbol, '\0');

        /* 2.2. Make the recursive call */
        trie_for_each_rec(child, callback, data, current_symbol);

        /* 2.3. Remove the last appended character */
        array_pop(*current_symbol);
        array_pop(*current_symbol);
        array_append(*current_symbol, '\0');

        /* 2.4. Advance the current child pointer */
        ++child;
    }
}

void trie_for_each(
        struct Trie *trie,
        void (*callback)(char*, struct TrieNode*, void*),
        void *data)
{
    struct CharArray current_symbol = { NULL, 0, 0 };
    array_append(current_symbol, '\0');
    trie_for_each_rec(&trie->root, callback, data, &current_symbol);
    array_free(current_symbol);
}

static struct HashTableNode *ht_lookup(struct HashTable *ht, uint64_t hash)
{
    int i;
    for (i = 0; i != ht->nodes.size; ++i) {
        if (ht->nodes.data[i].hash == hash) {
            return ht->nodes.data + i;
        }
    }
    return NULL;
}

void ht_init(struct HashTable *ht, uint64_t (*function)(void*))
{
    ht->nodes.data = NULL;
    ht->nodes.size = ht->nodes.cap = 0;
    ht->function = function;
}

void ht_deinit(struct HashTable *ht, void (*destructor)(void*))
{
    int i;
    if (destructor) {
        for (i = 0; i != ht->nodes.size; ++i) {
            destructor(ht->nodes.data[i].value);
        }
    }
    array_free(ht->nodes);
}

bool ht_put(struct HashTable *ht, void *key, void *value)
{
    uint64_t hash = ht->function(key);
    struct HashTableNode *preexisting = ht_lookup(ht, hash);
    struct HashTableNode node;
    if (preexisting) {
        return false;
    }
    node.hash = hash;
    node.key = key;
    node.value = value;
    array_append(ht->nodes, node);
    return true;
}

void *ht_get(struct HashTable *ht, void *key)
{
    uint64_t hash = ht->function(key);
    struct HashTableNode *preexisting = ht_lookup(ht, hash);
    if (preexisting) {
        return preexisting->value;
    } else {
        return NULL;
    }
}

void ht_for_each(
        struct HashTable *ht,
        void (*callback)(struct HashTableNode*, void*),
        void *data)
{
    int i;
    for (i = 0; i != ht->nodes.size; ++i) {
        callback(ht->nodes.data + i, data);
    }
}

bool all_of(char *first, char *last, int (*predicate)(int))
{
    while (first != last) {
        if (!predicate(*first++)) {
            return false;
        }
    }
    return true;
}

char *find(char *first, char *last, int value)
{
    while (first != last && *first != value) {
        ++first;
    }
    return first;
}

char *find_if_not(char *first, char *last, int (*predicate)(int))
{
    while (first != last && predicate(*first)) {
        ++first;
    }
    return first;
}

// Modified algorithm from StackOverflow:
// http://stackoverflow.com/questions/314401/how-to-read-a-line-from-the-console-in-c
char *my_getline(FILE *file, bool *eof)
{
    char *line = my_malloc(100);
    char *linep = line;
    size_t lenmax = 100;
    size_t len = lenmax;
    int c;

    *eof = false;

    for (;;) {
        c = fgetc(file);
        if (c == EOF) {
            *eof = true;
            break;
        }

        if (--len == 0) {
            char *linen;
            len = lenmax;
            linen = my_realloc(linep, lenmax *= 2);
            line = linen + (line - linep);
            linep = linen;
        }

        if ((*line++ = c) == '\n') {
            break;
        }
    }

    *line = '\0';
    return linep;
}

// Modified algorithm from StackOverflow:
// http://stackoverflow.com/questions/174531/easiest-way-to-get-files-contents-in-c
char *my_getfile(char *filename)
{
    char *buffer = 0;
    long length;
    FILE *file = fopen(filename, "rb");

    if (!file) {
        return NULL;
    }

    fseek(file, 0, SEEK_END);
    length = ftell(file);
    fseek(file, 0, SEEK_SET);
    buffer = my_malloc(length + 1);

    if (fread(buffer, 1, length, file) == (size_t)length) {
        buffer[length] = '\0';
    } else {
        my_free(buffer);
        buffer = NULL;
    }

    fclose(file);

    return buffer;
}

// Test code
// ---------

static void array_test(struct TestContext *tc_parent)
{
    struct TestContext *tc = tc_create_child("ARRAY", tc_parent);
    struct { int *data, size, cap; }
    arr1 = { NULL, 0, 0 }, arr2 = { NULL, 0, 0 };
    // 1. Fill a test array:
    array_append(arr1, 1);
    array_append(arr1, 2);
    array_append(arr1, 3);
    array_append(arr1, 4);
    array_append(arr1, 5);
    tc_generic(tc, "Filled array size incorrect", arr1.size == 5);
    tc_generic(tc, "Filled array value 0 incorrect", arr1.data[0] == 1);
    tc_generic(tc, "Filled array value 1 incorrect", arr1.data[1] == 2);
    tc_generic(tc, "Filled array value 2 incorrect", arr1.data[2] == 3);
    tc_generic(tc, "Filled array value 3 incorrect", arr1.data[3] == 4);
    tc_generic(tc, "Filled array value 4 incorrect", arr1.data[4] == 5);
    // 2. Make a copy:
    array_copy(arr2, arr1);
    tc_generic(tc, "Copied array size incorrect", arr2.size == arr1.size);
    tc_generic(tc, "Copied array value 0 incorrect", arr2.data[0] == 1);
    tc_generic(tc, "Copied array value 1 incorrect", arr2.data[1] == 2);
    tc_generic(tc, "Copied array value 2 incorrect", arr2.data[2] == 3);
    tc_generic(tc, "Copied array value 3 incorrect", arr2.data[3] == 4);
    tc_generic(tc, "Copied array value 4 incorrect", arr2.data[4] == 5);
    // 3. Remove some elements from the original array:
    //                        Before: [ 1, 2, 3, 4, 5 ]
    array_remove(arr1, 1); // Now:    [ 1, 5, 3, 4 ] (this stuffs the last element
                           //                         into the removed position)
    array_pop(arr1);       // Now:    [ 1, 5, 3 ]
    array_remove(arr1, 2); // Now:    [ 1, 5 ]
    tc_generic(tc, "Decimated array size incorrect", arr1.size == 2);
    tc_generic(tc, "Decimated array value 0 incorrect", arr1.data[0] == 1);
    tc_generic(tc, "Decimated array value 1 incorrect", arr1.data[1] == 5);
    // 4. Verify, that the copy remains untouched:
    tc_generic(tc, "Copied array size incorrect", arr2.size == 5);
    tc_generic(tc, "Copied array value 0 incorrect", arr2.data[0] == 1);
    tc_generic(tc, "Copied array value 1 incorrect", arr2.data[1] == 2);
    tc_generic(tc, "Copied array value 2 incorrect", arr2.data[2] == 3);
    tc_generic(tc, "Copied array value 3 incorrect", arr2.data[3] == 4);
    tc_generic(tc, "Copied array value 4 incorrect", arr2.data[4] == 5);
    // 5. Cleanup:
    array_free(arr1);
    array_free(arr2);
}

static void string_test(struct TestContext *tc_parent)
{
    struct TestContext *tc = tc_create_child("STRING", tc_parent);
    char *rostring = "test";
    char *copy;
    char *built = NULL;
    char *expected_built = "eneduerikefaketest";
    copy = str_copy(rostring);
    tc_generic(tc,
               "Copied string not equal",
               strcmp(rostring, copy) == 0);
    str_append(built, "ene");
    str_append(built, "due");
    str_append(built, "rike");
    str_append(built, "%s", "fake");
    str_append_range(built, rostring, rostring + strlen(rostring));
    tc_generic(tc,
               "Built string not equal",
               strcmp(built, expected_built) == 0);
    my_free(copy);
    my_free(built);
}

static void pointer_stack_test(struct TestContext *tc_parent)
{
    int x = 1, y = 2, z = 3;
    struct PtrStack ps;
    struct TestContext *tc = tc_create_child("PTR-STACK", tc_parent);
    ptr_stack_init(&ps);
    ptr_stack_push(&ps, &x);
    tc_assert_equal_ptr(tc, "Pointer stack state correct 1", &x, ptr_stack_top(&ps));
    ptr_stack_push(&ps, &y);
    tc_assert_equal_ptr(tc, "Pointer stack state correct 2", &y, ptr_stack_top(&ps));
    ptr_stack_push(&ps, &z);
    tc_assert_equal_ptr(tc, "Pointer stack state correct 3", &z, ptr_stack_top(&ps));
    ptr_stack_pop(&ps);
    tc_assert_equal_ptr(tc, "Pointer stack state correct 4", &y, ptr_stack_top(&ps));
    ptr_stack_pop(&ps);
    tc_assert_equal_ptr(tc, "Pointer stack state correct 5", &x, ptr_stack_top(&ps));
    ptr_stack_deinit(&ps, NULL);
}

static void int_destructor(void *ptr)
{
    int *int_ptr = (int*)ptr;
    my_free(int_ptr);
}

static int global_counter;

static char *common_keys[] = { "alfa", "alfa-romeo", "beta", NULL };

static void trie_test_callback(char *key, struct TrieNode *node, void *data)
{
    struct TestContext *tc = (struct TestContext*)data;
    bool good = false;
    (void)node;
    ++global_counter;
    char **expected = common_keys;
    while (*expected) {
        if (strcmp(*expected, key) == 0) {
            good = true;
            break;
        }
        ++expected;
    }
    tc_generic(tc, "Trie for-each: key found", good);
}

static void trie_test(struct TestContext *tc_parent)
{
    struct TestContext *tc = tc_create_child("TRIE", tc_parent);
    struct Trie trie;
    int *a = my_malloc(sizeof(*a));
    int *b = my_malloc(sizeof(*b));
    int *c = my_malloc(sizeof(*c));
    trie_init(&trie);
    trie_insert(&trie, common_keys[0], a);
    trie_insert(&trie, common_keys[1], b);
    trie_insert(&trie, common_keys[2], c);
    tc_generic(
        tc,
        "Trie lookup: expected element not found",
        (int*)trie_find(&trie, common_keys[0])->data == a);
    tc_generic(
        tc,
        "Trie lookup: expected element not found",
        (int*)trie_find(&trie, common_keys[1])->data == b);
    tc_generic(
        tc,
        "Trie lookup: expected element not found",
        (int*)trie_find(&trie, common_keys[2])->data == c);
    tc_generic(
        tc,
        "Trie lookup: expected element not found",
        trie_find(&trie, "non-existent") == NULL);
    global_counter = 0;
    trie_for_each(&trie, trie_test_callback, tc);
    tc_generic(
        tc,
        "Check trie callback findings count",
        global_counter == 3);
    trie_deinit(&trie, int_destructor);
}

static uint64_t hash_fast_string_v(void *ptr)
{
    char *string = (char*)ptr;
    return hash_fast(ptr, strlen(string));
}

static void hash_table_test_callback(struct HashTableNode *node, void *data)
{
    (void)node;
    (void)data;
    ++global_counter;
}

static void hash_table_test(struct TestContext *tc_parent)
{
    struct TestContext *tc = tc_create_child("HASH-TABLE", tc_parent);
    struct HashTable ht;
    int *a = my_malloc(sizeof(*a));
    int *b = my_malloc(sizeof(*b));
    int *c = my_malloc(sizeof(*c));
    ht_init(&ht, hash_fast_string_v);
    // 1. Fill the hash table:
    tc_generic(
        tc,
        "Hash table insert: valid insertion failed",
        ht_put(&ht, common_keys[0], a));
    tc_generic(
        tc,
        "Hash table insert: valid insertion failed",
        ht_put(&ht, common_keys[1], b));
    tc_generic(
        tc,
        "Hash table insert: valid insertion failed",
        ht_put(&ht, common_keys[2], c));
    tc_generic(
        tc,
        "Hash table insert: invalid insertion succeeded",
        ht_put(&ht, common_keys[0], a) == false);
    // 2. Lookup:
    tc_generic(
        tc,
        "Hash table lookup: expected element not found",
        (int*)ht_get(&ht, common_keys[0]) == a);
    tc_generic(
        tc,
        "Hash table lookup: expected element not found",
        (int*)ht_get(&ht, common_keys[1]) == b);
    tc_generic(
        tc,
        "Hash table lookup: expected element not found",
        (int*)ht_get(&ht, common_keys[2]) == c);
    // 3. Callback based iteration:
    global_counter = 0;
    ht_for_each(&ht, hash_table_test_callback, tc);
    tc_generic(
        tc,
        "Hash table callback count: incorrect count value",
        global_counter == 3);
    // 4. Cleanup:
    ht_deinit(&ht, int_destructor);
}

void common_test(struct TestContext *tc_parent)
{
    struct TestContext *tc = tc_create_child("COMMON", tc_parent);
    array_test(tc);
    string_test(tc);
    pointer_stack_test(tc);
    trie_test(tc);
    hash_table_test(tc);
}
