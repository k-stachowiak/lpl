/*
 * Copyright (C) 2020-2021 Krzysztof Stachowiak
 *
 * This file is part of the Language Programming Language compiler.
 *
 * Language Programming Language is free software: you can redistribute it
 * and/or modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Language Programming Language compiler is distributed in the hope that it
 * will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "amd64_memory.h"

#include <assert.h>

// Objects management
// ------------------

static struct AMD64Object *obj_create(
        struct DataType *data_type,
        struct AMD64Location location)
{
    struct AMD64Object *result = my_malloc(sizeof(*result));
    result->data_type = data_type;
    result->location = location;
    return result;
}

static void obj_destroy(struct AMD64Object *obj)
{
    dt_destroy(obj->data_type);
    my_free(obj);
}

static void obj_destroy_v(void *obj)
{
    obj_destroy((struct AMD64Object *)obj);
}

void amd64_obj_swap(struct AMD64Object *x, struct AMD64Object *y)
{
    struct AMD64Object temp = *y;
    *y = *x;
    *x = temp;
}

// Scope management
// ----------------

struct AMD64Scope *amd64_scope_create(struct AMD64Scope *parent)
{
    struct AMD64Scope *result = my_malloc(sizeof(*result));
    result->parent = parent;
    trie_init(&result->objects);
    return result;
}

void amd64_scope_destroy(struct AMD64Scope *scope)
{
    if (scope->parent) {
        amd64_scope_destroy(scope->parent);
    }
    trie_deinit(&scope->objects, obj_destroy_v);
    my_free(scope);
}

void amd64_scope_put(
        struct AMD64Scope *scope,
        char *name,
        struct AMD64Object *obj)
{
    trie_insert(&scope->objects, name, obj);
}

struct AMD64Object *amd64_scope_find(struct AMD64Scope *scope, char *name)
{
    struct TrieNode *node = trie_find(&scope->objects, name);
    if (node) {
        return (struct AMD64Object *)node->data;

    } else if (scope->parent) {
        return amd64_scope_find(scope->parent, name);

    } else {
        return NULL;
    }
}

struct ScopeForEachContext {
    struct AMD64Scope *scope;
    void (*parent_callback)(char *, struct AMD64Object *, void *);
    void *parent_data;
};

static void scope_for_each_callback(
        char *name,
        struct TrieNode *node,
        void *data)
{
    struct AMD64Object *obj = (struct AMD64Object *)node->data;
    struct ScopeForEachContext *sfec = (struct ScopeForEachContext *)data;
    sfec->parent_callback(name, obj, sfec->parent_data);
}

void amd64_scope_for_each(
        struct AMD64Scope *scope,
        void (*callback)(char *, struct AMD64Object *, void *),
        void *data)
{
    struct ScopeForEachContext sfec = {
        .parent_callback = callback,
        .parent_data = data
    };
    if (scope->parent) {
        amd64_scope_for_each(scope->parent, callback, data);
    }
    trie_for_each(&scope->objects, scope_for_each_callback, &sfec);
}

// High level registers management
// -------------------------------

enum AMD64GPRegister amd64_reg_to_gpreg(enum AMD64Register reg) {
    switch (reg) {
    case AMD64_REG_AX:
        return AMD64_GPREG_AX;
    case AMD64_REG_BX:
        return AMD64_GPREG_BX;
    case AMD64_REG_CX:
        return AMD64_GPREG_CX;
    case AMD64_REG_DX:
        return AMD64_GPREG_DX;
    case AMD64_REG_SP:
        return AMD64_GPREG_SP;
    case AMD64_REG_BP:
        return AMD64_GPREG_BP;
    case AMD64_REG_SI:
        return AMD64_GPREG_SI;
    case AMD64_REG_DI:
        return AMD64_GPREG_DI;

    case AMD64_REG_8:
        return AMD64_GPREG_8;
    case AMD64_REG_9:
        return AMD64_GPREG_9;
    case AMD64_REG_10:
        return AMD64_GPREG_10;
    case AMD64_REG_11:
        return AMD64_GPREG_11;
    case AMD64_REG_12:
        return AMD64_GPREG_12;
    case AMD64_REG_13:
        return AMD64_GPREG_13;
    case AMD64_REG_14:
        return AMD64_GPREG_14;
    case AMD64_REG_15:
        return AMD64_GPREG_15;

    case AMD64_REG_XMM0:
    case AMD64_REG_XMM1:
    case AMD64_REG_XMM2:
    case AMD64_REG_XMM3:
    case AMD64_REG_XMM4:
    case AMD64_REG_XMM5:
    case AMD64_REG_XMM6:
    case AMD64_REG_XMM7:
    case AMD64_REG_MAX:
        ABORT("Algorithmic corruption");
    }
    ABORT("Impossible state");
}

enum AMD64SSERegister amd64_reg_to_ssereg(enum AMD64Register reg) {
    switch (reg) {
    case AMD64_REG_AX:
    case AMD64_REG_BX:
    case AMD64_REG_CX:
    case AMD64_REG_DX:
    case AMD64_REG_SP:
    case AMD64_REG_BP:
    case AMD64_REG_SI:
    case AMD64_REG_DI:
    case AMD64_REG_8:
    case AMD64_REG_9:
    case AMD64_REG_10:
    case AMD64_REG_11:
    case AMD64_REG_12:
    case AMD64_REG_13:
    case AMD64_REG_14:
    case AMD64_REG_15:
        ABORT("Algorithmic corruption");

    case AMD64_REG_XMM0:
        return AMD64_SSEREG_XMM0;
    case AMD64_REG_XMM1:
        return AMD64_SSEREG_XMM1;
    case AMD64_REG_XMM2:
        return AMD64_SSEREG_XMM2;
    case AMD64_REG_XMM3:
        return AMD64_SSEREG_XMM3;
    case AMD64_REG_XMM4:
        return AMD64_SSEREG_XMM4;
    case AMD64_REG_XMM5:
        return AMD64_SSEREG_XMM5;
    case AMD64_REG_XMM6:
        return AMD64_SSEREG_XMM6;
    case AMD64_REG_XMM7:
        return AMD64_SSEREG_XMM7;

    case AMD64_REG_MAX:
        ABORT("Algorithmic corruption");
    }
    ABORT("Impossible state");
}

enum AMD64Register amd64_reg_from_gpreg(enum AMD64GPRegister reg) {
    switch (reg) {
    case AMD64_GPREG_AH:
        return AMD64_REG_AX;
    case AMD64_GPREG_BH:
        return AMD64_REG_BX;
    case AMD64_GPREG_CH:
        return AMD64_REG_CX;
    case AMD64_GPREG_DH:
        return AMD64_REG_DX;

    case AMD64_GPREG_AX:
        return AMD64_REG_AX;
    case AMD64_GPREG_BX:
        return AMD64_REG_BX;
    case AMD64_GPREG_CX:
        return AMD64_REG_CX;
    case AMD64_GPREG_DX:
        return AMD64_REG_DX;
    case AMD64_GPREG_SP:
        return AMD64_REG_SP;
    case AMD64_GPREG_BP:
        return AMD64_REG_BP;
    case AMD64_GPREG_SI:
        return AMD64_REG_SI;
    case AMD64_GPREG_DI:
        return AMD64_REG_DI;

    case AMD64_GPREG_8:
        return AMD64_REG_8;
    case AMD64_GPREG_9:
        return AMD64_REG_9;
    case AMD64_GPREG_10:
        return AMD64_REG_10;
    case AMD64_GPREG_11:
        return AMD64_REG_11;
    case AMD64_GPREG_12:
        return AMD64_REG_12;
    case AMD64_GPREG_13:
        return AMD64_REG_13;
    case AMD64_GPREG_14:
        return AMD64_REG_14;
    case AMD64_GPREG_15:
        return AMD64_REG_15;
    }
    ABORT("Impossible state");
}

enum AMD64Register amd64_reg_from_ssereg(enum AMD64SSERegister reg) {
    switch (reg) {
    case AMD64_SSEREG_XMM0:
        return AMD64_REG_XMM0;
    case AMD64_SSEREG_XMM1:
        return AMD64_REG_XMM1;
    case AMD64_SSEREG_XMM2:
        return AMD64_REG_XMM2;
    case AMD64_SSEREG_XMM3:
        return AMD64_REG_XMM3;
    case AMD64_SSEREG_XMM4:
        return AMD64_REG_XMM4;
    case AMD64_SSEREG_XMM5:
        return AMD64_REG_XMM5;
    case AMD64_SSEREG_XMM6:
        return AMD64_REG_XMM6;
    case AMD64_SSEREG_XMM7:
        return AMD64_REG_XMM7;
    }
    ABORT("Impossible state");
}

enum AMD64Register amd64_reg_from_ireg(enum AMD64IndirectRegister ireg) {
    switch (ireg) {
    case AMD64_IREG_AX:
        return AMD64_REG_AX;
    case AMD64_IREG_BX:
        return AMD64_REG_BX;
    case AMD64_IREG_CX:
        return AMD64_REG_CX;
    case AMD64_IREG_DX:
        return AMD64_REG_DX;
    case AMD64_IREG_SP:
        return AMD64_REG_SP;
    case AMD64_IREG_BP:
        return AMD64_REG_BP;
    case AMD64_IREG_SI:
        return AMD64_REG_SI;
    case AMD64_IREG_DI:
        return AMD64_REG_DI;

    case AMD64_IREG_8:
        return AMD64_REG_8;
    case AMD64_IREG_9:
        return AMD64_REG_9;
    case AMD64_IREG_10:
        return AMD64_REG_10;
    case AMD64_IREG_11:
        return AMD64_REG_11;
    case AMD64_IREG_12:
        return AMD64_REG_12;
    case AMD64_IREG_13:
        return AMD64_REG_13;
    case AMD64_IREG_14:
        return AMD64_REG_14;
    case AMD64_IREG_15:
        return AMD64_REG_15;
    }
    ABORT("Impossible state");
}

enum AMD64Register amd64_reg_from_sreg(enum AMD64SIBRegister sreg) {
    switch (sreg) {
    case AMD64_SREG_AX:
        return AMD64_REG_AX;
    case AMD64_SREG_BX:
        return AMD64_REG_BX;
    case AMD64_SREG_CX:
        return AMD64_REG_CX;
    case AMD64_SREG_DX:
        return AMD64_REG_DX;
    case AMD64_SREG_SP:
        return AMD64_REG_SP;
    case AMD64_SREG_BP:
        return AMD64_REG_BP;
    case AMD64_SREG_SI:
        return AMD64_REG_SI;
    case AMD64_SREG_DI:
        return AMD64_REG_DI;

    case AMD64_SREG_8:
        return AMD64_REG_8;
    case AMD64_SREG_9:
        return AMD64_REG_9;
    case AMD64_SREG_10:
        return AMD64_REG_10;
    case AMD64_SREG_11:
        return AMD64_REG_11;
    case AMD64_SREG_12:
        return AMD64_REG_12;
    case AMD64_SREG_13:
        return AMD64_REG_13;
    case AMD64_SREG_14:
        return AMD64_REG_14;
    case AMD64_SREG_15:
        return AMD64_REG_15;
    }
    ABORT("Impossible state");
}

bool amd64_reg_is_gp(enum AMD64Register reg)
{
    return reg >= AMD64_REG_AX && reg <= AMD64_REG_15;
}

bool amd64_reg_is_sse(enum AMD64Register reg)
{
    return reg >= AMD64_REG_XMM0 && reg <= AMD64_REG_XMM7;
}

static bool reg_is_type_compatible(enum AMD64Register reg,
                                   struct DataType *data_type)
{
    switch (data_type->type) {
    case DT_PRIMITIVE:
        if (data_type->primitive == DT_PRIM_DOUBLE) {
            return amd64_reg_is_sse(reg);
        } else {
            return amd64_reg_is_gp(reg);
        }
    case DT_POINTER:
    case DT_BUFFER:
    case DT_CSTRING:
        return amd64_reg_is_gp(reg);
    }
    ABORT("Impossible state");
}

static bool reg_occupied_by_loc(enum AMD64Register reg,
                                struct AMD64Location *loc)
{
    if (loc->type == AMD64_LT_REGISTER_GP) {
        return reg == amd64_reg_from_gpreg(loc->gpreg.gpreg);

    } else if (loc->type == AMD64_LT_REGISTER_SSE) {
        return reg == amd64_reg_from_ssereg(loc->ssereg.ssereg);

    } else if (loc->type == AMD64_LT_REGISTER_INDIRECT) {
        return reg == amd64_reg_from_ireg(loc->ireg.ireg);

    } else if (loc->type == AMD64_LT_MEMORY_SIB) {
        return
            reg == amd64_reg_from_sreg(loc->msib.base) ||
            reg == amd64_reg_from_sreg(loc->msib.index);

    } else {
        return false;

    }
}

// Memory manager internal
// -----------------------

// ### Temporaries related helpers

static void mem_temps_clear(struct AMD64Memory *mem)
{
    int i;
    for (i = 0; i != mem->temps.size; ++i) {
        obj_destroy(mem->temps.data[i]);
    }
    array_free(mem->temps);
}

static int mem_temp_index(
        struct AMD64Memory *mem,
        struct AMD64Object *obj)
{
    int i;
    for (i = 0; i != mem->temps.size; ++i) {
        if (mem->temps.data[i] == obj) {
            break;
        }
    }
    return i;
}

static void mem_try_temp_remove(
        struct AMD64Memory *mem,
        struct AMD64Object *obj)
{
    int temp_index;
    if ((temp_index = mem_temp_index(mem, obj)) < mem->temps.size) {
        array_remove(mem->temps, temp_index);
    }
}

// ### Location acquisition/release details

static bool mem_try_acquire_register(struct AMD64Memory *mem,
                                     enum AMD64Register reg)
{
    // This one tries acquiring a single register.

    if (!amd64_mem_register_free(mem, reg)) {
        return false;
    } else {
        mem->register_map |= reg;
        return true;
    }
}

static bool mem_try_acquire_registers(
        struct AMD64Memory *mem,
        struct AMD64Location *loc)
{
    // Here we try to acquire all (if any) registers required by the given
    // location definition.

    switch (loc->type) {
    case AMD64_LT_IMMEDIATE:
    case AMD64_LT_MEMORY_OFFSET_SMALL:
    case AMD64_LT_MEMORY_OFFSET_BIG:
    case AMD64_LT_MEMORY_RIP_OFFSET:
    case AMD64_LT_DEFERRED_MEMORY_RIP_OFFSET:
        return true;

    case AMD64_LT_REGISTER_GP:
        return mem_try_acquire_register(
            mem, amd64_reg_from_gpreg(loc->gpreg.gpreg));

    case AMD64_LT_REGISTER_SSE:
        return mem_try_acquire_register(
            mem, amd64_reg_from_ssereg(loc->ssereg.ssereg));

    case AMD64_LT_REGISTER_INDIRECT:
        if (loc->ireg.ireg == AMD64_IREG_SP || loc->ireg.ireg == AMD64_IREG_BP) {
            // Don't attempt acquisition of SP nor BP; they're special
            return true;
        } else {
            return mem_try_acquire_register(
                mem, amd64_reg_from_ireg(loc->ireg.ireg));
        }

    case AMD64_LT_MEMORY_SIB:
        return
            mem_try_acquire_register(mem, amd64_reg_from_sreg(loc->msib.base)) &&
            mem_try_acquire_register(mem, amd64_reg_from_sreg(loc->msib.index));
    }
    ABORT("Impossible state");
}

static void mem_register_release(struct AMD64Memory *mem, enum AMD64Register reg)
{
    mem->register_map &= ~reg;
}

static struct AMD64Location mem_acquire_stack_location(
        struct AMD64Memory *mem,
        int size)
{
    mem->rbp_offset -= size;
    struct AMD64Displacement disp = { .value = mem->rbp_offset };
    return amd64_loc_make_register_indirect(amd64_loc_size_from_bytes(size),
                                            AMD64_IREG_BP,
                                            AMD64_IAS_64,
                                            disp);
}

// Memory manager public
// ---------------------

// ### Initialization/deinitialization

void amd64_mem_init(struct AMD64Memory *mem)
{
    mem->register_map = 0;
    mem->rbp_offset = -0x10; // -8 for return address, -8 for the current RBP
    mem->local_scope = amd64_scope_create(NULL);
    mem->temps.data = NULL;
    mem->temps.size = mem->temps.cap = 0;
}

void amd64_mem_deinit(struct AMD64Memory *mem)
{
    mem_temps_clear(mem);
    amd64_scope_destroy(mem->local_scope);
    mem->rbp_offset = -0x10; // -8 for return address, -8 for the current RBP
    mem->register_map = 0;
}

// ### Sub-scope management

void amd64_mem_subscope_create(struct AMD64Memory *mem)
{
    mem->local_scope = amd64_scope_create(mem->local_scope);
}

void amd64_mem_subscope_destroy(struct AMD64Memory *mem)
{
    struct AMD64Scope *parent = mem->local_scope->parent;
    mem->local_scope->parent = NULL;
    amd64_scope_destroy(mem->local_scope);
    mem->local_scope = parent;
}

// ### Register acquisition

bool amd64_mem_register_free(struct AMD64Memory *mem, enum AMD64Register reg)
{
    return !(mem->register_map & reg);
}

enum AMD64Register amd64_mem_find_free_gpreg(struct AMD64Memory *mem)
{
    enum AMD64Register reg;
    for (reg = AMD64_REG_AX; reg != AMD64_REG_XMM0; reg <<= 1) {
        if (amd64_mem_register_free(mem, reg)) {
            return reg;
        }
    }
    return AMD64_REG_MAX;
}

enum AMD64Register amd64_mem_find_free_ssereg(struct AMD64Memory *mem)
{
    enum AMD64Register reg;
    for (reg = AMD64_REG_XMM0; reg != AMD64_REG_MAX; reg <<= 1) {
        if (amd64_mem_register_free(mem, reg)) {
            return reg;
        }
    }
    return AMD64_REG_MAX;
}

enum AMD64Register amd64_mem_find_free_compatible_register(
        struct AMD64Memory *mem,
        struct DataType *data_type)
{
    enum AMD64Register reg;
    for (reg = 1; reg != AMD64_REG_MAX; reg <<= 1) {
        if (amd64_mem_register_free(mem, reg) &&
            reg_is_type_compatible(reg, data_type)) {
            break;
        }
    }
    return reg;
}

// ### Object creation

struct AMD64Object *amd64_mem_create_object(
        struct AMD64Memory *mem,
        struct DataType *data_type,
        struct AMD64Location location)
{
    struct AMD64Object *result;
    if (!mem_try_acquire_registers(mem, &location)) {
        ABORT("Algorythmic corruption: illegal Object info creation.");
    }
    result = obj_create(data_type, location);
    array_append(mem->temps, result);
    return result;
}

struct AMD64Object *amd64_mem_create_imm_unit(struct AMD64Memory *mem)
{
    return amd64_mem_create_object(
        mem,
        dt_create_primitive(DT_PRIM_UNIT),
        amd64_loc_make_immediate(AMD64_LS_8_BIT, 1));
}

struct AMD64Object *amd64_mem_create_imm_boolean(
        struct AMD64Memory *mem,
        uint8_t value)
{
    return amd64_mem_create_object(
        mem,
        dt_create_primitive(DT_PRIM_BOOLEAN),
        amd64_loc_make_immediate(AMD64_LS_8_BIT, value));
}

struct AMD64Object *amd64_mem_create_imm_word(
        struct AMD64Memory *mem,
        int64_t value)
{
    return amd64_mem_create_object(
        mem,
        dt_create_primitive(DT_PRIM_WORD),
        amd64_loc_make_immediate(AMD64_LS_64_BIT, value));
}

struct AMD64Object *amd64_mem_create_reg(
        struct AMD64Memory *mem,
        enum AMD64Register reg,
        struct DataType *data_type)
{
    assert(reg_is_type_compatible(reg, data_type));
    if (amd64_reg_is_sse(reg)) {
        return amd64_mem_create_object(
            mem,
            data_type,
            amd64_loc_make_register_sse(amd64_reg_to_ssereg(reg)));
    } else {
        return amd64_mem_create_object(
            mem,
            data_type,
            amd64_loc_make_register_gp(
                amd64_loc_size_from_bytes(dt_effective_size(data_type)),
                amd64_reg_to_gpreg(reg)));
    }
}

struct AMD64Object *amd64_mem_create_reg_word(
        struct AMD64Memory *mem,
        enum AMD64Register reg)
{
    return amd64_mem_create_reg(
        mem, reg, dt_create_primitive(DT_PRIM_WORD));
}

struct AMD64Object *amd64_mem_create_stack_object(
        struct AMD64Memory *mem,
        struct DataType *data_type)
{
    if (data_type->type == DT_BUFFER || data_type->type == DT_CSTRING) {
        ABORT("Not implemented yet");

    } else {
        return amd64_mem_create_object(
            mem,
            data_type,
            mem_acquire_stack_location(mem, dt_effective_size(data_type)));
    }
}

struct AMD64Object *amd64_mem_create_stack_word(struct AMD64Memory *mem)
{
    return amd64_mem_create_stack_object(
        mem, dt_create_primitive(DT_PRIM_WORD));
}

// ### Object disposal

void amd64_mem_dispose(struct AMD64Memory *mem, struct AMD64Object *obj)
{
    if (!obj) {
        return;
    }

    // 1. Don't remove an object if it's registered in the scope:
    if (amd64_mem_find_by_obj(mem, obj)) {
        return;
    }

    // 2. Unregister from temps if found there:
    mem_try_temp_remove(mem, obj);

    // 3. Release register if associated with this object:
    if (obj->location.type == AMD64_LT_REGISTER_GP) {
        mem_register_release(
            mem, amd64_reg_from_gpreg(obj->location.gpreg.gpreg));

    } else if (obj->location.type == AMD64_LT_REGISTER_SSE) {
        mem_register_release(
            mem, amd64_reg_from_ssereg(obj->location.ssereg.ssereg));

    } else if (obj->location.type == AMD64_LT_REGISTER_INDIRECT &&
               obj->location.ireg.ireg != AMD64_IREG_SP &&
               obj->location.ireg.ireg != AMD64_IREG_BP) {
        // Don't attempt releasing SP or BP; they're special
        // TODO: Try making rSP and rBP as non-special as possible
        mem_register_release(
            mem, amd64_reg_from_ireg(obj->location.ireg.ireg));

    } else if (obj->location.type == AMD64_LT_MEMORY_SIB) {
        mem_register_release(mem, amd64_reg_from_sreg(obj->location.msib.base));
        mem_register_release(mem, amd64_reg_from_sreg(obj->location.msib.index));

    }

    // 4. Destroy the info object:
    obj_destroy(obj);
}

// ### Scope insertion

bool amd64_mem_try_put(
        struct AMD64Memory *mem,
        char *name,
        struct AMD64Object *obj)
{
    if (amd64_scope_find(mem->local_scope, name)) {
        return false;
    }

    mem_try_temp_remove(mem, obj);
    amd64_scope_put(mem->local_scope, name, obj);

    return true;
}

void amd64_mem_put(
        struct AMD64Memory *mem,
        char *name,
        struct AMD64Object *obj)
{
    if (!amd64_mem_try_put(mem, name, obj)) {
        ABORT("Algorythmic corruption");
    }
}

// ### Scope lookup

struct FindByObjContext {
    struct AMD64Object *obj;
    char *name;
};

static void find_by_obj_callback(char *name, struct AMD64Object *obj, void *data)
{
    struct FindByObjContext *ctx = (struct FindByObjContext *)data;
    if (ctx->name) {
        return;
    }
    if (obj == ctx->obj) {
        ctx->name = name;
    }
}

char *amd64_mem_find_by_obj(
        struct AMD64Memory *mem,
        struct AMD64Object *obj)
{
    struct FindByObjContext ctx = {
        .obj = obj,
        .name = NULL
    };
    amd64_scope_for_each(mem->local_scope, find_by_obj_callback, &ctx);
    return ctx.name;
}

struct FindByRegContext {
    enum AMD64Register reg;
    struct AMD64Object *obj;
};

static void find_by_reg_callback(char *name, struct AMD64Object *obj, void *data)
{
    struct FindByRegContext *ctx = (struct FindByRegContext *)data;
    (void)name;
    if (ctx->obj) {
        return;
    }
    if (reg_occupied_by_loc(ctx->reg, &obj->location)) {
        ctx->obj = obj;
    }
}

struct AMD64Object *amd64_mem_find_by_reg(
        struct AMD64Memory *mem,
        enum AMD64Register reg)
{
    int i;
    struct FindByRegContext ctx = {
        .reg = reg,
        .obj = NULL
    };
    amd64_scope_for_each(mem->local_scope, find_by_reg_callback, &ctx);
    if (ctx.obj) {
        return ctx.obj;
    }
    for (i = 0; i != mem->temps.size; ++i) {
        if (reg_occupied_by_loc(reg, &mem->temps.data[i]->location)) {
            return mem->temps.data[i];
        }
    }
    return NULL;
}

struct AMD64Object *amd64_mem_find_by_name(struct AMD64Memory *mem, char *name)
{
    return amd64_scope_find(mem->local_scope, name);
}

bool amd64_mem_is_temp(struct AMD64Memory *mem, struct AMD64Object *obj)
{
    return mem_temp_index(mem, obj) < mem->temps.size;
}
