/*
 * Copyright (C) 2020 Krzysztof Stachowiak
 *
 * This file is part of the Language Programming Language compiler.
 *
 * Language Programming Language is free software: you can redistribute it
 * and/or modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Language Programming Language compiler is distributed in the hope that it
 * will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef AST_H
#define AST_H

#include "test.h"

#include <stdbool.h>
#include <stdint.h>

// AST
// ===
// This is the definition of the Abstract Syntax Tree.

// Data structures
// ---------------

enum AstLiteralType {
    // This structure stores numbers representable in the source code,
    // i.e. they do not have to map directly to their machine representation.
    // E.g. both 8-bit and 64-bit integer may be represented by an integral
    // literal.
    AST_LIT_VOID,
    AST_LIT_UNIT,
    AST_LIT_BOOLEAN,
    AST_LIT_INTEGER,
    AST_LIT_REAL,
    AST_LIT_CHARACTER,
    AST_LIT_STRING
};

struct AstLiteral {
    enum AstLiteralType type;
    union {
        char boolean;
        int64_t integer;
        double real;
        char character;
        char *string;
    };
};

enum AstUnaryType {
    AST_UN_POSITIVE,
    AST_UN_NEGATIVE,
    AST_UN_DEREFERENCE,
    AST_UN_ADDRESS
};

struct AstUnary {
    enum AstUnaryType type;
    struct AstNode *operand;
};

enum AstBinaryType {
    AST_BIN_ADD,
    AST_BIN_SUBTRACT,
    AST_BIN_MULTIPLY,
    AST_BIN_DIVIDE,
    AST_BIN_MODULO,
    AST_BIN_POWER,
    AST_BIN_ASSIGN,
    AST_BIN_IMPLY,
    AST_BIN_SEQUENCE,
    AST_BIN_BLOCK_SEQUENCE,
    AST_BIN_LIST_SEQUENCE,
    AST_BIN_EQUAL,
    AST_BIN_NOT_EQUAL,
    AST_BIN_LESS_THAN,
    AST_BIN_GREATER_THAN,
    AST_BIN_LESS_THAN_OR_EQUAL,
    AST_BIN_GREATER_THAN_OR_EQUAL,
    AST_BIN_CONCATENATE,
    AST_BIN_PROC_CALL,
    AST_BIN_SUBSCRIPT,
    AST_BIN_PROC_HEAD
};

struct AstBinary {
    enum AstBinaryType type;
    struct AstNode *left, *right;
};

enum AstBlockType {
    AST_BLOCK_IFFI,
    AST_BLOCK_DOOD,
    AST_BLOCK_PROCCORP
};

struct AstBlock {
    enum AstBlockType type;
    struct AstNode *body;
};

struct AstSymbol {
    char *string;
};

enum AstNodeType {
    AST_LITERAL,
    AST_SYMBOL,
    AST_UNARY,
    AST_BINARY,
    AST_BLOCK
};

struct AstNode {
    enum AstNodeType type;
    union {
        struct AstLiteral literal;
        struct AstSymbol symbol;
        struct AstUnary unary;
        struct AstBinary binary;
        struct AstBlock block;
    };
};

// Constructors
// ------------

struct AstNode *ast_create_literal_void(void);
struct AstNode *ast_create_literal_unit(void);
struct AstNode *ast_create_literal_bool(char value);
struct AstNode *ast_create_literal_integer(uint64_t value);
struct AstNode *ast_create_literal_real(double value);
struct AstNode *ast_create_literal_character(char value);
struct AstNode *ast_create_literal_string(char *value);

struct AstNode *ast_create_symbol(char *value);

struct AstNode *ast_create_unary(
        enum AstUnaryType type,
        struct AstNode *operand);

struct AstNode *ast_create_binary(
        enum AstBinaryType type,
        struct AstNode *left,
        struct AstNode *right);

struct AstNode *ast_create_block(
        enum AstBlockType type,
        struct AstNode *body);

// Introspection
// -------------

bool ast_is_literal(struct AstNode *node);
bool ast_is_literal_spec(struct AstNode *node, enum AstLiteralType type);
bool ast_is_symbol(struct AstNode *node);
bool ast_is_symbol_spec(struct AstNode *node, char *value);

// Deep copy algorithm
// -------------------

struct AstNode *ast_copy(struct AstNode *node);

// Deep destructor
// ---------------

void ast_destroy(struct AstNode *node);

// Other algorithms
// ----------------

// The visit algorithm prerforms a pre-order traverse of the AST calling the
// callback function for each of the encountered nodes. Additionally the
// traverse depth is passed to the callback.
void ast_visit(
        struct AstNode *node,
        void (*callback)(void *data, int depth, struct AstNode *node),
        void *data);

// Serializes the AST to a string in a tree format
char *ast_to_string_tree(struct AstNode *node);

// Serializes the AST to a string in an algebraic form
char *ast_to_string_algebraic(struct AstNode *node);

// Serializes the AST to a string in a dot file format
char *ast_to_string_dot(struct AstNode *node);

// Iterates over a sequence of expressions separated with a sequence operator.
// In the tree space the structure isn't linear, which makes it complicated to
// traverse in a general way.
void ast_for_each_left(
        struct AstNode *node,
        enum AstBinaryType op,
        void (*callback)(void*, struct AstNode*),
        void *data);

// Test procedure
// --------------

void ast_test(struct TestContext *tc_parent);

#endif
