\documentclass[10pt,a4paper]{article}

\usepackage[english]{babel}
\usepackage[english]{isodate}

\usepackage{graphicx}
\usepackage{hyperref}

\usepackage[dvipsnames]{xcolor}

\usepackage{listings}
\lstdefinelanguage
    {mylanguage}
    {
      morekeywords=[1]{proc,corp,prod,dorp,sum,mus},
      morekeywords=[2]{if,fi,do,od},
      morecomment=[s]{\{}{\}},
      morestring=[b]",
      morestring=[b]',
    }
\lstnewenvironment
    {mylisting}
    {~\newline\minipage{.95\textwidth}}
    {\endminipage\newline}
\lstset{
    basicstyle=\scriptsize\ttfamily,
    breaklines=true,
    frame=single,
    aboveskip=1.5em,
    belowskip=1.5em,
    language=mylanguage,
    showstringspaces=false,
    keywordstyle=[1]\color{blue},
    keywordstyle=[2]\color{RoyalBlue},
    stringstyle=\color{Maroon},
    commentstyle=\color{OliveGreen},
}

\newcommand{\lplblock}[1]{\textcolor{RoyalBlue}{\texttt{#1}}}
\newcommand{\lploperator}[1]{\textcolor{RoyalBlue}{\texttt{#1}}}
\newcommand{\lplkeyword}[1]{\textcolor{blue}{\texttt{#1}}}

\title{LPL Manual}
\author{Krzysztof Stachowiak}
\date{\today}

\begin{document}

\maketitle
\thispagestyle{empty}

\pagebreak
\thispagestyle{empty}
\hspace{0pt}
\vfill
\begin{center}
  {\huge DON'T PANIC}
\end{center}
\vfill
\hspace{0pt}
\pagebreak

\pagenumbering{Roman}
\tableofcontents
\newpage

\pagenumbering{arabic}
\section{Introduction}
A quote by Richard Feynman says: ``What I cannot create, I do not understand''.
LPL is an effort of its author towards understanding:
\begin{itemize}
\item buggy compilers,
\item for underdesigned programming languages,
\item targetting overly complicated hardware.
\end{itemize}

\section{Examples study}
This section presents a few simple programs that depict the intended use of LPL.
Here we emphasise on the language's syntax and the fundamental semantics.

\subsection{Hello, World!}
Let's have a look at a typical ``Hello, World!'' example.
The program looks like this:

\begin{mylisting}
proc main(Void) -> ret #
    print("Hello, World!\n");
    ret := 0
corp
\end{mylisting}

The presented code is a single, complex algebraic expression, which has been depicted in Figure~\ref{Figure:HelloWorldAST}.
Each node in the tree represents either an operator/operand, or a group.
Operands and operators are to be understood as in a mathematical expression, e.g. \texttt{2 + 3} consists of an operator \texttt{+} and two operands: \texttt{2} and \texttt{3}.
This expression would translate to a simple 3-node tree with a \texttt{+} operator as the root with two children: \texttt{2} and \texttt{3}.
The groups enclose more than one expression element as a single tree node.
For example, an expression \texttt{(2 + 2) * 2} contains a groupped expression enclosed in the parentheses.
The root of this tree is the operator \texttt{*}, with two children: \texttt{2} and the group enclosed in the parentheses, which in turn forms a subtree as in the previous example.

What determines the position of the given operators, operands and groups is the operator priority.
Just like in mathematics, in LPL each operator and block is assigned a priority.

\begin{figure}[ht]
  \includegraphics[width=1.0\textwidth]{hello_world_ast}
  \caption{An example of a program decomposed into a single AST tree}
  \label{Figure:HelloWorldAST}
\end{figure}

One important thing to note is, that while in mathematics we use only one method of grouping expressions - the parentheses, in LPL there are more kinds of blocks.

In the demonstrated program, we can see an example of the \lplblock{proc - corp} block, which defines a procedure.
All the blocks are expected to contain one or more expressions separated by the block sequence operator: \lploperator{\#}.
Procedures will take the following shape: \texttt{proc <header> \# <body> corp}.

In this case the header is: \texttt{main(Void) -> ret}.
It is based on the procedure header operator \lploperator{->}, which separates the call pattern and the return pattern.
The call pattern consists of the procedure's name and its argument list: \texttt{name(arguments)}.
The procedure's name is ``main'' ane it's argument list is given by the keyword \lplkeyword{Void}.
This is a special case that allows defining a procedure that doesn't take any arguments.
The data types, including \lplkeyword{Void} and it's semantics, will be described in more detail later.

The second expression in the given procedure is a sequence of commands -- the procedure's body.
A sequence of commands is separated with the command sequence operator: \lploperator{;}.
If the return pattern contains variables (here, just \textit{ret}), they must all be assigned to in the procedure body.

The first command in our procedure's body is a call to the \texttt{print} procedure, which takes a character buffer and passes it to the operating system for printing into the default output stream.
The second command is an assignment (via the assignment operator: \lploperator{:=}) of a literal \texttt{0} to the variable \texttt{ret}.
It has been designated as the procedure result in the header, which effectively causes this value to be returned as the procedure's result, once its execution is completed.
If anybody was to execut a call to our \texttt{main} procedure, they could assign it to a variable: \texttt{x := main()}.
This will assign the value that \texttt{ret} has at the end of the procedure's execution to the variable \texttt{x}.

\subsection{Greatest common divisor}
In this section an example procedure for computing the greatest common divisor is presented.
We'll be using the Euclid's algorithm for that, which can be implemented like this:

\begin{mylisting}
proc gcd(x, y) -> z #
    do
        y != 0 => x, y := y, x % y
    od;
    z := x
corp
\end{mylisting}

The goal of this example is to present two features: multiple variable assignment and the \lplblock{do - od} block.

The multiple variable assignment allows assigning values to multiple variables in a single command.
The general syntax is the following: \texttt{x1, x2, ... := y1, y2, ...}.
On the left hand side of the \lploperator{:=} operator we can see a list of destinations, whereas on the right hand side we can see a list of resprctive sources.
To define a list, the list sequence operator \lploperator{,} is used.
The semantics of such acommand is that first all the expressions to the right of the \lploperator{:=} operator are evaluated into temporary variables, then they are assigned to the variables to the left of the operator.
Obviously, we need equal amount of sources and destinations for such an assignment to be valid.
Although the multiple assignment feature may be seen mainly as a convenience reducing the number of commands written in the source, it can be used for other interesting purposes.
For example it is allowed to put the same variable on the both sides of the assignment operator, which would allow us to implement the swap operation like this: \texttt{x, y := y, x}.

The second feature presented in this example is the \lplblock{do - od} block, that allows for the implementation of the repetition (a.k.a. iteration) semantics.
This type of block must contain a sequence of so called \textit{guarded command}.
The guarded command is constructed using the implication operator \lploperator{=>}, to the left of which a condition (a.k.a. the \textit{guard}) is given, and to the right of which there's a command.
The command will not be executed if the guarding expression evaluates to false.
If it evaluates to true, then in case of this type of block it will be executed.
The contents of \lplblock{do - od} block are evaluated repeatedly.
For each iteration the guarded commands are checked, and all the commands, for which the guarding expression evaluates to true, are executed.
If in a given iteration no command has been executed, the iteration is finished.

On another note, the example from this section may be rewritten to an equivalent form:
\begin{mylisting}
proc gcd(x, y) -> x #
    do
        y != 0 => x, y := y, x % y
    od
corp
\end{mylisting}
Note, how the argument \texttt{x} is also designated as the procedure's return value.
This saves us explicit introduction of the result variable, however, one could argue, that it reduce the procedure's readability.

\subsection{Signum}
Let's have a look at a simple procedure implementing the mathematical signum function:

\begin{mylisting}
proc signum(x) -> ret #
    if
        ^x < 0 => ret := -1 #
        ^x = 0 => ret := 0 #
        ^x > 0 => ret := 1
    fi
corp
\end{mylisting}

The first feature presented in this section is the pointer dereference operator: \lploperator{\^}.
In the procedure it is assumed that \texttt{x} is a pointer, and therefore the result of its dereferencing is the value it points to.

Another feature introduced in this example is a new type of block: \lplblock{if - fi}.
It allows for the implementation of the conditional execution of parts of the program.
Similarily to the \lplblock{do - od}, the \lplblock{if - fi} block is expected to contain a sequence of guarded commands.

For any \lplblock{if - fi} block's execution, only one of the guarded commands will be executed: one for which the guarding expression is true.
In the given example three alternative commands are given, resulting in different procedure's outcomes.
Based on the rule of trichotomy, the provided guards can be claimed to be exclusive and exhaustive, i.e. all possible values of \texttt{\^{}x} are covered by them, and no \texttt{\^{}x} value leads to more than one guard evaluating to true.
This is significant for two reasons.
If more than one guard expression evaluates to true, only one of them will be executed, and it is (and for a good reason) not specified, which.
Secondly, if no guard expression evaluates to true a run time error is reported.
Therefore a robustly defined algorithm must have the guard set carefully defined to cover all the possible runtime preconditions without an accidental overlap.

\section{Data types}

\subsection{Primitives}

The basic types are: \texttt{Void}, \texttt{Unit}, \texttt{Boolean}, \texttt{Byte}, \texttt{Word} and \texttt{Double}.

\emph{Void} represents a type with no values at all.
No variable of this type may ever exist, but it isn't entirely pointless.
It may be used to express arguments list for a procedure that is supposed to take none.

\emph{Unit} is a type that only has a single value.
It is useful when we want to express a computation that does not return a meaningful result, such as a procedure that only has side effects, e.g. printing a string on the screen.

\emph{Boolean} is a two-value type allowing for expressing logical values: \lplkeyword{true} and \lplkeyword{false}.

\emph{Byte} is a type representing a smallest addressable unit of data on a given architecture; most often 8-bit.

\emph{Word} type represents a default machine's integral type, e.g. 64-bit on the x86 architectures.

\emph{Double} type represents a default machine's floating point type, e.g. 64-bit \emph{IEE754} representation.

\subsection{Sequences}

There are two types of expressing sequences of objects in memory: \emph{arrays} and \emph{buffers}.

An array type is given by the type of the stored object and number of objects.
This allows for efficient implementation of assignment between array object of the same type as it is only a matter of copying the bytes between two locations of sizes known at the compile time.
It is convenient for expressing constructs such as matrices, or sequences encoded in binary protocols.

An array is initialized in the following way:

\begin{mylisting}
x := Array(1, 2, 3)
\end{mylisting}
or, if we don't know the contents beforehand:
\begin{mylisting}
x := Array_of(Word, 3);
x[0] := 1;
x[1] := 2;
x[2] := 3;
\end{mylisting}

Array type is not convenient to express a type such as string, as two strings of different lengths are in general considered to be of the same type, but e.g. two mathematical vectors of different lengths are not.

The buffer type resolves this issue as in its case, the buffer's length is not a part of the type itself.
Instead the buffer's size is stored along the buffer's data and is only known at the run time.

A buffer is initialized in the following way:

\begin{mylisting}
x := Buffer(1, 2, 3)
\end{mylisting}
or, if we don't know the contents beforehand:
\begin{mylisting}
x := Buffer_of(Word, 3);
x[0] := 1;
x[1] := 2;
x[2] := 3;
\end{mylisting}

\subsection{Pointers}

Pointers are variables that store reference information about (or addresses of) other variables.
Also, when accessing a sequence of variables, knowing the address and size of one of them, we may compute the address of its neighbors.
Therefore pointers are a convenient way of traversing arrays or buffers.

Here is an example code dealing with pointers:

\begin{mylisting}
x := 4;
x_ptr := @x;
^x_ptr := 2;
print(to_string(x));     { This will print "2" }
print(to_string(^x_ptr)) { This will print "2" as well }
\end{mylisting}

\subsection{User defined types}

Users may create new types based on the existing ones.
The most common way of doing so is combining many values in structures, a.k.a. records.
Let's say, we have combined two variables of types \texttt{T1} and \texttt{T2} into a structure.
Note, how \texttt{T1} allows its variables to represent one of the set of values.
E.g. on x86 architectures, a \emph{Byte} variable can assume values: 0, 1, 2, ..., 255; it can have 256 different values.
We can associate a notion of \emph{set} of these numbers with the \emph{Byte} type.
We can do so for \texttt{T1}, \texttt{T2}, and other types (note, that for some types the associated set may be infinite; note 2, that when introducing the most basic data types, it was stated clearly, how big are their value sets -- wat is their cardinality).
With a structure of two values, of types \texttt{T1} and \texttt{T2}, we may express all the possible combinations of the values in sets associated with the given types.
The resulting set of possible values will be a Cartesian product of the original sets, and hence, the structure-like types may be associated with the product operation.
A way to define such a product type in the language is using the \lplblock{prod - dorp} block.
Let's look at the example of defining a product type that represents a pair of values:

\begin{minipage}{\linewidth}
  \begin{mylisting}
prod Pair(T1, T2) #
  a : T1 #
  b : T2
dorp
  \end{mylisting}
\end{minipage}

The first element in the block, \texttt{Pair(T1, T2)}, gives the type's name and a list of arguments.
The following elements in the block, \texttt{a : T1} and \texttt{b : T2}, define the members of the type.
When we define a procedure, we don't need to specify the argument's types, as they may be deduced based on the call to the given procedure; we can, therefore, say: \texttt{foo(x)}, not specifying the type of \texttt{x}.
We can say the same about the variables used inside the procedure, as their values are computed based on existing values, and thus their types can be deduced as well; e.g. \texttt{x := 1} implies, that \texttt{x} is to be of an integral type.
However, inside a type definition, we just enumerate its members, for which there is no way of deducing their types.
This is why, we must use the constraint operator: \lploperator{:}.
Stating \texttt{x : y} means, that \texttt{x} must fulfill a constraint \texttt{y}.
In the given example, we constrain the member \texttt{a} to be of type \texttt{T1}, and \texttt{b} to be of type \texttt{T2}.

Note, that this renders the type \texttt{Pair} abstract, until \texttt{T1} and \texttt{T2} have been provided.
We create an object of such type in the following way:
\begin{mylisting}
a := Pair(3 : Word, 5 : Byte)
\end{mylisting}
to specify \texttt{T1} and \texttt{T2} explicitly, or:
\begin{mylisting}
a := Pair(3, 5)
\end{mylisting}
to allow for the compiler to deduce the types from the given arguments.

\bigskip

Another user defined type is called the sum type.
Analogously to sets, the sum type represents a type, that can represent all the values of the types that constitute it, but only one at the time.
In the set theory, such an operation is called \emph{union}, and so it is called in some programming languages.
We define a sum type with a \lplblock{sum - mus} block.
As an example, let's consider a type \emph{Maybe} known from the language Haskell:

\begin{mylisting}
sum Maybe(T) #
    just : T #
    nothing : Unit
mus
\end{mylisting}

A variable of type \texttt{Maybe(T)} can assume any value of type T \emph{and} \texttt{unit}.

We create a sum type object in the following way:
\begin{mylisting}
x : Maybe(Word);
x.just := 3
\end{mylisting}
or:
\begin{mylisting}
x : Maybe(Word);
x.nothing := unit
\end{mylisting}

In both examples, in the first statement we constrain \texttt{x} to be of the specific type \texttt{Maybe(Word)} (note, that as the underlying type \texttt{T} has been provided, we obtain a concrete type out of an abstract one).
The second statement assigns a value to a specific member of the sum.

The semantics of reading and writing to a variable of a sum type is, that all the possible alternative values share the same memory area.
Therefore it is easy to determine an outcome of a read, if it is done for the same member as the last write.
A read of another member will result with obtaining a value, created by memory-level projection of the last written variable, in whichever way it was encoded.
Several pairs of types share a common memory footprint, which allows for using sum types for conversion between them.

\end{document}
