/*
 * Copyright (C) 2020 Krzysztof Stachowiak
 *
 * This file is part of the Language Programming Language compiler.
 *
 * Language Programming Language is free software: you can redistribute it
 * and/or modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Language Programming Language compiler is distributed in the hope that it
 * will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "data_types.h"
#include "common.h"

#include <stdlib.h>

struct DataType *dt_create_primitive(enum DataTypePrimitiveType primitive)
{
    struct DataType *result = my_malloc(sizeof(*result));
    result->type = DT_PRIMITIVE;
    result->primitive = primitive;
    return result;
}

struct DataType *dt_create_pointer(struct DataType *subtype)
{
    struct DataType *result = my_malloc(sizeof(*result));
    result->type = DT_POINTER;
    result->pointer.subtype = subtype;
    return result;
}

struct DataType *dt_create_buffer(struct DataType *subtype)
{
    struct DataType *result = my_malloc(sizeof(*result));
    result->type = DT_BUFFER;
    result->buffer.subtype = subtype;
    return result;
}

struct DataType *dt_create_cstring(void) {
    struct DataType *result = my_malloc(sizeof(*result));
    result->type = DT_CSTRING;
    return result;
}

struct DataType *dt_copy(struct DataType *dt)
{
    if (!dt) {
        return NULL;
    }

    switch (dt->type) {
    case DT_PRIMITIVE:
        return dt_create_primitive(dt->primitive);
    case DT_POINTER:
        return dt_create_pointer(dt_copy(dt->pointer.subtype));
    case DT_BUFFER:
        return dt_create_buffer(dt_copy(dt->buffer.subtype));
    case DT_CSTRING:
        return dt_create_cstring();
    }

    ABORT("Impossible state");
}

void dt_swap(struct DataType **x, struct DataType **y)
{
    struct DataType *temp = *y;
    *y = *x;
    *x = temp;
}

void dt_destroy(struct DataType *dt)
{
    if (!dt) {
        return;
    }

    switch (dt->type) {
    case DT_PRIMITIVE:
        break;
    case DT_POINTER:
        dt_destroy(dt->pointer.subtype);
        break;
    case DT_BUFFER:
        dt_destroy(dt->pointer.subtype);
        break;
    case DT_CSTRING:
        break;
    }

    my_free(dt);
}

bool dt_equal(struct DataType *x, struct DataType *y)
{
    enum DataTypeType common_type;

    if (!x && !y) {
        return true;
    }

    if ((!x && y) || (x && !y)) {
        return false;
    }

    if (x->type != y->type) {
        return false;
    }

    common_type = x->type;

    switch (common_type) {
    case DT_PRIMITIVE:
        return x->primitive == y->primitive;

    case DT_POINTER:
        return dt_equal(x->pointer.subtype, y->pointer.subtype);

    case DT_BUFFER:
        return dt_equal(x->buffer.subtype, y->buffer.subtype);

    case DT_CSTRING:
        return true;
    }

    ABORT("Impossible state");
}

bool dt_is_buffer_ptr(struct DataType *dt)
{
    return
        dt->type == DT_POINTER &&
        dt->pointer.subtype->type == DT_BUFFER;
}

char *dt_name(struct DataType *dt)
{
    char *result = NULL;
    char *child_name;

    if (!dt) {
        return NULL;
    }

    switch (dt->type) {
    case DT_PRIMITIVE:
        switch (dt->primitive) {
        case DT_PRIM_UNIT:
            str_append(result, "unit");
            break;
        case DT_PRIM_BOOLEAN:
            str_append(result, "boolean");
            break;
        case DT_PRIM_BYTE:
            str_append(result, "byte");
            break;
        case DT_PRIM_WORD:
            str_append(result, "word");
            break;
        case DT_PRIM_DOUBLE:
            str_append(result, "double");
            break;
        }
        break;

    case DT_POINTER:
        child_name = dt_name(dt->pointer.subtype);
        str_append(result, "pointer_to(%s)", child_name);
        my_free(child_name);
        break;

    case DT_BUFFER:
        child_name = dt_name(dt->buffer.subtype);
        str_append(result, "buffer_of(%s)", child_name);
        my_free(child_name);
        break;

    case DT_CSTRING:
        str_append(result, "c-string");
        break;
    }

    return result;
}

int dt_effective_size(struct DataType *dt)
{
    if (!dt) {
        ABORT("Invalid input");
    }

    switch (dt->type) {
    case DT_PRIMITIVE:
        switch (dt->primitive) {
        case DT_PRIM_UNIT:
        case DT_PRIM_BOOLEAN:
        case DT_PRIM_BYTE:
            return 1;
        case DT_PRIM_WORD:
        case DT_PRIM_DOUBLE:
            return 8;
        }
        break;

    case DT_POINTER:
        return 8;

    case DT_BUFFER:
        // Effective size for buffer is 8 as we store the pointer to the actual
        // data.
        return 8;

    case DT_CSTRING:
        // C-String is represented as a pointer to the constant character buffer
        return 8;
    }

    ABORT("Impossible state");
}

void dtl_init(struct DataTypeList *dtl)
{
    dtl->data = NULL;
    dtl->size = dtl->cap = 0;
}

void dtl_deinit(struct DataTypeList *dtl)
{
    int i;
    for (i = 0; i != dtl->size; ++i) {
        dt_destroy(dtl->data[i]);
    }
    my_free(dtl->data);
    dtl->data = NULL;
    dtl->size = dtl->cap = 0;
}

void dtl_copy(struct DataTypeList *dst, struct DataTypeList *src)
{
    int i;

    if (!src || !dst) {
        ABORT("Invalid input");
    }

    dtl_deinit(dst);
    dtl_init(dst);

    for (i = 0; i != src->size; ++i) {
        struct DataType *copy = dt_copy(src->data[i]);
        array_append(*dst, copy);
    }
}

bool dtl_equal(struct DataTypeList *x, struct DataTypeList *y)
{
    int i, common_size;

    if (x->size != y->size) {
        return false;
    }

    common_size = x->size;

    for (i = 0; i != common_size; ++i) {
        if (!dt_equal(x->data[i], y->data[i])) {
            return false;
        }
    }

    return true;
}
