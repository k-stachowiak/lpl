/*
 * Copyright (C) 2020 Krzysztof Stachowiak
 *
 * This file is part of the Language Programming Language compiler.
 *
 * Language Programming Language is free software: you can redistribute it
 * and/or modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Language Programming Language compiler is distributed in the hope that it
 * will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "scanner.h"
#include "common.h"
#include "error.h"

void int_destroy_v(void *x)
{
    my_free(x);
}

// Procedure prototype functions
// -----------------------------

struct ProcedurePrototype *pp_create(void)
{
    struct ProcedurePrototype *result = my_malloc(sizeof(*result));
    result->name = NULL;
    result->arguments.data = NULL;
    result->arguments.size = result->arguments.cap = 0;
    result->results.data = NULL;
    result->results.size = result->results.cap = 0;
    result->body = NULL;
    return result;
}

void pp_destroy(struct ProcedurePrototype *pp)
{
    if (!pp) {
        return;
    }
    array_free(pp->results);
    array_free(pp->arguments);
    my_free(pp);
}

char *pp_argument_name(struct ProcedurePrototype *pp, int index)
{
    struct AstNode *node = pp->arguments.data[index];
    if (node->type != AST_SYMBOL) {
        return NULL;
    }
    return node->symbol.string;
}

char *pp_result_name(struct ProcedurePrototype *pp, int index)
{
    struct AstNode *node = pp->results.data[index];
    if (node->type != AST_SYMBOL) {
        return NULL;
    }
    return node->symbol.string;
}

static bool pp_header_loaded(struct ProcedurePrototype *pp)
{
    return pp->name;
}

static bool pp_body_loaded(struct ProcedurePrototype *pp)
{
    return pp->body;
}

// Scan result functions
// ---------------------

void psr_deinit(struct ProcedureScanResult *psr)
{
    ppt_deinit(&psr->ppt);
    if (psr->error_message) {
        my_free(psr->error_message);
    }
}

// Procedure prototype table functions
// -----------------------------------

void ppt_init(struct ProcedurePrototypeTable *ppt)
{
    ppt->data = NULL;
    ppt->cap = ppt->size = 0;
}

void ppt_deinit(struct ProcedurePrototypeTable *ppt)
{
    int i;
    if (ppt->cap == 0) {
        return;
    }
    for (i = 0; i < ppt->size; ++i) {
        pp_destroy(ppt->data[i]);
    }
    my_free(ppt->data);
    ppt->data = NULL;
    ppt->size = ppt->cap = 0;
}

struct ProcedurePrototype *ppt_find(
        struct ProcedurePrototypeTable *ppt,
        char *name)
{
    int i;
    for (i = 0; i != ppt->size; ++i) {
        struct ProcedurePrototype *pp = ppt->data[i];
        if (strcmp(pp->name, name) == 0) {
            return pp;
        }
    }
    return NULL;
}

static void ppt_insert(
        struct ProcedurePrototypeTable *ppt,
        struct ProcedurePrototype *pp)
{
    array_append(*ppt, pp);
}

// Procedure scanner code
// ----------------------

struct ScanProcedureContext {
    struct ErrorContext error_context;
    struct ProcedurePrototype *temp_procedure_prototype;
    struct ProcedurePrototypeTable ppt;
};

static void spc_init(struct ScanProcedureContext *spc)
{
    error_init(&spc->error_context);
    spc->temp_procedure_prototype = NULL;
    ppt_init(&spc->ppt);
}

static struct ProcedurePrototypeTable spc_deinit(
        struct ScanProcedureContext *spc)
{
    error_deinit(&spc->error_context);
    pp_destroy(spc->temp_procedure_prototype);
    return spc->ppt;
}

static void scan_procedure_arguments_callback(void *data, struct AstNode *node)
{
    struct ScanProcedureContext *spc = (struct ScanProcedureContext *)data;
    struct ProcedurePrototype *pp = spc->temp_procedure_prototype;

    if (node->type != AST_SYMBOL && node->type != AST_LITERAL) {
        error_throw(
            &spc->error_context,
            "Unexpected node when procedure argument expected");
    }

    array_append(pp->arguments, node);
}

static void scan_procedure_results_callback(void *data, struct AstNode *node)
{
    struct ScanProcedureContext *spc = (struct ScanProcedureContext *)data;
    struct ProcedurePrototype *pp = spc->temp_procedure_prototype;

    if (node->type != AST_SYMBOL && node->type != AST_LITERAL) {
        error_throw(
            &spc->error_context,
            "Unexpected node when procedure result expected");
    }

    array_append(pp->results, node);
}

static void scan_procedure_callback(void *data, struct AstNode *node)
{
    struct ScanProcedureContext *spc = (struct ScanProcedureContext *)data;
    struct ProcedurePrototype *pp = spc->temp_procedure_prototype;

    // The procedure block elements will be scanned separately, each time
    // this function will be called. The Procedure prototype object (pp
    // variable) stores the state of this process. The first stage is scanning
    // the procedure's header, secondly the procedure body is scanned. They must
    // come in this order.

    if (!pp_header_loaded(pp)) {
        // a) Since the procedure header has not been found yet, the current
        //    node must contain it:

        struct AstNode *call_pattern_node;
        struct AstNode *return_pattern_node;
        struct AstNode *arguments_node;
        struct AstNode *name_node;

        // a)1. Expecting the top level expression to be a procedure header
        //      operator:
        if (node->type != AST_BINARY ||
            node->binary.type != AST_BIN_PROC_HEAD) {
            error_throw(
                &spc->error_context,
                "Unexpected node when procedure header operator expected");
        }

        call_pattern_node = node->binary.left;
        return_pattern_node = node->binary.right;

        // a)2. Analyze the call pattern:
        if (call_pattern_node->type != AST_BINARY ||
            call_pattern_node->binary.type != AST_BIN_PROC_CALL) {
            error_throw(
                &spc->error_context,
                "Unexpected node when procedure call pattern expected");
        }

        // a)2.1. Read procedure's name:
        name_node = call_pattern_node->binary.left;
        if (name_node->type != AST_SYMBOL) {
            error_throw(
                &spc->error_context,
                "Unexpected node when procedure name expected");
        }
        pp->name = name_node->symbol.string;

        // a)2.2. Read procedure's arguments:
        arguments_node = call_pattern_node->binary.right;
        ast_for_each_left(
            arguments_node,
            AST_BIN_LIST_SEQUENCE,
            scan_procedure_arguments_callback,
            spc);

        // a)3. Analyze the return pattern:
        ast_for_each_left(
            return_pattern_node,
            AST_BIN_LIST_SEQUENCE,
            scan_procedure_results_callback,
            spc);

    } else if (!pp_body_loaded(pp)) {
        // b) If the header has been read the current node must be the body:
        pp->body = node;

    } else {
        // c) If we have got here, it measn, that there are more than two
        //    expressions in the procedure, which is an error:
        error_throw(
            &spc->error_context,
            "Too many expressions in the procedure declaration");
    }
}

static void scan_procedure(
        struct ScanProcedureContext *spc,
        struct AstNode *node)
{
    struct ProcedurePrototype *pp;

    // 1. Create and fill in the temporary procedure prototype structure:
    //    (we store the prototype in the scanner's context, so that upon error
    //     it can be released in the deinitialization code)
    spc->temp_procedure_prototype = pp_create();
    ast_for_each_left(
            node,
            AST_BIN_BLOCK_SEQUENCE,
            scan_procedure_callback,
            spc);

    // 2. Check for duplicate entries:
    pp = ppt_find(&spc->ppt, spc->temp_procedure_prototype->name);
    if (pp) {
        error_throw(&spc->error_context, "Duplicate procedure prototype found");
    }

    // 3. Record the prototype:
    ppt_insert(
        &spc->ppt,
        spc->temp_procedure_prototype);

    // 4. Reset the temp pointer not to come in the way of clean up
    //    upon error:
    spc->temp_procedure_prototype = NULL;
}

static void scan_procedures_callback(void *data, struct AstNode *node)
{
    struct ScanProcedureContext *spc = (struct ScanProcedureContext*)data;
    if (node->type == AST_BLOCK && node->block.type == AST_BLOCK_PROCCORP) {
        scan_procedure(spc, node->block.body);
    } else {
        error_throw(&spc->error_context, "Illegal toplevel AST node");
    }
}

struct ProcedureScanResult scan_procedures(struct AstNode *ast)
{
    struct ScanProcedureContext spc;
    struct ProcedureScanResult psr;

    // 1. Initialize scanner's state:
    spc_init(&spc);

    // 2. Handle potential errors:
    if (error_catch(&spc.error_context)) {
        psr.error_message = str_copy(spc.error_context.message);
        psr.ppt = spc_deinit(&spc);
        return psr;
    }

    // 3. Scan procedures:
    ast_for_each_left(
        ast,
        AST_BIN_BLOCK_SEQUENCE,
        scan_procedures_callback,
        &spc);

    // 4. Prepare result and return it:
    psr.error_message = NULL;
    psr.ppt = spc_deinit(&spc);
    return psr;
}

char *psr_to_string(struct ProcedureScanResult *psr)
{
    char *result = NULL;
    struct ProcedurePrototypeTable *ppt = &psr->ppt;
    struct ProcedurePrototype **pp = ppt->data;
    int i, ppt_size = ppt->size;

    str_append(result, "Scan result:\n");
    for (i = 0; i < ppt_size; ++i, ++pp) {
        int j;
        int arg_size = (*pp)->arguments.size;
        int res_size = (*pp)->results.size;
        struct AstNode **node;
        char *temp;
        str_append(result, "\t* Name: %s\n", (*pp)->name);
        str_append(result, "\t* Arguments:\n");
        for (j = 0, node = (*pp)->arguments.data; j < arg_size; ++j, ++node) {
            temp = ast_to_string_algebraic(*node);
            str_append(result, "\t\t* %s, ", temp);
            my_free(temp);
        }
        str_append(result, "\n");
        str_append(result, "\t* Results:\n");
        for (j = 0, node = (*pp)->results.data; j < res_size; ++j, ++node) {
            temp = ast_to_string_algebraic(*node);
            str_append(result, "\t\t* %s, ", temp);
            my_free(temp);
        }
        str_append(result, "\n");
        temp = ast_to_string_algebraic((*pp)->body);
        str_append(result, "\t* Body: %s\n~~~\n\n", temp);
        my_free(temp);
    }

    return result;
}

// Tests
// -----

#include "parser.h"

static void test_valid(struct TestContext *tc_parent)
{
    struct TestContext *tc = tc_create_child("VALID-CODE", tc_parent);

    // 1. Prepare input:
    struct TokCharStream cs;
    struct ParseResult pr;
    struct ProcedureScanResult psr;
    struct ProcedurePrototypeTable *ppt;
    struct ProcedurePrototype *pp;
    char *source =
        "proc foo(void) -> Unit #\n"
        "    noop(void)\n"
        "corp #\n"
        "proc bar(x, y) -> z #\n"
        "    noop(void)\n"
        "corp";

    // 2. Parser source code:
    tok_cs_init_string(&cs, source);
    pr = parse(&cs, false);
    tok_cs_deinit(&cs);
    tc_generic(tc,
               "Failed parsing source",
               !pr.error_message);
    if (!pr.result) {
        fprintf(stderr, "ERROR: %s\n", pr.error_message);
        parse_result_deinit(&pr);
        return;
    }

    // 3. Scan the AST:
    psr = scan_procedures(pr.result);
    tc_generic(tc,
               "Failed scanning conde",
               !psr.error_message);
    if (psr.error_message) {
        psr_deinit(&psr);
        parse_result_deinit(&pr);
        return;
    }

    // 4. Perform the test assertions:

    ppt = &psr.ppt;
    tc_generic(tc,
               "Check PSR: incorrect procedures count in table",
               ppt->size == 2);

    pp = ppt->data[0];
    tc_generic(tc,
               "Check PSR procedure 1: incorrect procedure name",
               strcmp(pp->name, "foo") == 0);
    tc_generic(tc,
               "Check PSR procedure 1: incorrect arguments count",
               pp->arguments.size == 1);
    tc_generic(tc,
               "Check PSR procedure 1: incorrect results count",
               pp->results.size == 1);
    tc_generic(tc,
               "Check PSR procedure 1: incorrect result name",
               ast_is_symbol_spec(pp->results.data[0], "Unit"));

    pp = ppt->data[1];
    tc_generic(tc,
               "Check PSR procedure 2: incorrect procedure name",
               strcmp(pp->name, "bar") == 0);
    tc_generic(tc,
               "Check PSR procedure 2: incorrect arguments count",
               pp->arguments.size == 2);
    tc_generic(tc,
               "Check PSR procedure 2: incorrect argument 1 name",
               ast_is_symbol_spec(pp->arguments.data[0], "x"));
    tc_generic(tc,
               "Check PSR procedure 2: incorrect argument 2 name",
               ast_is_symbol_spec(pp->arguments.data[1], "y"));
    tc_generic(tc,
               "Check PSR procedure 2: incorrect results count",
               pp->results.size == 1);
    tc_generic(tc,
               "Check PSR procedure 2: incorrect result name",
               ast_is_symbol_spec(pp->results.data[0], "z"));

    // 5. Cleanup:
    psr_deinit(&psr);
    parse_result_deinit(&pr);
}

void scanner_test(struct TestContext *tc_parent)
{
    struct TestContext *tc = tc_create_child("SCANNER", tc_parent);
    test_valid(tc);
}
