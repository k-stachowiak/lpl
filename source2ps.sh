#!/usr/bin/env bash

# Copyright (C) 2020 Krzysztof Stachowiak
#
# This file is part of the Language Programming Language compiler.
#
# Language Programming Language is free software: you can redistribute it
# and/or modify it under the terms of the GNU General Public License as
# published by the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# Language Programming Language compiler is distributed in the hope that it
# will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with Framework.  If not, see <http://www.gnu.org/licenses/>.

CLI=./cmd

if [ "$#" -ne 1 ]
then
    echo "Usage: $0 <source>"
    exit 1
fi

if [ ! -f $CLI ]
then
    echo "Command line tool not present."
    exit 1
fi

if [ ! -f $1 ]
then
    echo "File $1 not found"
    exit 1
fi

./cmd dump ast $1 | dot -Tps -o $1.ps
