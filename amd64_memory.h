/*
 * Copyright (C) 2020-2021 Krzysztof Stachowiak
 *
 * This file is part of the Language Programming Language compiler.
 *
 * Language Programming Language is free software: you can redistribute it
 * and/or modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Language Programming Language compiler is distributed in the hope that it
 * will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef AMD64_MEMORY_H
#define AMD64_MEMORY_H

#include "common.h"
#include "data_types.h"
#include "illiterate-amd64-encoder/amd64_encoder.h"

#include <stdbool.h>

// AMD64 Memory
// ============
//
// This module enables management of a virtual set of AMD64 resources.
// First we define a few helper datat types that help express basic memory
// management semantics, and finally we give the definition of the actual
// memory manager type.

// Object
// ------

// The AMD64Object structure defines an object operated by the AMD64 machine.
// It is given by the object's type and its location in the machine.
// The object references are explicitly acquired, and must be explicitly
// disposed when no longer needed.

struct AMD64Object {
    struct DataType *data_type;
    struct AMD64Location location;
};

void amd64_obj_swap(struct AMD64Object *x, struct AMD64Object *y);

// Scope
// -----

// Here we provide a data structure for lexical scoping.

struct AMD64Scope {
    struct AMD64Scope *parent;
    struct Trie objects;
};

struct AMD64Scope *amd64_scope_create(struct AMD64Scope *parent);

void amd64_scope_destroy(struct AMD64Scope *scope);

void amd64_scope_put(struct AMD64Scope *scope,
                     char *name,
                     struct AMD64Object *obj);

struct AMD64Object *amd64_scope_find(struct AMD64Scope *scope, char *name);

void amd64_scope_for_each(
        struct AMD64Scope *scope,
        void (*callback)(char *, struct AMD64Object *, void *),
        void *data);

// High level registers
// --------------------

// While the instruction encoder defines its own data types to represent the
// AMD64 registers, at the level of the general memory management, they are
// treated differently, therefore we define them separately here.

enum AMD64Register {
    AMD64_REG_AX   = 0x0000001,
    AMD64_REG_BX   = 0x0000002,
    AMD64_REG_CX   = 0x0000004,
    AMD64_REG_DX   = 0x0000008,
    AMD64_REG_SP   = 0x0000010,
    AMD64_REG_BP   = 0x0000020,
    AMD64_REG_SI   = 0x0000040,
    AMD64_REG_DI   = 0x0000080,

    AMD64_REG_8    = 0x0000100,
    AMD64_REG_9    = 0x0000200,
    AMD64_REG_10   = 0x0000400,
    AMD64_REG_11   = 0x0000800,
    AMD64_REG_12   = 0x0001000,
    AMD64_REG_13   = 0x0002000,
    AMD64_REG_14   = 0x0004000,
    AMD64_REG_15   = 0x0008000,

    AMD64_REG_XMM0 = 0x0010000,
    AMD64_REG_XMM1 = 0x0020000,
    AMD64_REG_XMM2 = 0x0040000,
    AMD64_REG_XMM3 = 0x0080000,
    AMD64_REG_XMM4 = 0x0100000,
    AMD64_REG_XMM5 = 0x0200000,
    AMD64_REG_XMM6 = 0x0400000,
    AMD64_REG_XMM7 = 0x0800000,

    AMD64_REG_MAX  = 0x1000000
};

enum AMD64GPRegister amd64_reg_to_gpreg(enum AMD64Register reg);
enum AMD64SSERegister amd64_reg_to_ssereg(enum AMD64Register reg);

enum AMD64Register amd64_reg_from_gpreg(enum AMD64GPRegister reg);
enum AMD64Register amd64_reg_from_ssereg(enum AMD64SSERegister reg);
enum AMD64Register amd64_reg_from_ireg(enum AMD64IndirectRegister ireg);
enum AMD64Register amd64_reg_from_sreg(enum AMD64SIBRegister sreg);

bool amd64_reg_is_gp(enum AMD64Register reg);
bool amd64_reg_is_sse(enum AMD64Register reg);

// Memory manager
// --------------
//
// This type represents AMD64 memory model in a single stack frame.
// TODO: This should probably be renamed to something like AMD64StackFrame
//       once I really understand what that is.

struct AMD64Memory {
    enum AMD64Register register_map;
    int64_t rbp_offset;
    struct AMD64Scope *local_scope;
    struct { struct AMD64Object **data; int size, cap; } temps;
};

void amd64_mem_init(struct AMD64Memory *mem);
void amd64_mem_deinit(struct AMD64Memory *mem);

void amd64_mem_subscope_create(struct AMD64Memory *mem);
void amd64_mem_subscope_destroy(struct AMD64Memory *mem);

bool amd64_mem_register_free(struct AMD64Memory *mem, enum AMD64Register reg);

enum AMD64Register amd64_mem_find_free_gpreg(struct AMD64Memory *mem);
enum AMD64Register amd64_mem_find_free_ssereg(struct AMD64Memory *mem);
enum AMD64Register amd64_mem_find_free_compatible_register(
        struct AMD64Memory *mem,
        struct DataType *data_type);

struct AMD64Object *amd64_mem_create_object(
        struct AMD64Memory *mem,
        struct DataType *data_type,
        struct AMD64Location location);

struct AMD64Object *amd64_mem_create_imm_unit(
        struct AMD64Memory *mem);

struct AMD64Object *amd64_mem_create_imm_boolean(
        struct AMD64Memory *mem,
        uint8_t value);

struct AMD64Object *amd64_mem_create_imm_word(
        struct AMD64Memory *mem,
        int64_t value);

struct AMD64Object *amd64_mem_create_reg(
        struct AMD64Memory *mem,
        enum AMD64Register reg,
        struct DataType *data_type);

struct AMD64Object *amd64_mem_create_reg_word(
        struct AMD64Memory *mem,
        enum AMD64Register reg);

struct AMD64Object *amd64_mem_create_stack_object(
        struct AMD64Memory *mem,
        struct DataType *data_type);

struct AMD64Object *amd64_mem_create_stack_word(struct AMD64Memory *mem);

void amd64_mem_dispose(struct AMD64Memory *mem, struct AMD64Object *obj);

bool amd64_mem_try_put(
        struct AMD64Memory *mem,
        char *name,
        struct AMD64Object *obj);
void amd64_mem_put(
        struct AMD64Memory *mem,
        char *name,
        struct AMD64Object *obj);

char *amd64_mem_find_by_obj(
        struct AMD64Memory *mem,
        struct AMD64Object *obj);

struct AMD64Object *amd64_mem_find_by_reg(
        struct AMD64Memory *mem,
        enum AMD64Register reg);

struct AMD64Object *amd64_mem_find_by_name(
        struct AMD64Memory *mem,
        char *name);

bool amd64_mem_is_temp(struct AMD64Memory *mem, struct AMD64Object *obj);

#endif
